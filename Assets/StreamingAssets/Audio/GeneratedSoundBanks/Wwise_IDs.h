/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ANTONIO = 3660849047U;
        static const AkUniqueID ANTONIO2 = 386775687U;
        static const AkUniqueID ANTONIOSTART = 3846191413U;
        static const AkUniqueID DASH = 1942692385U;
        static const AkUniqueID DEATH = 779278001U;
        static const AkUniqueID DRYINGFOOTSTEPS = 2917503229U;
        static const AkUniqueID ENTEREDEXITZONE = 556609300U;
        static const AkUniqueID ENTERING_WATER = 1693174379U;
        static const AkUniqueID FOOTSTEP = 1866025847U;
        static const AkUniqueID LEAVEDEXITZONE = 1162170676U;
        static const AkUniqueID LIGHTS_BUZZING = 305834622U;
        static const AkUniqueID LOW_HP_DIALOG = 694828579U;
        static const AkUniqueID LOW_ON_TIME_MUSIC = 3892442613U;
        static const AkUniqueID PLAY_AMANECER = 1659668068U;
        static const AkUniqueID PLAY_AMBIENT = 1562304622U;
        static const AkUniqueID PLAY_XXLSCRAP = 2227453841U;
        static const AkUniqueID RADIOMUSIC_PLAY = 3682185828U;
        static const AkUniqueID RADIOMUSIC_STOP = 2159768926U;
        static const AkUniqueID RAT = 1047714102U;
        static const AkUniqueID SCRAPPED = 2449481535U;
        static const AkUniqueID SCRAPPEDCOMMENT = 4207971634U;
        static const AkUniqueID SCRAPPING = 3945264084U;
        static const AkUniqueID STAFF_HIT = 2137267339U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace ANTONIO_CITY
        {
            static const AkUniqueID GROUP = 2355328131U;

            namespace SWITCH
            {
                static const AkUniqueID FIRST_DIALOG = 2774171492U;
                static const AkUniqueID SECOND_DIALOG = 2928661496U;
                static const AkUniqueID THIRD_DIALOG = 20648483U;
            } // namespace SWITCH
        } // namespace ANTONIO_CITY

        namespace FOOTSTEPSMATERIAL
        {
            static const AkUniqueID GROUP = 982447677U;

            namespace SWITCH
            {
                static const AkUniqueID MUD = 712897245U;
                static const AkUniqueID WATER = 2654748154U;
            } // namespace SWITCH
        } // namespace FOOTSTEPSMATERIAL

        namespace LOWONTIME
        {
            static const AkUniqueID GROUP = 2377043385U;

            namespace SWITCH
            {
                static const AkUniqueID FIRSTWARNING = 4264152975U;
                static const AkUniqueID THIRDWARNING = 4196274796U;
            } // namespace SWITCH
        } // namespace LOWONTIME

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID DRYINGFOOTSTEPS = 2917503229U;
        static const AkUniqueID SCRAPRARITY = 1819818029U;
        static const AkUniqueID SIDECHAIN = 1883033791U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID DIALOGS = 4236861272U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID OBJECTS = 1695690031U;
        static const AkUniqueID PLAYERSOUNDS = 1327972334U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
