﻿using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.GameServices
{
    public interface IGameSettingsProvider
    {
        public T Get<T>() where T : ScriptableObject;
    }

    public class GameSettingsProvider : MonoBehaviour, IGameSettingsProvider
    {
        [SerializeField] public List<ScriptableObject> allSettings;
        
        private Dictionary<System.Type, ScriptableObject> settingsDictionary;

        private void Awake()
        {
            settingsDictionary = new();
            
            foreach (var setting in allSettings)
            {
                settingsDictionary[setting.GetType()] = setting;
            }
        }
        
        public T Get<T>() where T : ScriptableObject
        {
            if (settingsDictionary.TryGetValue(typeof(T), out var setting))
            {
                return setting as T;
            }
            
            Debug.LogWarning($"No settings found for type {typeof(T)}");
            return null;
        }
    }
}