﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace _Project.Scripts.GameServices
{
    public enum ActionMap
    {
        CharacterController,
        UINavigation
    }
    
    public interface IInputProvider
    {
        public void Reset();
        public void SwitchActionMap(ActionMap actionMap);
        public event Action AnyButtonPressed;
        public event Action<Vector2> MoveInputStarted;
        public event Action<Vector2> MoveInputPerformed;
        public event Action<Vector2> MoveInputCanceled;
        public event Action<Vector2> RotationPerformed;
        public event Action<float> RotateCameraPerformed;
        public event Action RotateCameraCanceled;
        public event Action RunStarted;
        public event Action RunCanceled;
        public event Action DashPerformed;
        public event Action ActionPerformed;
        public event Action ActionCanceled;
        public event Action AttackPerformed;
        public event Action AttackCanceled;
        public event Action UseUtilityStarted;
        public event Action UseUtilityCanceled;
        public event Action InventoryStarted;
        public event Action InventorySwitchStarted;
        public event Action StartButtonPerformed;
        public event Action MapPerformed;
    }
    
    public class InputProvider : MonoBehaviour, IInputProvider
    {
        [Title("Settings")]
        [SerializeField] private float moveVectorThreshold = 0.125f;
        [ReadOnly] private Vector2 moveInputVector;
        [SerializeField] private Logger logger;
        [SerializeField] private PlayerInput playerInputComponent;
        public event Action AnyButtonPressed;
        public event Action<Vector2> MoveInputStarted;
        public event Action<Vector2> MoveInputPerformed;
        public event Action<Vector2> MoveInputCanceled;
        public event Action<Vector2> RotationPerformed;
        public event Action<float> RotateCameraPerformed;
        public event Action RotateCameraCanceled;
        public event Action RunStarted;
        public event Action RunCanceled;
        public event Action DashPerformed;
        public event Action ActionPerformed;
        public event Action ActionCanceled;
        public event Action AttackPerformed;
        public event Action AttackCanceled;
        public event Action UseUtilityStarted;
        public event Action UseUtilityCanceled;
        public event Action InventoryStarted;
        public event Action InventorySwitchStarted;
        public event Action StartButtonPerformed;
        public event Action MapPerformed;

        private PlayerInputs _playerInputs;
        
        private IDisposable m_EventListener;
        private WaitForSeconds anyButtonCheckInterval = new WaitForSeconds(_buttonPressedCooldownCheck);
        private const float _buttonPressedCooldownCheck = 0.25f;
        private bool _justPressedAnyButton;
        private bool _isGamepad;

        private List<InputDevice> _allowedDevices;
        private List<Gamepad> _allowedGamepads;
        
        private void Awake()
        {
            _playerInputs = new PlayerInputs();
        }
        
        private void OnEnable()
        {
            _allowedDevices = new List<InputDevice>();
            _allowedGamepads = new List<Gamepad>();
            _playerInputs.Enable();

            m_EventListener = InputSystem.onAnyButtonPress
                .Call(OnAnyButtonPressed);
            
            foreach (var device in InputSystem.devices)
            {
                if (device.name.Contains("XInput"))
                {
                    Debug.Log("XInput detectado. Ignorando...");
                    continue;
                }
                
                if (device == Gamepad.current)
                {
                    _allowedDevices.Add(device);
                }
                //
                // if (device is Gamepad gamepad)
                // {
                //     _allowedGamepads.Add(gamepad);
                // }
                
                if (device == Keyboard.current)
                {
                    _allowedDevices.Add(device);
                }

                if (device == Mouse.current)
                {
                    _allowedDevices.Add(device);
                }
                
                if (device.description.manufacturer == "Unwanted Device Manufacturer")
                {
                    InputSystem.DisableDevice(device);
                }
            }
        }

        private void OnDisable()
        {
            _playerInputs.Disable();
            m_EventListener.Dispose();
        }
        
        public void Reset()
        {
            OnDisable();
            OnEnable();
            playerInputComponent.onControlsChanged -= OnControllsChangedEvent;
                
            _playerInputs.CharacterControls.Move.started -= OnMoveInputStarted;
            _playerInputs.CharacterControls.Move.canceled -= OnMoveInputCanceled;
        
            _playerInputs.CharacterControls.RotateCamera.performed -= OnRotateCameraPerformed;
            _playerInputs.CharacterControls.RotateCamera.canceled -= OnRotateCameraCanceled;
            
            _playerInputs.CharacterControls.Run.started -= OnRunStarted;
            _playerInputs.CharacterControls.Run.canceled -= OnRunCanceled;

            _playerInputs.CharacterControls.Dash.performed -= OnDashPerformed;

            _playerInputs.CharacterControls.Action.performed -= OnActionPerformed;
            _playerInputs.CharacterControls.Action.canceled -= OnActionCanceled;

            _playerInputs.CharacterControls.Attack.performed -= OnAttackPerformed;
            _playerInputs.CharacterControls.Attack.canceled -= OnAttackCanceled;

            _playerInputs.CharacterControls.UseUtility.started -= OnUseUtilityStarted;
            _playerInputs.CharacterControls.UseUtility.canceled -= OnUseUtilityCanceled;

            _playerInputs.CharacterControls.Inventory.started -= OnInventoryStarted;
            _playerInputs.CharacterControls.InventorySwitch.started -= OnInventorySwitch;

            _playerInputs.CharacterControls.Map.performed -= OnMapPerformed;
            
            _playerInputs.UINavigation.Move.performed -= OnUINavigationMove;
            _playerInputs.UINavigation.Action.performed -= OnActionPerformed;
            
            Start();
        }

        private void Start()
        {
            playerInputComponent.onControlsChanged += OnControllsChangedEvent;
            
            _playerInputs.CharacterControls.Move.started += OnMoveInputStarted;
            _playerInputs.CharacterControls.Move.canceled += OnMoveInputCanceled;
        
            _playerInputs.CharacterControls.RotateCamera.performed += OnRotateCameraPerformed;
            _playerInputs.CharacterControls.RotateCamera.canceled += OnRotateCameraCanceled;
            
            _playerInputs.CharacterControls.Run.started += OnRunStarted;
            _playerInputs.CharacterControls.Run.canceled += OnRunCanceled;

            _playerInputs.CharacterControls.Dash.performed += OnDashPerformed;

            _playerInputs.CharacterControls.Action.started += OnActionPerformed;
            _playerInputs.CharacterControls.Action.canceled += OnActionCanceled;

            _playerInputs.CharacterControls.Attack.performed += OnAttackPerformed;
            _playerInputs.CharacterControls.Attack.canceled += OnAttackCanceled;

            _playerInputs.CharacterControls.UseUtility.started += OnUseUtilityStarted;
            _playerInputs.CharacterControls.UseUtility.canceled += OnUseUtilityCanceled;

            _playerInputs.CharacterControls.Inventory.started += OnInventoryStarted;
            _playerInputs.CharacterControls.InventorySwitch.started += OnInventorySwitch;

            _playerInputs.CharacterControls.Map.performed += OnMapPerformed;
            
            _playerInputs.UINavigation.Move.performed += OnUINavigationMove;
            _playerInputs.UINavigation.Action.performed += OnActionPerformed;
        }

        private void OnControllsChangedEvent(PlayerInput input)
        {
            Gamepad gamepad = input.GetDevice<Gamepad>();
            if (gamepad is UnityEngine.InputSystem.Switch.SwitchProControllerHID)
            {
                foreach (var item in Gamepad.all)
                {
                    if ((item is UnityEngine.InputSystem.XInput.XInputController) && (Math.Abs(item.lastUpdateTime - gamepad.lastUpdateTime) < 0.1))
                    {
                        Debug.Log($"Switch Pro controller detected and a copy of XInput was active at almost the same time. Disabling XInput device. `{gamepad}`; `{item}`");
                        InputSystem.DisableDevice(item);
                    }
                }
            }
        }

        private void Update()
        {
            if (_playerInputs.CharacterControls.enabled)
            {
                Vector2 lookDirection = _playerInputs.CharacterControls.LookDirection.ReadValue<Vector2>();
                RotationPerformed?.Invoke(lookDirection);
                
                moveInputVector = _playerInputs.CharacterControls.Move.ReadValue<Vector2>();

                if (moveVectorThreshold <= 0)
                {
                    OnMoveInputPerformed(moveInputVector);
                    return;
                }
            
                if (moveInputVector.sqrMagnitude > moveVectorThreshold * moveVectorThreshold)
                    OnMoveInputPerformed(moveInputVector);
            }
        }
        
        public void SwitchActionMap(ActionMap actionMap)
        {
            _playerInputs.CharacterControls.Disable();
            _playerInputs.UINavigation.Disable();
            
            switch (actionMap)
            {
                case ActionMap.CharacterController:
                    _playerInputs.CharacterControls.Enable();
                    break;
                case ActionMap.UINavigation:
                    _playerInputs.UINavigation.Enable();
                    break;
                default:
                    _playerInputs.CharacterControls.Enable();
                    break;
            }
        }
        
        public void OnDeviceChange(PlayerInput playerInput)
        {
            _isGamepad = playerInput.currentControlScheme.Equals("ProController") ? true : false;
        }
        
        private void OnUINavigationMove(InputAction.CallbackContext context)
        {
            LogContext(context, "OnMoveInputStarted");
            MoveInputPerformed?.Invoke(context.ReadValue<Vector2>());
        }
        
        private void OnAnyButtonPressed(InputControl button)
        {
            // var device = button.device;
            
            if (_justPressedAnyButton)
            {
                return;
            }
            _justPressedAnyButton = true;
            
            // // Ignore presses on devices that are already used by a player.
            // if (PlayerInput.FindFirstPairedToDevice(device) != null)
            // {
            //     return;
            // }
            
            StartCoroutine(AnyButtonPressCooldown());            
            AnyButtonPressed?.Invoke();
        }

        private IEnumerator AnyButtonPressCooldown()
        {
            yield return anyButtonCheckInterval;
            _justPressedAnyButton = false;
        }

        private void OnMoveInputStarted(InputAction.CallbackContext context)
        {
            LogContext(context, "OnMoveInputStarted");
            MoveInputStarted?.Invoke(context.ReadValue<Vector2>());
        }

        private void OnMoveInputPerformed(Vector2 moveInput)
        {
            // logger.Log($"OnMoveInputPerformed");
            MoveInputPerformed?.Invoke(moveInput);
        }

        private void OnMoveInputCanceled(InputAction.CallbackContext context)
        {
            LogContext(context, "OnMoveInputCanceled");
            MoveInputCanceled?.Invoke(context.ReadValue<Vector2>());
        }

        private void OnRotateCameraPerformed(InputAction.CallbackContext context)
        {
            RotateCameraPerformed?.Invoke(context.ReadValue<float>());
        }
        
        private void OnRotateCameraCanceled(InputAction.CallbackContext context)
        {
            RotateCameraCanceled?.Invoke();
        }
        
        private void OnRunStarted(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnRunStarted");
            RunStarted?.Invoke();
        }

        private void OnRunCanceled(InputAction.CallbackContext context)
        {
            
            if (!_allowedDevices.Contains(context.control.device))
            {
                return;
            }
            LogContext(context, "OnRunCanceled");
            RunCanceled?.Invoke();
        }
        private void OnDashPerformed(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnDashPerformed");
            DashPerformed?.Invoke();
        }

        private void OnActionPerformed(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnActionPerformed");
            ActionPerformed?.Invoke();
        }

        private void OnActionCanceled(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            //
            LogContext(context, "OnActionCanceled");
            ActionCanceled?.Invoke();
        }

        private void OnAttackPerformed(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnAttackStarted");
            AttackPerformed?.Invoke();
        }
        
        private void OnAttackCanceled(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }

            LogContext(context, "OnAttackCanceled");
            AttackCanceled?.Invoke();
        }
        
        private void OnUseUtilityStarted(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnUseUtilityStarted");
            UseUtilityStarted?.Invoke();
        }

        private void OnUseUtilityCanceled(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnUseUtilityCanceled");
            UseUtilityCanceled?.Invoke();
        }

        private void OnInventoryStarted(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnInventoryStarted");
            InventoryStarted?.Invoke();
        }

        private void OnInventorySwitch(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnInventorySwitch");
            InventorySwitchStarted?.Invoke();
        }

        private void OnMapPerformed(InputAction.CallbackContext context)
        {
            // if (!_allowedDevices.Contains(context.control.device))
            // {
            //     return;
            // }
            
            LogContext(context, "OnMapPerformed");
            MapPerformed?.Invoke();
        }

        private void LogContext(InputAction.CallbackContext context, string input)
        {
            string actionMapName = context.action.actionMap.name;
            InputDevice device = context.control.device;
            string deviceName = device.displayName;
            
            logger.Log($"{input} triggered from ActionMap: {actionMapName}, Device: {deviceName}");
        }
    }
}