﻿namespace _Project.Scripts.Core.NarrativeSystem
{
    // [Button("Small Injure")]
    // public void Injure()
    // {
    //     string name = InkConstants.HealthStatus.VariableName;
    //     StringValue value = new StringValue(InkVariablesConstants.HealthStatus.Healthy); 
    //     EventManager.Shared.Raise(new GameEvents.DialogueEvents.InkDialogueVariableUpdated(name, value));   
    // }

    public static class InkConstants
    {
        public static readonly string CurrentHealthPercentage = "current_health_percentage";
        
        public static readonly string CurrentScrapAmountCommon = "current_scrap_amount_common";
        public static readonly string CurrentScrapAmountUncommon = "current_scrap_amount_uncommon";
        public static readonly string CurrentScrapAmountRare = "current_scrap_amount_rare";
        public static readonly string CurrentScrapAmountEpic = "current_scrap_amount_epic";
        public static readonly string CurrentScrapAmountLegendary = "current_scrap_amount_legendary";
        
        public static readonly string Guita = "current_scrap_amount_legendary";
        
        public static readonly string HasGrabbedRedSphere  = "has_grabbed_red_sphere";
        public static readonly string HasGrabbedGreenSphere  = "has_grabbed_green_sphere";
        public static readonly string HasGrabbedBlueSphere  = "has_grabbed_blue_sphere";
        
        // TBD
        public static class HealthStatus
        {
            public static readonly string VariableName = "current_health_status";
            public static readonly string Healthy = "HEALTHY";
            public static readonly string SomeWhatInjured = "SOMEWHAT_INJURED";
            public static readonly string SeverelyInjured = "SEVERELY_INJURED";
        }
    }
}