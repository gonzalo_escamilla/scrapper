﻿using System.Collections.Generic;
using Ink.Runtime;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class DialogueData
    {
        public string Line;
        public List<Choice> Choices;
    }
}