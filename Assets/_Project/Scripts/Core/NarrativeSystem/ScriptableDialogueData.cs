﻿using UnityEngine;
using UnityEngine.Serialization;

namespace _Project.Scripts.Core.NarrativeSystem
{
    [CreateAssetMenu(menuName = "Scrapper/DialogueSystem/Create Dialogue Data", fileName = "ScriptableDialogueData",
        order = 0)]
    public class ScriptableDialogueData : ScriptableObject
    {
        [FormerlySerializedAs("dialogueData")] public DialogueTriggerData dialogueTriggerData;
    }
}