﻿using System;
using _Project.Scripts.Core.EventsSystem;
using Enderlook.EventManager;
using Ink.Runtime;
using Sirenix.OdinInspector;
using UnityEngine;
using Object = Ink.Runtime.Object;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class DialogueController : MonoBehaviour
    {
        [Title("Ink Story")] 
        [SerializeField] private TextAsset inkJson;

        public event Action<DialogueData> DialogueUpdated;
        public event Action<int> ChoiceIndexChanged;
        public event Action DialogueCompleted;
        
        private Story _story;
        private InkExternalFunctions _inkExternalFunctions;
        private InkDialogueVariables _inkDialogueVariables;
        
        private int _currentChoiceIndex = 0;
        private bool _dialoguePlaying;
        
        private void Awake()
        {
            _story = new Story(inkJson.text);
            _inkExternalFunctions = new InkExternalFunctions();
            _inkExternalFunctions.Bind(_story);
            
            _inkDialogueVariables = new InkDialogueVariables(_story);
            
            EventManager.Shared.Subscribe<GameEvents.Dialogue.InkVariableUpdated>(OnInkDialogueVariablesChanged);
        }

        private void OnInkDialogueVariablesChanged(GameEvents.Dialogue.InkVariableUpdated eventData)
        {
            Object objectValue = InkUtilities.GetValueObject(eventData.Value);
            _inkDialogueVariables.UpdateVariableState(eventData.VariableName, objectValue);
        }

        private void OnDestroy()
        {
            _inkExternalFunctions.Unbind(_story);

            EventManager.Shared.Unsubscribe<GameEvents.Dialogue.InkVariableUpdated>(OnInkDialogueVariablesChanged);
        }

        public void StartDialogue(DialogueTriggerData dialogueTriggerData)
        {
            if (_dialoguePlaying)
                return;
            _dialoguePlaying = true;

            if (dialogueTriggerData.KnotName.Equals(""))
            {
                Debug.LogWarning("Knot name was the empty string when entering the dialogue");
                return;
            }

            _story.ChoosePathString(dialogueTriggerData.KnotName);
            
            _inkDialogueVariables.SyncVariablesAndStartListening(_story);
            
            ContinueOrExitStory();
        }

        public void Next()
        {
            if (!_dialoguePlaying) 
                return;
            
            ContinueOrExitStory();
        }

        public void SetChoiceIndex(int index)
        {
            if (_story.currentChoices.Count == 0)
            {
                _currentChoiceIndex = 0;
                Debug.LogWarning("No choices available in this dialogue.");
                return;
            }
        
            _currentChoiceIndex = Mathf.Clamp(index, 0, _story.currentChoices.Count - 1);
        }

        public void MoveChoiceIndex(int direction)
        {
            if (_story.currentChoices.Count == 0)
            {
                _currentChoiceIndex = 0;
                Debug.LogWarning("No choices available in this dialogue.");
                return;
            }
        
            _currentChoiceIndex = (_currentChoiceIndex + direction + _story.currentChoices.Count) % _story.currentChoices.Count;
            ChoiceIndexChanged?.Invoke(_currentChoiceIndex);
        }
        
        private void ContinueOrExitStory()
        {
            if (!_story.canContinue && _story.currentChoices.Count == 0)
            {
                ExitDialogue();
                return;
            }

            if (_story.currentChoices.Count > 0)
            {
                _story.ChooseChoiceIndex(_currentChoiceIndex);
                _currentChoiceIndex = 0;
            }
            
            var dialogueData = new DialogueData()
            {
                Line = _story.Continue(),
                Choices = _story.currentChoices
            };
            DialogueUpdated?.Invoke(dialogueData);
        }

        private void ExitDialogue()
        {
            Debug.Log("Exit Dialogue");
            _dialoguePlaying = false;
            _story.ResetState();

            _inkDialogueVariables.StopListening(_story);
            
            DialogueCompleted?.Invoke();
        }
    }
}