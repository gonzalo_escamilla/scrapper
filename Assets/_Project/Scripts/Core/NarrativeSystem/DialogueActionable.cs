﻿using _Project.Scripts.Core.EventsSystem;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class DialogueActionable : MonoBehaviour, IActionable
    {
        [SerializeField] private ScriptableDialogueData dialogueData;

        private void Awake()
        {
            IsActionable = true;
        }

        public bool IsActionable { get; private set; }

        public void PerformAction()
        {
            EventManager.Shared.Raise(
                new GameEvents.Dialogue.DialogueInteractionCompleted(dialogueData.dialogueTriggerData));
        }

        public void UndoAction()
        {
        }
    }
}