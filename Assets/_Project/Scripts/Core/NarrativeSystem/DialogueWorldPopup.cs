﻿using System;
using _Project.Scripts.Core.UI;
using _Project.Scripts.GameServices;
using UnityEngine;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class DialogueWorldPopup : MonoBehaviour
    {
        [SerializeField] private PopUpImage _popUpImage;

        private void Awake()
        {
            Services.Add<DialogueWorldPopup>(this);
        }

        private void OnDestroy()
        {
            Services.Remove(this);
        }

        public void Show(Vector3 position)
        {
            transform.position = position;
            _popUpImage.Show();
        }

        public void Hide(Vector3 position)
        {
            transform.position = position;
            _popUpImage.Hide();
        }
    }
}