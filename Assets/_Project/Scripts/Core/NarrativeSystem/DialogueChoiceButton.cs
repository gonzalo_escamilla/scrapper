﻿using Ink.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class DialogueChoiceButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Image image;
        [SerializeField] private Image arrowImage;
        [SerializeField] private Color baseColor;
        [SerializeField] private Color selectedColor;
        
        private Choice _choiceData;
        
        public void ShowChoice(Choice dataChoice)
        {
            _choiceData = dataChoice;
            text.text = _choiceData.text;
        }

        public void Select()
        {
            image.color = selectedColor;
            arrowImage.gameObject.SetActive(true);
        }
        
        public void Deselect()
        {
            image.color = baseColor;
            arrowImage.gameObject.SetActive(false);
        }
        
        public void Clear()
        {
            text.text = "";
        }
    }
}