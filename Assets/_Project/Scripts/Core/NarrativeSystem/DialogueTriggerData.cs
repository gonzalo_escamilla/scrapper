﻿using System;
using UnityEngine;

namespace _Project.Scripts.Core.NarrativeSystem
{
    [Serializable]
    public class DialogueTriggerData
    {
        [SerializeField] private string knotName;
        [SerializeField] private string dialogueTitle;
        [SerializeField] private Sprite dialogueIcon;

        public string KnotName => knotName;
        public string DialogueTitle => dialogueTitle;
        public Sprite DialogueIcon => dialogueIcon;
    }
}