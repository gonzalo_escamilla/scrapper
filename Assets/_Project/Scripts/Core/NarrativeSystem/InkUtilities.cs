﻿using Ink.Runtime;
using UnityEngine;
using Object = Ink.Runtime.Object;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public static class InkUtilities
    {
        public static StringValue GetStringValue(string value)
        {
            return new StringValue(value);
        }
        
        public static Object GetValueObject(object value)
        {
            switch (value)
            {
                case string stringValue:
                    return new StringValue(stringValue);
                case int intValue:
                    return new IntValue(intValue);
                case float floatValue:
                    return new FloatValue(floatValue);
                case bool boolValue:
                    return new BoolValue(boolValue);
                default:
                    Debug.LogError($"Value type is not supported. Type {value.GetType()}");
                    return null;
            }
        }
    }
}