﻿using _Project.Scripts.GameServices;
using _Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class DialogueInteractable : SinglePressInteractable
    {
        [SerializeField] private DetectorSettings detectorSettings;
        private Detector<ScrapperController> _detector;

        private DialogueWorldPopup _dialogueWorldPopup;
        
        private void Awake()
        {
            _detector = new Detector<ScrapperController>(transform, detectorSettings);
            _detector.OnTargetDetected += OnTargetDetected;
            _detector.OnTargetLost += OnTargetLost;
        }

        public override void Start()
        {
            base.Start();
            _dialogueWorldPopup = Services.Get<DialogueWorldPopup>();
        }
        
        private void Update()
        {
            _detector.Update();
        }

        private void OnDestroy()
        {
            _detector.OnTargetDetected -= OnTargetDetected;
            _detector.OnTargetLost -= OnTargetLost;
        }

        private void OnDrawGizmosSelected()
        {
            _detector.OnDrawGizmos();
        }

        private void OnTargetDetected(ScrapperController scrapperController)
        {
            Show();
        }

        private void OnTargetLost(ScrapperController scrapperController)
        {
            Hide();
        }

        private void Show()
        {
            _dialogueWorldPopup.Show(transform.position);
        }

        private void Hide()
        {
            _dialogueWorldPopup.Hide(transform.position);
        }
    }
}