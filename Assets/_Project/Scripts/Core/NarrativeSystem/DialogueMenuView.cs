using System.Collections;
using System.Collections.Generic;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.GameServices;
using Ink.Runtime;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class DialogueMenuView : GameMenuBase
    {
        [Title("References")] 
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI bodyText;
        [SerializeField] private CanvasGroup choicesCanvasGroup;
        [SerializeField] private List<DialogueChoiceButton> choiceButtons;
        [SerializeField] private CanvasGroup canvasGroup;

        [Title("Settings")] 
        [SerializeField] private float textSpeed;
        [SerializeField] private float speedMultiplier;

        private DialogueController dialogueController;
        private IInputProvider _inputProvider;
        private float _originalTextSpeed;
        private bool _textBeingDisplayed;
    
        private void Awake()
        {
            dialogueController = Services.Get<DialogueController>();
            
            canvasGroup.alpha = 0;
            dialogueController.DialogueCompleted += OnDialogueCompleted;
            dialogueController.DialogueUpdated += ShowDialogue;
            dialogueController.ChoiceIndexChanged += ChoiceIndexChanged;
            
            _inputProvider = Services.Get<IInputProvider>();
            _inputProvider.ActionPerformed += OnActionPerformed;
            _originalTextSpeed = textSpeed;
        }

        private void OnDestroy()
        {
            if (dialogueController != null)
            {
                dialogueController.DialogueCompleted -= OnDialogueCompleted;
                dialogueController.DialogueUpdated -= ShowDialogue;
                dialogueController.ChoiceIndexChanged -= ChoiceIndexChanged;
            }

            if (_inputProvider != null) 
                _inputProvider.ActionPerformed -= OnActionPerformed;
        }

        private void ChoiceIndexChanged(int choiceIndex)
        {
            DeselectAllButtons();
            DialogueChoiceButton choiceButton = choiceButtons[choiceIndex];
            choiceButton.Select();
        }

        private void DeselectAllButtons()
        {
            foreach (DialogueChoiceButton button in choiceButtons)
            {
                button.Deselect();
            }
        }
        
        private void OnActionPerformed()
        {
            if (_textBeingDisplayed)
            {
                SpeedUp();
                return;
            }
        
            dialogueController.Next();
        }

        private void SpeedUp()
        {
            textSpeed *= speedMultiplier;
        }

        private void ShowDialogue(DialogueData data)
        {
            Clear();
            DeselectAllButtons();
            ShowChoicesIfAny(data.Choices);
            StartCoroutine(TypeLine(data.Line));
        }

        private void ShowChoicesIfAny(List<Choice> dataChoices)
        {
            foreach (var choiceButton in choiceButtons)
                choiceButton.gameObject.SetActive(false);
            choicesCanvasGroup.alpha = 0;
                        
            if (dataChoices.Count <= 0 || dataChoices.Count > choiceButtons.Count)
            {
                return;
            }
            
            choicesCanvasGroup.alpha = 1;
            for (int i = 0; i < dataChoices.Count; i++)
            {
                DialogueChoiceButton choiceButton = choiceButtons[i];
                choiceButton.gameObject.SetActive(true);
                choiceButton.ShowChoice(dataChoices[i]);
                if (i == 0)
                {
                    choiceButton.Select();
                }
            }
        }

        private IEnumerator TypeLine(string line)
        {
            _textBeingDisplayed = true;
            foreach (var c in line)
            {
                bodyText.text += c;
                if (c == ' ') 
                    continue;
            
                yield return new WaitForSeconds(1/textSpeed);
            }
            _textBeingDisplayed = false;
            textSpeed = _originalTextSpeed;
        }

        private void Clear()
        {
            bodyText.text = "";
            titleText.text = "";
        }

        public override void Show()
        {
            canvasGroup.alpha = 1;
        }

        public override void Hide()
        {
            canvasGroup.alpha = 0;
        }

        private void OnDialogueCompleted()
        {
            ActionFinished?.Invoke();
        }
    }
}