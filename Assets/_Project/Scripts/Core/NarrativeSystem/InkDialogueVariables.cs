﻿using System.Collections.Generic;
using Ink.Runtime;
using UnityEngine;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class InkDialogueVariables
    {
        private Dictionary<string, Ink.Runtime.Object> variables;

        public InkDialogueVariables(Story story)
        {
            variables = new Dictionary<string, Ink.Runtime.Object>();
            foreach (var name in story.variablesState)
            {
                Ink.Runtime.Object value = story.variablesState.GetVariableWithName(name);
                variables.Add(name, value);
                Debug.Log($"Initialized global dialogue variables: {name} = {value}");
            }
        }

        public void SyncVariablesAndStartListening(Story story)
        {
            SyncVariablesToStory(story);
            story.variablesState.variableChangedEvent += UpdateVariableState;
        }

        public void StopListening(Story story)
        {
            story.variablesState.variableChangedEvent -= UpdateVariableState;
        }

        /// <summary>
        /// Updates the in-game runtime variable to a new value. Mostlikely due to a change in the variable inside the
        /// story.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void UpdateVariableState(string name, Ink.Runtime.Object value)
        {
            if (!variables.ContainsKey(name))
            {
                return;
            }   
            variables[name] = value;
            Debug.Log($"Updated global dialogue variables: {name} = {value}");
        }

        private void SyncVariablesToStory(Story story)
        {
            foreach (var variable in variables)
            {
                story.variablesState.SetGlobal(variable.Key, variable.Value);
            }
        }
    }
}