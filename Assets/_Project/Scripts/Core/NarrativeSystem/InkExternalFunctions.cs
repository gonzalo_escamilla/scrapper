﻿using _Project.Scripts.GameServices;
using Ink.Runtime;
using UnityEngine;

namespace _Project.Scripts.Core.NarrativeSystem
{
    public class InkExternalFunctions
    {
        public void Bind(Story story)
        {
            story.BindExternalFunction(nameof(SpawnScrap), (string scrapRarity) => SpawnScrap(scrapRarity));
        }

        public void Unbind(Story story)
        {
            story.UnbindExternalFunction(nameof(SpawnScrap));
        }
        
        private void SpawnScrap(string rarity)
        {
            Debug.Log($"Should spawn a {rarity} scrap.");
        }
    }
}