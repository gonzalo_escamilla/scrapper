﻿using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Core.CoreGUI
{
    public class RestartMenu : MonoBehaviour
    {
        [SerializeField] private CanvasGroup canvasGroup;
    
        public void Initialize()
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }

        public void Show()
        {
            canvasGroup.DOFade(1, 0.25f);
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
        }
        
        public void Hide()
        {
            canvasGroup.DOFade(0, 0.25f);
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }
    }
}