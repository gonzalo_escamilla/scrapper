﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace _Project.Scripts.Core.CoreGUI
{
    public class Compass : MonoBehaviour
    {
        [SerializeField] private RectTransform compassBarTransform;
        [SerializeField] private RectTransform objectiveMarkerTransform;
        [SerializeField] private RectTransform northMarkerTransform;
        [SerializeField] private RectTransform southMarkerTransform;
        [SerializeField] private RectTransform eastMarkerTransform;
        [SerializeField] private RectTransform westMarkerTransform;
        [SerializeField] private Transform objectiveTransform;
        [SerializeField] private int widthFactor = 2;
        
        [Title("Intermidiate")]
        [SerializeField] private RectTransform northEastMarkerTransform;
        [SerializeField] private RectTransform northWestMarkerTransform;
        [SerializeField] private RectTransform southEastMarkerTransform;
        [SerializeField] private RectTransform southWestMarkerTransform;
        
        private Camera _mainCamera;

        private void Awake()
        {
            _mainCamera = Camera.main;
        }

        private void Update()
        {
            if (_mainCamera == null)
            {
                _mainCamera = Camera.main;
                return;
            }
            
            SetMarkerPosition(northMarkerTransform, Vector3.forward * 10000);
            SetMarkerPosition(southMarkerTransform, Vector3.back * 10000);
            SetMarkerPosition(eastMarkerTransform, Vector3.right * 10000);
            SetMarkerPosition(westMarkerTransform, Vector3.left * 10000);
            
            SetMarkerPosition(northEastMarkerTransform, Vector3.forward * 10000 + Vector3.right * 10000);
            SetMarkerPosition(northWestMarkerTransform, Vector3.forward * 10000 + Vector3.left * 10000);
            SetMarkerPosition(southEastMarkerTransform, Vector3.back * 10000 + Vector3.right * 10000);
            SetMarkerPosition(southWestMarkerTransform, Vector3.back * 10000 + Vector3.left * 10000);
        }

        private void SetMarkerPosition(RectTransform markerTransform, Vector3 worldPosition)
        {
            Vector3 directionToTarget = worldPosition - _mainCamera.transform.position;
            float angle = Vector2.SignedAngle(new Vector2(directionToTarget.x, directionToTarget.z), new Vector2(_mainCamera.transform.forward.x, _mainCamera.transform.forward.z));
            float compassPositionX = Mathf.Clamp(2*angle/_mainCamera.fieldOfView, -1, 1);
            markerTransform.anchoredPosition = new Vector2(compassBarTransform.rect.width/widthFactor * compassPositionX, 0);
        }
    }
}