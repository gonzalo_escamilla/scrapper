﻿using System;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core.CoreGUI
{
    public class GamePlayMenuView : GameMenuBase
    {
        [SerializeField] private HUD hud;
        [SerializeField] private RestartMenu restartMenu;

        private IFadeScreenService _fadeScreenService;
        private IInputProvider _inputProvider;
        
        private bool _canPressKey;

        private void Awake()
        {
            _inputProvider = Services.Get<IInputProvider>();
        }

        public override void Initialize()
        {
            base.Initialize();

            _fadeScreenService = Services.Get<IFadeScreenService>();
                
            hud.Initialize();
            restartMenu.Initialize();
            
            _inputProvider.AnyButtonPressed += OnAnyButtonPressed;
            _canPressKey = false;
        }
        
        public override void Show()
        {
            gameObject.SetActive(true);
            
            hud.Show();
        }

        public override void Hide()
        {
            hud.Hide();
            restartMenu.Hide();
            
            gameObject.SetActive(false);
        }

        public void ShowHUD()
        {
            hud.Show();
        }
        
        public void HideHUD()
        {
            hud.Hide();
        }
        public void ShowRestartMenu()
        {
            _fadeScreenService.FadeTo(0.94f, 1f, () => _canPressKey = true); // TODO: Remove magical value
            restartMenu.Show();
        }

        private void OnAnyButtonPressed()
        {
            if (!_canPressKey)
            {
                return;
            }
            _canPressKey = false;
            
            _inputProvider.AnyButtonPressed -= OnAnyButtonPressed;
            EventManager.Shared.Raise<GameEvents.GameStateEvents.RestartRunTriggered>();
        }

        public void HideRestartMenu()
        {
            _fadeScreenService.Transition(ScreenTransitionType.In);
            restartMenu.Hide();
        }
    }
}