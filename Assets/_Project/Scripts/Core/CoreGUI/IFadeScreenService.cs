﻿using System;
using System.Collections;

namespace _Project.Scripts.Core.CoreGUI
{
    public enum ScreenTransitionType
    {
        In,
        Out
    }
    
    public interface IFadeScreenService
    {
        void Transition(ScreenTransitionType type, Action callback = null);
        public void FadeTo(float value, float duration = 1, Action callback = null);
    }
}