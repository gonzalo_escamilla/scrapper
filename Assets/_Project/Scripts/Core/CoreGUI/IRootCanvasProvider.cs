﻿using UnityEngine;

namespace _Project.Scripts.Core.CoreGUI
{
    public interface IRootCanvasProvider
    {
        public Transform CanvasRoot { get; }
        public void AddNewChild(GameObject newChild);
    }
}