using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Core.CoreGUI
{
    [CreateAssetMenu(menuName = "Scrapper/Create HUD Settings", fileName = "HUD Settings", order = 0)]
    public class HUDSettings : ScriptableObject
    {
        [SerializeField] private float healthSizeFactor;
        [SerializeField] private List<HUDHealthColorTier> _tiers;

        [Title("Health Bar Settings")]
        [SerializeField] private float healthBarFillSpeedOnExchange;
        [SerializeField] private float healthBarDelayOnExchange;
        [SerializeField] private Ease healthBarEase;
        
        public float HealthSizeFactor => healthSizeFactor;

        public float HealthBarFillSpeedOnExchange => healthBarFillSpeedOnExchange;
        public float HealthBarDelayOnExchange => healthBarDelayOnExchange;
        public Ease HealthBarEase => healthBarEase;
        
        public Color GetTierColor(float healthValue)
        {
            return _tiers.First(t => t.Threshold <= healthValue).Color;
        }
    }

    [System.Serializable]
    public struct HUDHealthColorTier
    {
        public float Threshold;
        public Color Color;
    }
}