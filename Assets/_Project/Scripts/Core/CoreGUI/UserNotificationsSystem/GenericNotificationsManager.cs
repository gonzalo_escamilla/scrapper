using _Project.Scripts.Core.EventsSystem;
using Enderlook.EventManager;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Core.UserNotificationsSystem
{
    public class GenericNotificationsManager : MonoBehaviour
    {
        [SerializeField, Required] private PopupController _popupController;
        
        private void OnEnable()
        {
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.ScrapGrabbed>(OnScrapGrabbed);
        }

        private void OnDisable()
        {
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.ScrapGrabbed>(OnScrapGrabbed);
        }

        private void OnScrapGrabbed(GameEvents.CharacterEvents.ScrapGrabbed data)
        {
            Debug.Log($"Scrap grabbed of type {data.GrabbedScrapItem.Rarity}");
            
            string popupText = $"Scrap: <color={data.GrabbedScrapItem.GetRarityColor()}>{data.GrabbedScrapItem.Rarity}</color>";
            _popupController.AddPopup(popupText);
        }
    }
}
