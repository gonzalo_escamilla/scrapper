﻿using UnityEngine;

namespace _Project.Scripts.Core.CoreGUI
{
    public class RootCanvasView : MonoBehaviour, IRootCanvasProvider
    {
        [SerializeField] private Transform rootCanvas;

        public Transform CanvasRoot => rootCanvas;
        public void AddNewChild(GameObject newChild)
        {
            newChild.transform.SetAsFirstSibling();
        }
    }
}