﻿using System;
using UnityEngine;

namespace _Project.Scripts.Core.CoreGUI
{
    public class MapMenuView : GameMenuBase
    {
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private RectTransform marMarker;

        private bool _isMarkerActive;
        
        public override void Initialize()
        {
            base.Initialize();
            canvasGroup.alpha = 0;
            marMarker.gameObject.SetActive(false);
        }

        private void Update()
        {
            // if (Input.GetKeyDown(KeyCode.Alpha3))
            // {
            //     _isMarkerActive = !_isMarkerActive;
            // }
        }

        public override void Show()
        {
            canvasGroup.alpha = 1;
            marMarker.gameObject.SetActive(_isMarkerActive);
        }

        public override void Hide()
        {
            canvasGroup.alpha = 0;
            marMarker.gameObject.SetActive(_isMarkerActive);
        }
    }
}