﻿using System;
using System.Collections;
using _Project.Scripts.Core.EventsSystem;
using DG.Tweening;
using Enderlook.EventManager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.CoreGUI
{
    public class HUD : MonoBehaviour
    {
        [SerializeField] private HUDSettings settings;
        [SerializeField] private Image healthFillImage;
        [SerializeField] private TextMeshProUGUI healthAmountLabel;
        [SerializeField] private CanvasGroup canvasGroup;

        [SerializeField] private TextMeshProUGUI enduranceLabel;
        [SerializeField] private TextMeshProUGUI initialHealthLabel;
        [SerializeField] private TextMeshProUGUI agilityLabel;
        [SerializeField] private TextMeshProUGUI speedLabel;
        
        private float _characterInitialHealth;
        private float _characterHealth;
        private Color _currentColor;

        private void OnEnable()
        {
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.HealthLost>(OnCharacterLostHealth);
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.InitialHealthSet>(OnInitialHealthSet);
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.HealthChangedInExchange>(OnHealthChangedInExchange);
            
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.StatsChanged>(OnStatsChanged);
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.SpeedAttributeChanged>(OnSpeedAttributeChanged);
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.HealthAttributeChanged>(OnHealthAttributeChanged);
        }
        
        private void OnDisable()
        {
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.HealthLost>(OnCharacterLostHealth);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.InitialHealthSet>(OnInitialHealthSet);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.HealthChangedInExchange>(OnHealthChangedInExchange);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.StatsChanged>(OnStatsChanged);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.SpeedAttributeChanged>(OnSpeedAttributeChanged);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.HealthAttributeChanged>(OnHealthAttributeChanged);
        }

        public void Initialize()
        {
            canvasGroup.alpha = 0;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }

        public void Show()
        {
            canvasGroup.DOFade(1, 0.25f);
        }
        
        public void Hide()
        {
            canvasGroup.DOFade(0, 0.25f);
        }
        
        private void OnInitialHealthSet(GameEvents.CharacterEvents.InitialHealthSet healthData)
        {
            _characterInitialHealth = healthData.CurrentHealth;
            _characterHealth = _characterInitialHealth;
            
            var health = _characterInitialHealth * settings.HealthSizeFactor;
            
            healthFillImage.rectTransform.sizeDelta = new Vector2(health, healthFillImage.rectTransform.sizeDelta.y);
            SetHealthLabel(_characterInitialHealth);
        }

        private void OnCharacterLostHealth(GameEvents.CharacterEvents.HealthLost healthData)
        {
            _characterHealth = healthData.CurrentHealth;

            var health = healthData.CurrentHealth * settings.HealthSizeFactor;
            healthFillImage.rectTransform.sizeDelta = new Vector2(health, healthFillImage.rectTransform.sizeDelta.y);
            CheckForColor();
            SetHealthLabel(_characterHealth);
        }
        
        private void OnHealthChangedInExchange(GameEvents.CharacterEvents.HealthChangedInExchange data)
        {
            float finalHealth = _characterHealth + data.AddedHealth;
            var barSizeTarget = finalHealth * settings.HealthSizeFactor;
            Vector2 sizeDeltaTarget = new Vector2(barSizeTarget, healthFillImage.rectTransform.sizeDelta.y);
            healthFillImage.rectTransform.DOSizeDelta(sizeDeltaTarget, settings.HealthBarFillSpeedOnExchange).SetEase(settings.HealthBarEase);
            
            _characterHealth = finalHealth;
            CheckForColor();
            SetHealthLabel(finalHealth);
        }
        
        private void CheckForColor()
        {
            var color = settings.GetTierColor(_characterHealth);

            if (_currentColor == color)
            {
                return;
            }
            
            healthFillImage.color = color;
        }

        private void SetHealthLabel(float healthAmount)
        {
            healthAmountLabel.text = ((int)healthAmount).ToString();
        }
        
        private void OnHealthAttributeChanged(GameEvents.CharacterEvents.HealthAttributeChanged data)
        {
            string initialHealthText = $"Initial Health: {data.Health:F1}";
            initialHealthLabel.text = initialHealthText;
        }

        private void OnSpeedAttributeChanged(GameEvents.CharacterEvents.SpeedAttributeChanged data)
        {
            string speedText = $"Speed: {data.Speed:F1}";
            speedLabel.text = speedText;
        }

        private void OnStatsChanged(GameEvents.CharacterEvents.StatsChanged data)
        {
            string enduranceText = $"Endurance: {data.Stats.Endurance:F1}";
            enduranceLabel.text = enduranceText;
            
            string agilityText = $"Agility: {data.Stats.Agility:F1}";
            agilityLabel.text = agilityText;
        }
    }
}