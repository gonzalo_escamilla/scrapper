﻿using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.GameServices;

namespace _Project.Scripts.Core.GameStates
{
    public abstract class GameState
    {
        protected GameStateController _controller;
        protected IMenuInstanceProvider _menuInstanceProvider;
        protected IGameSettingsProvider _gameSettingsProvider;
        protected CoroutineService _coroutineService;
        protected IDebug _debug;
        protected object _metadata;
        
        protected GameState(GameStateController controller, object metadata)
        {
            _controller = controller;
            _menuInstanceProvider = Services.Get<IMenuInstanceProvider>();
            _debug = Services.Get<IDebug>();
            _gameSettingsProvider = Services.Get<IGameSettingsProvider>();
            _coroutineService = Services.Get<CoroutineService>();
            _metadata = metadata;
        }

        public abstract void Enter();
        public abstract void Exit();

        public virtual void Update() { }
        
        /// <summary>
        /// This state should dispose and shutdown all resources
        /// </summary>
        protected virtual void Dispose(){ }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}