﻿using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.Player;
using _Project.Scripts.Core.ScavengeSystem;
using Enderlook.EventManager;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Core.GameStates
{
    public class Scavenging : GameState
    {
        private ScrapperController _scrapperController;
        private PlayerController _playerController;
        private ScavengeProcessController _processController;
        private ScrapLevelObject _scrapLevelObject;
        private GamePlayMenuView _gamePlayMenuView;

        public Scavenging(GameStateController controller, object metadata) : base(controller, metadata)
        {
            _scrapperController = Object.FindObjectOfType<ScrapperController>(); // TODO: Replace for a service at some point.
            _playerController = Object.FindObjectOfType<PlayerController>();
            _processController = Object.FindObjectOfType<ScavengeProcessController>();
            
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterDied>(OnCharacterDied);
            EventManager.Shared.Subscribe<GameEvents.GameStateEvents.RestartRunTriggered>(RestartRunTriggeredAfterPlayerDied);

            _gamePlayMenuView = _menuInstanceProvider.GetMenuInstance<GamePlayMenuView>();

            _scrapLevelObject = metadata as ScrapLevelObject;
            if (_scrapLevelObject == null)
            {
                _debug.LogError("DebrisData is null");
            }
        }

        public override void Enter()
        {
            _playerController.SetCharacterInputActive(false);
            _scrapperController.StopInputs();
            _scrapperController.IsOnScavengingProcess = true;
            
            _processController.SetData(_scrapLevelObject.debrisData);
            _processController.ScavengeCompleted += OnScavengeCompleted;
            _processController.UserExitScavenge += OnUserExitScavenge;
        }
        
        public override void Exit()
        {
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterDied>(OnCharacterDied);
            EventManager.Shared.Unsubscribe<GameEvents.GameStateEvents.RestartRunTriggered>(RestartRunTriggeredAfterPlayerDied);
            
            _playerController.SetCharacterInputActive(true);
            _scrapperController.EnableInputs();
            _scrapperController.IsOnScavengingProcess = false;
        }
        
        private void OnScavengeCompleted()
        {
            _processController.ScavengeCompleted -= OnScavengeCompleted;
            _scrapLevelObject.ScrapperLeaved();
            _controller.SwitchState<Gameplay>();
        }

        private void OnUserExitScavenge()
        {
            _processController.UserExitScavenge -= OnUserExitScavenge;
            _scrapLevelObject.ScrapperLeaved();
            _controller.SwitchState<Gameplay>();
        }
        
        private void OnCharacterDied()
        {
            _scrapLevelObject.ScrapperLeaved();
            _gamePlayMenuView.HideHUD();
            _gamePlayMenuView.ShowRestartMenu();
            _processController.Release();
        }
        
        private void RestartRunTriggeredAfterPlayerDied()
        {
            _gamePlayMenuView.Hide();
            _controller.SwitchState<StartRunForTheFirstTime>();
        }
    }
}