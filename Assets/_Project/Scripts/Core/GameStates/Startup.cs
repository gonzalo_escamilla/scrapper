﻿using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.GameServices;
using Cysharp.Threading.Tasks;

namespace _Project.Scripts.Core.GameStates
{
    /// <summary>
    /// Handles, default login, game splash screen and any other feature before main menu.
    /// </summary>
    public class Startup : GameState
    {
        private StartupMenuView _startupMenu;
        private ISceneLoader _sceneLoader;
        
        public Startup(GameStateController controller, object metadata) : base(controller, metadata)
        {
            _sceneLoader = Services.Get<ISceneLoader>();
        }

        public override void Enter()
        {
            Load().Forget();
        }

        private async UniTaskVoid Load()
        {
            await _sceneLoader.LoadSceneAsyncAdditive(1);
            if ( _sceneLoader.IsThereAnyLoadedPlayableScene())
            {
                _controller.SwitchState<StartRunForTheFirstTime>();
                return;
            }
            
            await _sceneLoader.LoadSceneAsyncAdditive(2);
            _controller.SwitchState<StartRunForTheFirstTime>();
        }

        public override void Exit() { }
        
        protected override void Dispose() { }
    }
}