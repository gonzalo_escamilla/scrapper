﻿using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.GameServices;
// using Cysharp.Threading.Tasks;

namespace _Project.Scripts.Core.GameStates
{
    public class LoadingLevel : GameState
    {
        private ISceneLoader _sceneLoader;
        private IFadeScreenService _fadeScreen;
        
        public LoadingLevel(GameStateController controller, object metadata) : base(controller, metadata)
        {
            _sceneLoader = Services.Get<ISceneLoader>();
            _fadeScreen = Services.Get<IFadeScreenService>();
        }

        public override void Enter()
        {
            // LoadLevel().Forget();
        }
        public override void Exit() { }

        // private async UniTaskVoid LoadLevel()
        // {
        //     await _sceneLoader.LoadSceneAsync(_gameSettings.LevelSceneIndex);
        //     _gameAudio.PlayMainSong();
        //     _screenTransition.Transition(ScreenTransitionType.In, Exit);
        // }
        //
        public override void Update()
        {
        }

        protected override void Dispose()
        {
            _controller.SwitchState<Gameplay>();
        }
    }
}