﻿using System;
using System.Collections;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.InventorySystem;
using _Project.Scripts.Core.Player;
using Enderlook.EventManager;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Core.GameStates
{
    public class ExitZoneExchange : GameState
    {
        private ExitZone _exitZone;
        private LevelSettings _levelSettings;
        private ScrapperController _scrapperController;
        private InventoryController _inventoryController;
        
        public ExitZoneExchange(GameStateController controller, object metadata) : base(controller, metadata)
        {
            TryGetExitZone();

            _levelSettings = _gameSettingsProvider.Get<LevelSettings>();
            _scrapperController = Object.FindObjectOfType<ScrapperController>();
            _inventoryController = Object.FindObjectOfType<InventoryController>();
        }

        private void TryGetExitZone()
        {
            try
            {
                _exitZone = _metadata as ExitZone;
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Invalid conversion: {e}");
                throw;
            }
        }
        
        public override void Enter()
        {
            _scrapperController.StopInputs();
            _scrapperController.SetPosition(_exitZone.Center);

            _coroutineService.StartCoroutine(this, WaitToChangeHealth());
        }

        private IEnumerator WaitToChangeHealth()
        {
            yield return new WaitForSeconds(2f); // TODO: Late remove magic number
            var allScrapGathered = _inventoryController.GetAndClearAllScrap();
            float totalHealthToAdd = _levelSettings.GetAllHealthFrom(allScrapGathered);

    // _scrapperController.AddHealth(totalHealthToAdd); //TODO: Just for the prototype.
            // EventManager.Shared.Raise(new GameEvents.CharacterEvents.HealthChangedInExchange(totalHealthToAdd));
            
            _controller.SwitchState<RestartRun>();
        }
        
        public override void Exit() { }
    }
}