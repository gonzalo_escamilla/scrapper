﻿using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.Player;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core.GameStates
{
    public class RestartRun : GameState
    {
        private IFadeScreenService _fadeScreen;
        private PlayerController _playerController;
        private CoroutineService _coroutineService;
        private LevelController _levelController;
        private ScrapperController _scrapperController;
        
        public RestartRun(GameStateController controller, object metadata) : base(controller, metadata)
        {
            _fadeScreen = Services.Get<IFadeScreenService>();
            _coroutineService = Services.Get<CoroutineService>();

            _playerController = Object.FindObjectOfType<PlayerController>(); // TODO: Later we can add providers for this..
            _scrapperController = Object.FindObjectOfType<ScrapperController>(); // TODO: Later we can add providers for this..
            _levelController = Services.Get<LevelController>(); // TODO: Later we can add providers for this..
        }

        public override void Enter()
        {
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);

            _fadeScreen.Transition(ScreenTransitionType.Out, OnFadeOutComplete);
        }
        
        public override void Exit()
        {
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);
        }
        
        private void OnCharacterLeavedExitZone()
        {
            _controller.SwitchState<Gameplay>();
        }
        
        private void OnFadeOutComplete()
        {
            _levelController.Setup();
            
            _playerController.Initialize();
            _playerController.SetCharacterInputActive(true);
            
            _scrapperController.SetPosition(_levelController.GetSpawnPoint());
            
            _fadeScreen.Transition(ScreenTransitionType.In);
        }
    }
}