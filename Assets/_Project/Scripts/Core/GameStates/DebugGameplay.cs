﻿using System;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.Player;
using _Project.Scripts.GameServices;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Core.GameStates
{
    public class DebugGameplay : GameState
    {
        private IFadeScreenService _fadeScreen;
        private PlayerController _playerController;
        private ScrapperController _scrapperController;
        
        private Vector3 _spawnPoint;
        
        public DebugGameplay(GameStateController controller, object metadata) : base(controller, metadata)
        {
            try
            {
                _fadeScreen = Services.Get<IFadeScreenService>();
                _coroutineService = Services.Get<CoroutineService>();
            }
            catch (Exception e)
            {
                _debug.LogError("Base scene is mandatory to play the game!");
            }
            
            _playerController = Object.FindObjectOfType<PlayerController>(); // TODO: Later we can add providers for this..
            _scrapperController = Object.FindObjectOfType<ScrapperController>(); // TODO: Later we can add providers for this..
            
            if (!_playerController || !_scrapperController)
            {
                _debug.LogError("Character controller and player controller and mandatory to play the game even in debug mode!");
            }

            var characterInitialPositions = GameObject.FindGameObjectsWithTag("PlayerInitialPosition");
            if (characterInitialPositions == null || characterInitialPositions.Length == 0)
            {
                _debug.LogError("No spawn point found. Using current character position");
                _spawnPoint = _scrapperController.transform.position;
                return;
            }
            
            _spawnPoint = characterInitialPositions[0].transform.position;
        }

        public override void Enter()
        {
            var menu = _menuInstanceProvider.GetMenuInstance<GamePlayMenuView>();
            menu.Show();
            
            _fadeScreen.FadeTo(0, 0, OnFadeCompleted);
        }

        private void OnFadeCompleted()
        {
            _playerController.Initialize();
            _playerController.SetCharacterInputActive(true);

            _scrapperController.IsDebugRun = true;
            _scrapperController.Setup();
            _scrapperController.SetPosition(_spawnPoint);
        }
        
        public override void Exit()
        {
        }
    }
}