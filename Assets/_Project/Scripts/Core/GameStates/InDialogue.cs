﻿using System;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.NarrativeSystem;
using _Project.Scripts.Core.Player;
using _Project.Scripts.GameServices;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Core.GameStates
{
    public class InDialogue : GameState
    {
        private DialogueTriggerData _dialogueTriggerData;
        private DialogueMenuView _dialogueMenuView;
        private PlayerController _playerController;
        private ScrapperController _scrapperController;
        private DialogueController _dialogueController;
        private IInputProvider _inputProvider;
        
        public InDialogue(GameStateController controller, object metadata) : base(controller, metadata)
        {
            try
            {
                _dialogueTriggerData = (DialogueTriggerData)metadata;
                _dialogueMenuView = Services.Get<IMenuInstanceProvider>().GetMenuInstance<DialogueMenuView>();
                // TODO: It might be interesting to have this as a service. Bc its possible that we may use this controller in other in-game states or systems.
                _dialogueController = Services.Get<DialogueController>();
                _scrapperController = Object.FindObjectOfType<ScrapperController>();
                _playerController = Object.FindObjectOfType<PlayerController>();
                _inputProvider = Services.Get<IInputProvider>();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                throw;
            }
        }

        public override void Enter()
        {
            _playerController.SetCharacterInputActive(false);
            _scrapperController.StopInputs();
         
            _inputProvider.SwitchActionMap(ActionMap.UINavigation);
            
            _dialogueController.StartDialogue(_dialogueTriggerData);

            _dialogueMenuView.Show();
            _dialogueMenuView.ActionFinished += OnActionFinished;

            _inputProvider.MoveInputPerformed += OnMoveInputPerformed;
        }
        
        public override void Exit()
        {
            _inputProvider.MoveInputPerformed -= OnMoveInputPerformed;
            _inputProvider.SwitchActionMap(ActionMap.CharacterController);
        }
        
        private void OnActionFinished()
        {
            _dialogueMenuView.ActionFinished -= OnActionFinished;
            _dialogueMenuView.Hide();
            
            _playerController.SetCharacterInputActive(true);
            _scrapperController.EnableInputs();
            _controller.SwitchState<Gameplay>();
        }

        private void OnMoveInputPerformed(Vector2 direction)
        {
            _dialogueController.MoveChoiceIndex((int)direction.y);
        }
    }
}