﻿using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.Player;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core.GameStates
{
    public class Gameplay : GameState
    {
        private GamePlayMenuView _gamePlayMenuView;
        private IFadeScreenService _fadeScreen;
        private ScrapperController _scrapperController;
        
        public Gameplay(GameStateController controller, object metadata) : base(controller, metadata)
        {
            _gamePlayMenuView = _menuInstanceProvider.GetMenuInstance<GamePlayMenuView>();
            _fadeScreen = Services.Get<IFadeScreenService>();
            _scrapperController = Object.FindObjectOfType<ScrapperController>();
            
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterDied>(OnCharacterDied);
            EventManager.Shared.Subscribe<GameEvents.GameStateEvents.RestartRunTriggered>(RestartRunTriggeredAfterPlayerDied);
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.ScrapInteractionCompleted>(OnScrapInteractionCompleted);
            EventManager.Shared.Subscribe<GameEvents.Dialogue.DialogueInteractionCompleted>(OnDialogueInteractionCompleted);
        }
        
        public override void Enter()
        {
            _gamePlayMenuView.Show();
        }

        public override void Exit()
        {
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterDied>(OnCharacterDied);
            EventManager.Shared.Unsubscribe<GameEvents.GameStateEvents.RestartRunTriggered>(RestartRunTriggeredAfterPlayerDied);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.ScrapInteractionCompleted>(OnScrapInteractionCompleted);
            EventManager.Shared.Unsubscribe<GameEvents.Dialogue.DialogueInteractionCompleted>(OnDialogueInteractionCompleted);
        }
        
        public override void Update()
        {
        }

        private void OnCharacterDied()
        {
            _gamePlayMenuView.HideHUD();
            _gamePlayMenuView.ShowRestartMenu();
        }
        
        private void RestartRunTriggeredAfterPlayerDied()
        {
            _gamePlayMenuView.Hide();
            _controller.SwitchState<StartRunForTheFirstTime>();
        }
        
        private void OnCharacterEnteredExitZone(GameEvents.CharacterEvents.CharacterEnteredExitZone data)
        {
            _controller.SwitchState<ExitZoneExchange>(data.Zone);
        }
        
        private void OnScrapInteractionCompleted(GameEvents.CharacterEvents.ScrapInteractionCompleted data)
        {
            _controller.SwitchState<Scavenging>(data.ScrapLevelObject);
        }
        
        private void OnDialogueInteractionCompleted(GameEvents.Dialogue.DialogueInteractionCompleted data)
        {
            _controller.SwitchState<InDialogue>(data.DialogueTriggerData);
        }
    }
}