﻿using System;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Core.GameStates
{
    public class GameStateController : MonoBehaviour
    {
        [ShowInInspector] private string currentGameState => _currentGameState != null?_currentGameState.ToString() :"Unitialized";

        private GameState _currentGameState;
        private IDebug _debug;

        private void Awake()
        {
            EventManager.Shared.Subscribe<GameEvents.GameStateEvents.ApplicationResetTriggered>(OnApplicationResetTriggered);
        }

        private void OnDestroy()
        {
            EventManager.Shared.Unsubscribe<GameEvents.GameStateEvents.ApplicationResetTriggered>(OnApplicationResetTriggered);
        }

        public void Initialize()
        {
            _debug = Services.Get<IDebug>();
            
            StartFSM();
        }
        
        private void Update()
        {
            if (_currentGameState == null)
            {
                return;
            }
            
            _currentGameState.Update();
        }

        private void StartFSM()
        {
            SwitchState<Startup>();
        }

        private void StopFSM()
        {
            _currentGameState?.Exit();
            _currentGameState = null;
        }
        
        public void SwitchState<T>(object metadata = null) where T : GameState
        {
            _debug.Log($"GameState: Switching from {_currentGameState} to {typeof(T).Name}");
            _currentGameState?.Exit();

            _currentGameState = (T)Activator.CreateInstance(typeof(T), new object[2]{this, metadata});
            
            EventManager.Shared.Raise(new GameEvents.GameStateEvents.GameStateChanged(_currentGameState));
            _currentGameState.Enter();
        }
        
        private void OnApplicationResetTriggered()
        {
            StopFSM();
            SwitchState<ApplicationReset>();
        }
    }
}