﻿using System;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.Player;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Core.GameStates
{
    public class StartRunForTheFirstTime : GameState
    {
        private IFadeScreenService _fadeScreen;
        private PlayerController _playerController;
        private CoroutineService _coroutineService;
        private LevelController _levelController;
        private ScrapperController _scrapperController;

        private bool _uninitialized;
        
        public StartRunForTheFirstTime(GameStateController controller, object metadata) : base(controller, metadata)
        {
            try
            {
                _fadeScreen = Services.Get<IFadeScreenService>();
                _coroutineService = Services.Get<CoroutineService>();
                _levelController = Services.Get<LevelController>();
            }
            catch (Exception e)
            {
                _uninitialized = true;
                _debug.LogError("Some of the needed services are not present in the current context. Fallback to debug gameplay state");
                _controller.SwitchState<DebugGameplay>();
            }
            
            _playerController = Object.FindObjectOfType<PlayerController>(); // TODO: Later we can add providers for this..
            _scrapperController = Object.FindObjectOfType<ScrapperController>(); // TODO: Later we can add providers for this..
            
            if (!_playerController || !_scrapperController)
            {
                _uninitialized = true;
                _debug.LogError("Some of the needed components are not present in the current context. Fallback to debug gameplay state.");
                _controller.SwitchState<DebugGameplay>();
            }

        }

        public override void Enter()
        {
            if (_uninitialized)
                return;
            
            var menu = _menuInstanceProvider.GetMenuInstance<GamePlayMenuView>();
            menu.Show();
            
            _scrapperController.SetPosition(_levelController.GetRandomExitZone().Center);
            
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);

            _fadeScreen.Transition(ScreenTransitionType.Out, OnFadeOutComplete);
        }

        public override void Exit()
        {
            if (_uninitialized)
                return;
            
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);
        }
        
        private void OnCharacterLeavedExitZone()
        {
            _controller.SwitchState<Gameplay>();
        }

        private void OnFadeOutComplete()
        {
            _levelController.Setup();
            
            _playerController.Initialize();
            _playerController.SetCharacterInputActive(true);
            
            _scrapperController.Setup();
            _scrapperController.SetPosition(_levelController.GetSpawnPoint());
            
            _fadeScreen.Transition(ScreenTransitionType.In);
        }
    }
}