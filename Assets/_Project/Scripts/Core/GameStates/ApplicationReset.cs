﻿using System;
using _Project.Scripts.GameServices;
using Cysharp.Threading.Tasks;

namespace _Project.Scripts.Core.GameStates
{
    public class ApplicationReset : GameState
    {
        private ISceneLoader _sceneLoader;
        private CoroutineService _coroutineService;
        private IInputProvider _inputProvider;
        
        public ApplicationReset(GameStateController controller, object metadata) : base(controller, metadata)
        {
            _sceneLoader = Services.Get<ISceneLoader>();
            _coroutineService = Services.Get<CoroutineService>();
            _inputProvider = Services.Get<IInputProvider>();
        }
        
        public override void Enter()
        {
            Load().Forget();
        }

        private async UniTaskVoid Load()
        {
            await _sceneLoader.UnloadAllScenes();
            await UniTask.Delay(TimeSpan.FromSeconds(0.5f));
            _coroutineService.Dispose();
            _inputProvider.Reset();
            await UniTask.Delay(TimeSpan.FromSeconds(2f));
            _controller.SwitchState<Startup>();
        }

        public override void Exit() { }
        
        protected override void Dispose() { }
    }
}