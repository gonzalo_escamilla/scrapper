using System;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public class WeaponController : MonoBehaviour
    {
        [SerializeField] public WeaponDataSettings weaponDataSettings;

        [SerializeField] private Transform attackSpawnPoint;
        
        public Transform firePoint;
        [HideInInspector] public float nextFireTime;
        private bool isFiring;
        
        public event Action AttackStarted;
        public bool IsAttacking { get; set; }

        private BaseWeapon _currentWeapon;

        private void Awake()
        {
            _currentWeapon = GetWeapon(weaponDataSettings);
        }
        
        private void OnEnable()
        {
            weaponDataSettings.ValuesUpdated += Awake;
        }

        private void OnDisable()
        {
            weaponDataSettings.ValuesUpdated -= Awake;
        }

        private WeaponData GetWeaponData(WeaponDataSettings settings)
        {
            return new WeaponData(
                weaponName: settings.weaponName,
                projectilePrefab: settings.projectilePrefab,
                fireRate: settings.fireRate,
                damage: settings.damage,
                bulletSpeed: settings.bulletSpeed,
                fireMode: settings.fireMode,
                burstCount: settings.burstCount,
                chargeTime: settings.chargeTime,
                firePoint: firePoint,
                dispersionRadius: settings.dispersionRadius,
                collisionLayerMask: settings.collisionLayerMask
            );
        }

        public void StartAttacking()
        {
            _currentWeapon.StartShooting();
        }

        public void StopAttacking()
        {
            _currentWeapon.StopShooting();
        }
        
        public BaseWeapon GetWeapon(WeaponDataSettings settings)
        {
            switch (settings.fireMode)
            {
                case FireMode.SingleShot:
                    return new SimpleGun(this, GetWeaponData(settings));
                case FireMode.Automatic:
                    return new MachineGun(this, GetWeaponData(settings));
                case FireMode.Burst:
                    // return new BurstFire(burstCount);
                case FireMode.Charged:
                    // return new ChargedFire(chargeTime);
                default:
                    return new SimpleGun(this, GetWeaponData(settings));
            }
        }
    }
}