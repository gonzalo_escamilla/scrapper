﻿using _Project.Scripts.Core.EventsSystem;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public abstract class BaseWeapon
    {
        protected WeaponController _weaponController;
        protected WeaponData _weaponData;
        
        public abstract void StartShooting();
        public abstract void StopShooting();

        protected BaseWeapon(WeaponController weaponController, WeaponData weaponData)
        {
            _weaponController = weaponController;
            _weaponData = weaponData;
        }

        protected void ShotBullet()
        {
            Bullet bullet = Object.Instantiate( _weaponData.ProjectilePrefab,
                _weaponData.FirePoint.position,
                _weaponController.firePoint.rotation);
                
            bullet.Initialize(_weaponData.BulletSpeed, _weaponData.Damage, _weaponData.CollisionLayerMask, _weaponData.DispersionRadius);
            EventManager.Shared.Raise<GameEvents.CombatEvents.BulletFired>();
        }

        protected void Recharge() { }
    }
}