﻿using System.Collections;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public class MachineGun : BaseWeapon
    {
        private bool _isShooting;
        public MachineGun(WeaponController weaponController, WeaponData weaponData) : base(weaponController, weaponData) { }

        public override void StartShooting()
        {
            if (!_isShooting)
            {
                _isShooting = true;
                _weaponController.StartCoroutine(FireContinuously());
            }
        }

        public override void StopShooting()
        {
            _isShooting = false;
        }
        
        private IEnumerator FireContinuously()
        {
            while (_isShooting)
            {
                if (Time.time >= _weaponController.nextFireTime)
                {
                    _weaponController.nextFireTime = Time.time + 1f / _weaponData.FireRate;
                    ShotBullet();
                }
                yield return null;
            }
        }
    }
}