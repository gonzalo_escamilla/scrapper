﻿using _Project.Scripts.Core.DamageSystem;
using _Project.Scripts.Core.EventsSystem;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core
{
    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : MonoBehaviour
    {
        private AttackData DamageData => new AttackData(this.gameObject, transform.forward, _damage);

        private Rigidbody _rb;
        private LayerMask _collisionLayerMask;
        
        private float _speed;
        private float _lifetime = 5f;
        private int _damage = 10;

        public void Initialize(float bulletSpeed, int bulletDamage, LayerMask collisionLayer, float dispersion)
        {
            _speed = bulletSpeed;
            _damage = bulletDamage;
            _rb = GetComponent<Rigidbody>();
            _collisionLayerMask = collisionLayer;

            Vector3 randomOffset = Vector3.Cross(transform.forward, Vector3.up).normalized * Random.Range(-dispersion, dispersion);
            Vector3 direction = (transform.forward + randomOffset).normalized;
            
            _rb.velocity = direction * _speed;
            Destroy(gameObject, _lifetime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (((1 << other.gameObject.layer) & _collisionLayerMask) == 0) 
                return;
    
            IDamageable target = other.GetComponent<IDamageable>();
            if (target != null)
            {
                target.GetHit(DamageData);
                EventManager.Shared.Raise<GameEvents.CombatEvents.BulletHitEnemy>();
                Destroy(gameObject); // TODO: Implement pool
            }
            else if (!other.isTrigger)
            {
                EventManager.Shared.Raise<GameEvents.CombatEvents.BulletHitGround>();
                Destroy(gameObject);
            }
        }
    }
}