﻿using UnityEngine;

namespace _Project.Scripts.Core
{
    public class SimpleGun : BaseWeapon
    {
        public SimpleGun(WeaponController weaponController, WeaponData weaponData) : base(weaponController, weaponData) { }

        public override void StartShooting()
        {
            if (Time.time >= _weaponController.nextFireTime)
            {
                _weaponController.nextFireTime = Time.time + 1f / _weaponData.FireRate;
                ShotBullet();
            }
        }
        
        public override void StopShooting() { }
    }
}