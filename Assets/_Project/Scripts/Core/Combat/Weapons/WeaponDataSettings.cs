using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public enum FireMode
    {
        SingleShot,      // Fires once per trigger
        Automatic,       // Fires continuously while pressed
        Burst,           // Fires burst of bullets
        Charged          // Fires after charge release
    }

    public class WeaponData
    {
        public string WeaponName;
        public Bullet ProjectilePrefab;
        public float FireRate;
        public int Damage;
        public float BulletSpeed;
        public FireMode FireMode;
        public int BurstCount;
        public float ChargeTime;
        public Transform FirePoint;
        public float DispersionRadius;
        public LayerMask CollisionLayerMask;

        public WeaponData(string weaponName, 
            Bullet projectilePrefab, 
            float fireRate, 
            int damage, 
            float bulletSpeed, 
            FireMode fireMode, 
            int burstCount, 
            float chargeTime, 
            Transform firePoint,
            float dispersionRadius,
            LayerMask collisionLayerMask)
        {
            WeaponName = weaponName;
            ProjectilePrefab = projectilePrefab;
            FireRate = fireRate;
            Damage = damage;
            BulletSpeed = bulletSpeed;
            FireMode = fireMode;
            BurstCount = burstCount;
            ChargeTime = chargeTime;
            FirePoint = firePoint;
            DispersionRadius = dispersionRadius;
            CollisionLayerMask = collisionLayerMask;
        }
    }
    
    [CreateAssetMenu(menuName = "Scrapper/Create Character Weapon Settings", fileName = "CharacterWeaponSettings", order = 0)]
    public class WeaponDataSettings : ScriptableObject
    {
        public Action ValuesUpdated;
        
        public string weaponName;
        public Bullet projectilePrefab;
        public float fireRate;
        public int damage;
        public float bulletSpeed;
        public FireMode fireMode;
        public int burstCount; // Only for Burst
        public float chargeTime; // Only for Charged
        [Range(0,1f)] public float dispersionRadius;
        public LayerMask collisionLayerMask;
        
        private void OnValidate()
        {
            ValuesUpdated?.Invoke();
        }
    }
}