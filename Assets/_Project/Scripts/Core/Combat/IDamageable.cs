﻿using _Project.Scripts.Core.DamageSystem;

namespace _Project.Scripts.Core
{
    public interface IDamageable
    {        
        public void GetHit(AttackData damageAmount);
    }
}