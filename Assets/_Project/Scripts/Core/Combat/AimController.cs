﻿using System;
using _Project.Scripts.Core.EventsSystem;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core.Combat
{
    public class AimController : MonoBehaviour
    {
        [SerializeField] private Transform center;
        [SerializeField] private Transform aimTransform;
        [SerializeField] private float aimDistance;
        [SerializeField] private float aimFireVariation = 0.2f;
        [SerializeField] private float returnToOriginalSizeSpeed = 1f;
        
        private Vector3 _aimInitialSize;

        private void Awake()
        {
            _aimInitialSize = aimTransform.localScale;
            EventManager.Shared.Subscribe<GameEvents.CombatEvents.BulletFired>(OnBulletFired);
        }

        private void OnDestroy()
        {
            EventManager.Shared.Unsubscribe<GameEvents.CombatEvents.BulletFired>(OnBulletFired);
        }
        
        private void Update()
        {
            aimTransform.position = center.position + aimTransform.forward * aimDistance;
            
            Vector3 desiredScale = Vector3.Lerp(aimTransform.localScale, _aimInitialSize, Time.deltaTime * returnToOriginalSizeSpeed);
            aimTransform.localScale = desiredScale;
        }
        
        private void OnBulletFired()
        {
            aimTransform.localScale += (Vector3.one * aimFireVariation);
        }
    }
}