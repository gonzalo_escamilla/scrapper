using UnityEngine;

namespace _Project.Scripts.Core.UI
{
    public class Billboard : MonoBehaviour
    {
        private Camera _main;

        [Header("Axis Tracking")]
        [SerializeField] private bool trackX = true;
        [SerializeField] private bool trackY = true;
        [SerializeField] private bool trackZ = true;

        private void Awake()
        {
            _main = Camera.main;
        }

        protected virtual void LateUpdate()
        {
            if (_main == null)
                return;

            // Get the camera's forward direction
            Vector3 cameraForward = _main.transform.forward;

            // Zero out axes that are not being tracked
            cameraForward.x = trackX ? cameraForward.x : 0f;
            cameraForward.y = trackY ? cameraForward.y : 0f;
            cameraForward.z = trackZ ? cameraForward.z : 0f;

            // Apply the LookAt with the adjusted forward direction
            transform.LookAt(transform.position + cameraForward);
        }
    }
}