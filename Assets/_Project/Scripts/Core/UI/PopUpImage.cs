﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.UI
{
    public class PopUpImage : MonoBehaviour
    {
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Image image;
        [SerializeField] private bool showOnStart;
        [SerializeField] private float fadeDuration;
        [SerializeField] private float moveSpeed;
        [SerializeField] private Ease moveEase;
        [SerializeField] private RectTransform endPosition;
        [SerializeField] private RectTransform initialPosition;
        
        private void Awake()
        {
            canvasGroup.alpha = showOnStart ? 1 : 0;
        }

        public void SetData(Sprite sprite)
        {
            image.sprite = sprite;
        }

        [Button("Show")]
        public void Show()
        {
            canvasGroup.DOFade(1, fadeDuration);
            image.rectTransform.anchoredPosition = initialPosition.anchoredPosition;
            image.rectTransform
                .DOAnchorPosY(endPosition.anchoredPosition.y, moveSpeed)
                .SetSpeedBased()
                .SetEase(moveEase);
        }

        [Button("Hide")]
        public void Hide()
        {
            canvasGroup.DOFade(0, fadeDuration);
            image.rectTransform.anchoredPosition = endPosition.anchoredPosition;
            image.rectTransform
                .DOAnchorPosY(initialPosition.anchoredPosition.y, moveSpeed)
                .SetSpeedBased()
                .SetEase(moveEase);
        }
    }
}