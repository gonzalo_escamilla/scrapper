using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

public class PopupSlot : MonoBehaviour
{
    [SerializeField, Required] private TextMeshProUGUI popupText;

    public void Initialize(string text)
    {
        popupText.text = text;
        StartCoroutine(AutoDestroy());
    }

    private IEnumerator AutoDestroy()
    {
        yield return new WaitForSeconds(5f);
        
        Destroy(gameObject);
    }
}
