using Sirenix.OdinInspector;
using UnityEngine;

public class PopupController : MonoBehaviour
{
    [SerializeField, Required] private PopupSlot _popupSlotPrefab;
    [SerializeField, Required] private RectTransform _popupContainer;

    public void AddPopup(string text)
    {
        PopupSlot slot = Instantiate(_popupSlotPrefab, _popupContainer);
        slot.Initialize(text);
    }
}
