﻿using System;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.InventorySystem;
using _Project.Scripts.GameServices;
using UnityEngine;

namespace _Project.Scripts.Core.Player
{
    // TODO: Will see which purpose the player has.
    
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private ScrapperController _characterController;
        [SerializeField] private InventoryController inventoryController;
        
        private LevelController _levelController;
        private IInputProvider _inputProvider;
        
        private IDebug _debug;
        private MapMenuView _mapMenuView;
        private bool _isMapOpen;
        
        public void Initialize()
        {
            _characterController ??= FindObjectOfType<ScrapperController>();
            _debug = Services.Get<IDebug>();
            _inputProvider = Services.Get<IInputProvider>();
            _mapMenuView ??= Services.Get<IMenuInstanceProvider>().GetMenuInstance<MapMenuView>();
        }

        public void ClearInputs()
        {
            _inputProvider.MoveInputStarted -= OnMoveInputStarted;
            _inputProvider.MoveInputPerformed -= OnMoveInputPerformed;
            _inputProvider.MoveInputCanceled -= OnMoveInputCanceled;
            _inputProvider.RotationPerformed -= OnRotationPerformed;
            _inputProvider.RunStarted -= OnRunStarted;
            _inputProvider.RunCanceled -= OnRunCanceled;
            _inputProvider.DashPerformed -= OnDashStarted;
            _inputProvider.ActionPerformed -= OnActionPerformed;
            _inputProvider.ActionCanceled -= OnActionCanceled;
            _inputProvider.AttackPerformed -= OnAttackPerformed;
            _inputProvider.AttackCanceled -= OnAttackCanceled;
            _inputProvider.UseUtilityStarted -= OnUseUtilityStarted;
            _inputProvider.UseUtilityCanceled -= OnUseUtilityCanceled;
            _inputProvider.MapPerformed -= OnMapPerformed;
        }
        
        public void SetCharacterInputActive(bool isControllingCharacter)
        {
            ClearInputs();
            
            if (isControllingCharacter)
            {
                _inputProvider.MoveInputStarted += OnMoveInputStarted;
                _inputProvider.MoveInputPerformed += OnMoveInputPerformed;
                _inputProvider.MoveInputCanceled += OnMoveInputCanceled;
                _inputProvider.RotationPerformed += OnRotationPerformed;
                _inputProvider.RunStarted += OnRunStarted;
                _inputProvider.RunCanceled += OnRunCanceled;
                _inputProvider.DashPerformed += OnDashStarted;
                _inputProvider.ActionPerformed += OnActionPerformed;
                _inputProvider.ActionCanceled += OnActionCanceled;
                _inputProvider.AttackPerformed += OnAttackPerformed;
                _inputProvider.AttackCanceled += OnAttackCanceled;
                _inputProvider.UseUtilityStarted += OnUseUtilityStarted;
                _inputProvider.UseUtilityCanceled += OnUseUtilityCanceled;
                _inputProvider.MapPerformed += OnMapPerformed;
            
                _characterController.EnableInputs();
                return;
            }

            ClearInputs();
            _characterController.StopInputs();
        }

        private void OnMoveInputStarted(Vector2 direction)
        {
            // _characterController.Move(direction);
        }

        private void OnMoveInputPerformed(Vector2 direction)
        {
            _characterController.Move(direction);
        }

        private void OnMoveInputCanceled(Vector2 direction)
        {
            // _characterController.Move(direction);
        }

        private void OnRotationPerformed(Vector2 direction)
        {
            _characterController.Rotate(direction);
        }
        
        private void OnRunStarted()
        {
            _characterController.Run(true);
        }

        private void OnRunCanceled()
        {            
            _characterController.Run(false);
        }

        private void OnDashStarted()
        {
            _characterController.Dash();
        }

        private void OnActionPerformed()
        {
            _characterController.HandleInteraction(true);
        }

        private void OnActionCanceled()
        {
            _characterController.HandleInteraction(false);
        }

        private void OnAttackPerformed()
        {
            _characterController.StartAttacking();
        }

        private void OnAttackCanceled()
        {
            _characterController.StopAttacking();
        }
        
        private void OnUseUtilityStarted()
        {
            _characterController.RadarStarted();
        }

        private void OnUseUtilityCanceled()
        {
            _characterController.RadarCanceled();
        }
        
        private void OnMapPerformed()
        {
            SwitchMap();
        }

        private void SwitchMap()
        {
            if (_isMapOpen)
                _mapMenuView.Hide();
            else
                _mapMenuView.Show();
            _isMapOpen = !_isMapOpen;
        }
    }
}