// using _Project.Scripts.Core.InventorySystem;
// using UnityEngine;
//
// namespace _Project.Scripts.Core.Player.PlayerStates
// {
//     public class InventoryState : PlayerState
//     {
//         private InventoryController _inventoryController;
//         
//         public InventoryState(PlayerFSMSetupData _fsmSetupData, object data = null) : base(_fsmSetupData, data)
//         {
//             _inventoryController = _fsmSetupData.InventoryController;
//         }
//         
//         public override void Enter()
//         {
//             _inventoryController.Show();
//             _inputProvider.InventoryStarted += OnInventoryActionStarted;
//             _inputProvider.MoveInputPerformed += OnMoveInputStarted;
//             _inputProvider.AttackStarted += RotateCurrentGrabbedItem;
//             _inputProvider.ActionStarted += HandleGrabDrop;
//             _inputProvider.InventorySwitchStarted += HandleStashInventorySwitch;
//         }
//         
//         public override void Exit()
//         {
//             base.Exit();
//             _inputProvider.InventoryStarted -= OnInventoryActionStarted;
//             _inputProvider.MoveInputPerformed -= OnMoveInputStarted;
//             _inputProvider.AttackStarted -= RotateCurrentGrabbedItem;
//             _inputProvider.ActionStarted -= HandleGrabDrop;
//             _inputProvider.InventorySwitchStarted -= HandleStashInventorySwitch;
//         }
//
//         private void HandleGrabDrop()
//         {
//             if (_inventoryController.CurrentGrabbedItem)
//             {
//                 var placementData = _inventoryController.TrySetGrabbedItemOnCurrentSelectedIndex();
//                 if (placementData.CanFit)
//                 {
//                     _inventoryController.DropItem();
//                 }
//                 _inventoryController.ClearPlacementColorForItem(placementData.Item);
//                 
//                 return;
//             }
//
//             _inventoryController.TryGrabItem();
//         }
//
//         private void OnMoveInputStarted(Vector2 direction)
//         {
//             GridDirection gridDirection = InventoryUtilities.FromDirectionToGridDirection(direction);
//
//             if (_inventoryController.CurrentGrabbedItem)
//             {            
//                 _inventoryController.ClearPlacementForItem(_inventoryController.CurrentGrabbedItem);
//             }
//             
//             _inventoryController.MoveSelectionIndex(gridDirection);
//             
//             if (_inventoryController.CurrentGrabbedItem)
//             {            
//                 _inventoryController.PreviewGrabbedItemPlacement();
//             }
//         }
//         
//         private void RotateCurrentGrabbedItem()
//         {
//             if (!_inventoryController.CurrentGrabbedItem)
//             {
//                 return;
//             }
//
//             _inventoryController.ClearPlacementForItem(_inventoryController.CurrentGrabbedItem);
//             _inventoryController.RotateCurrentGrabbedItem();
//             _inventoryController.PreviewGrabbedItemPlacement();
//         }
//         
//         private void OnInventoryActionStarted()
//         {
//             _inventoryController.Hide();
//             _controller.SwitchState<CharacterControlState>();
//         }
//         
//         private void HandleStashInventorySwitch()
//         {
//             _inventoryController.HandleStashInventorySwitch();
//         } 
//     }
// }