﻿using _Project.Scripts.Core;
using DG.Tweening;
using KinematicCharacterController;
using UnityEngine;

public class AtoBPlatform : MonoBehaviour, IActionable, IMoverController 
{
    [SerializeField] private Transform platform;
    [SerializeField] private Transform initialPositionPivot;
    [SerializeField] private Transform finalPositionPivot;
    [SerializeField] private float movementSpeed;
    [SerializeField] private float waitToMove;
    [SerializeField] private Ease easeType;
    [SerializeField] private bool isActionable;
    
    private bool _isMoving;
    private bool _isInitialPosition;

    [SerializeField] private Transform _myGoalTransform;
    public PhysicsMover Mover;
    public bool IsActionable => isActionable;

    
    private void Awake()
    {
        platform.position = initialPositionPivot.position;
        _isInitialPosition = true;
        _myGoalTransform.position = initialPositionPivot.position;
        Mover.MoverController = this;
    }
    
    public void UpdateMovement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
    {
        // Set our platform's goal pose to the animation's
        goalPosition = _myGoalTransform.position;
        goalRotation = _myGoalTransform.rotation;
    }
    
    public void PerformAction()
    {
        if (_isMoving)
        {
            return;
        }
        _isMoving = true;
        Move();
    }
    
    public void UndoAction() { }
    
    private void Move()
    {
        if (_isInitialPosition)
        {
            _myGoalTransform.DOMove(finalPositionPivot.position, movementSpeed).SetSpeedBased().SetEase(easeType)
                .OnComplete(() => OnMovementCompleted(false));
            
            
            return;
        }
        
        _myGoalTransform.DOMove(initialPositionPivot.position, movementSpeed).SetSpeedBased().SetEase(easeType)
            .OnComplete(() => OnMovementCompleted(true));
        
        
        void OnMovementCompleted(bool isInitialPosition)
        {
            _isMoving = false;
            _isInitialPosition = isInitialPosition;
        }
    }
}