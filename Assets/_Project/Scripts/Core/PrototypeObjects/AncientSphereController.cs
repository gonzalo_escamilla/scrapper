﻿using UnityEngine;

namespace _Project.Scripts.Core.PrototypeObjects
{
    public class AncientSphereController : MonoBehaviour, IActionable
    {
        [SerializeField] private GameObject doorToOpen;
        
        public bool IsActionable { get; }

        private const int MaxSpheres = 3;
        private int _currentSpheres = 0;
        
        public void PerformAction()
        {
            _currentSpheres++;
            if (_currentSpheres >= MaxSpheres)
            {
                OpenDoor();
            }
        }

        private void OpenDoor()
        {
            doorToOpen.SetActive(false);
        }

        public void UndoAction()
        {
        }
    }
}