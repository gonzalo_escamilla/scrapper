﻿using UnityEngine;

namespace _Project.Scripts.Core.PrototypeObjects
{
    public class InteractiveCube : MonoBehaviour, IActionable
    {
        [SerializeField] private MeshRenderer myRenderer;
        [SerializeField] private Material turnedOnMaterial;
        [SerializeField] private Material turnedOffMaterial;
        
        public bool IsActionable { get; }
        public void PerformAction()
        {
            
            myRenderer.material = turnedOffMaterial;
            
        }

        public void UndoAction() { }
    }
}