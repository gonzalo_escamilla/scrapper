﻿using System;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.NarrativeSystem;
using Enderlook.EventManager;
using Ink.Runtime;
using UnityEngine;

namespace _Project.Scripts.Core.PrototypeObjects
{
    public enum AncientSphereType
    {
        Red,
        Green,
        Blue
    }
    
    public class AncientSphere : MonoBehaviour, IActionable
    {
        [SerializeField] private AncientSphereType type;
        [SerializeField] private AncientSphereController controller;
        [SerializeField] private MeshRenderer myRenderer;
        [SerializeField] private MeshRenderer altarRenderer;
        [SerializeField] private Material turnedOnMaterial;
        [SerializeField] private Material turnedOffMaterial;
        
        public bool IsActionable { get; }
        public void PerformAction()
        {
            controller.PerformAction();
            myRenderer.material = turnedOffMaterial;
            altarRenderer.material = turnedOnMaterial;

            UpdateInkVariables();
        }

        private void UpdateInkVariables()
        {
            string variableName = "";
            
            switch (type)
            {
                case AncientSphereType.Red:
                    variableName = InkConstants.HasGrabbedRedSphere;
                    break;
                case AncientSphereType.Green:
                    variableName = InkConstants.HasGrabbedGreenSphere;
                    break;
                case AncientSphereType.Blue:
                    variableName = InkConstants.HasGrabbedBlueSphere;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            EventManager.Shared.Raise(new GameEvents.Dialogue.InkVariableUpdated(variableName, true));   
        }

        public void UndoAction() { }
    }
}