﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Pool;

namespace _Project.Scripts.Core
{
    public class DebugConsole : MonoBehaviour
    {
        [SerializeField] private RectTransform container;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private ObjectPool<DebugConsoleText> textPool;
        [SerializeField] private DebugConsoleText textPrefab;

        private void Awake()
        {
            SetupPool();
        }

        [Button("Test Log")]
        public void TestLog()
        {
            Log("Some Text Kinda Long");    
            Log("A text which is really really long and might come with some dificuolties to render properly, what to do then ha?");    
        }
        
        public void Log(string text)
        {
            var debugText = textPool.Get();
            debugText.SetData(text);
            debugText.transform.SetParent(container);
        }
        
        private void SetupPool()
        {
            textPool = new ObjectPool<DebugConsoleText>(OnCreate, OnGet, OnRelease, null, true, 20, 100);
        }

        private DebugConsoleText OnCreate()
        {
            return Instantiate(textPrefab);
        }
        
        private void OnGet(DebugConsoleText debugText)
        {
        }
        
        private void OnRelease(DebugConsoleText debugText)
        {
            debugText.Dispose();
        }
    }
}