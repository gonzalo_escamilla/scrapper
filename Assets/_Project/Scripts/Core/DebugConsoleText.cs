﻿using TMPro;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public class DebugConsoleText : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private CanvasGroup canvasGroup;

        public void SetData(string text)
        {
            canvasGroup.alpha = 1;
            this.text.text = text;
        }

        public void Dispose()
        {
            canvasGroup.alpha = 0;
            this.text.text = "";
        }
    }
}