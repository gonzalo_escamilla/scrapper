﻿using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies.Worm
{
    [CreateAssetMenu(menuName = "Scrapper/Enemies/Worm/Worm Settings", fileName = "WormSettings", order = 0)]
    public class WormSettings : ScriptableObject
    {
        public int damage;
        public LayerMask attackLayer;
        public float bulletSpeed;
        public float fireRate;
        public float rotationSpeed;
        public Bullet bulletPrefab;
    }
}