﻿using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies.Worm
{
    public class Attacking : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; }
        public bool CanTransitionToSelf { get; set; }
        
        private Worm _worm;
        private WormSettings _wormSettings;
        private ScrapperController _scrapper;

        private float _nextFireTime;
        
        public Attacking(Worm worm, ScrapperController scrapper, WormSettings settings)
        {
            _worm = worm;;
            _scrapper = scrapper;
            _wormSettings = settings;
        }

        public void OnEnter()
        {
        }

        public void OnExit()
        {
        }

        public void Tick()
        {
            _worm.LookAt(_scrapper);
            
            if (Time.time >= _nextFireTime)
            {
                _nextFireTime = Time.time + 1f / _wormSettings.fireRate;
                _worm.ShotBullet();
            }
        }
    }
}