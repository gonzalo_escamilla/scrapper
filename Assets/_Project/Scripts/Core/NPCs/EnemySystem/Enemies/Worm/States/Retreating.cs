﻿using System.Collections;
using _Project.Scripts.GameServices;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies.Worm
{
    public class Retreating : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; private set; }
        public bool CanTransitionToSelf { get; set; }

        private Worm _worm;
        private CoroutineService _coroutineService = Services.Get<CoroutineService>();

        public Retreating(Worm worm)
        {
            _worm = worm;
        }
        
        public void OnEnter()
        {
            IsDone = false;
            _coroutineService.StartCoroutine(this, RunAnimation());
        }

        private IEnumerator RunAnimation()
        {
            _worm.Hide(false);
            yield return new WaitForSeconds(1.5f);
            IsDone = true;
        }
        
        public void OnExit()
        {
        }

        public void Tick()
        {
        }
    }
}