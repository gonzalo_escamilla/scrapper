﻿using System.Collections;
using _Project.Scripts.GameServices;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies.Worm
{
    public class Emerging : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; private set; }
        public bool CanTransitionToSelf { get; set; }

        private CoroutineService _coroutineService = Services.Get<CoroutineService>();
        private Worm _worm;
        private ScrapperController _scrapperController;

        public Emerging(Worm worm, ScrapperController scrapperController)
        {
            _worm = worm;
            _scrapperController = scrapperController;
        }
        
        public void OnEnter()
        {
            IsDone = false;
            _coroutineService.StartCoroutine(this, RunAnimation());
        }

        private IEnumerator RunAnimation()
        {
            _worm.RotateImmediately(_scrapperController.transform);
            _worm.Emerge(false);
            yield return new WaitForSeconds(1.5f);
            IsDone = true;
        }

        public void OnExit()
        {
        }

        public void Tick()
        {
        }
    }
}