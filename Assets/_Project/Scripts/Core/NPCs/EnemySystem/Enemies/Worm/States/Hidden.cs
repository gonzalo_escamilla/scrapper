﻿using Pathfinding;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies.Worm
{
    public class Hidden : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; }
        public bool CanTransitionToSelf { get; set; }

        public void Tick()
        {
        }

        public void OnEnter()
        {
        }

        public void OnExit()
        {
        }
    }
}