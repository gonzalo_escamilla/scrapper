﻿using System;
using _Project.Scripts.Core.DamageSystem;
using UnityEngine;
using DG.Tweening;
using Enderlook.EventManager;
using _Project.Scripts.Core.EventsSystem;

namespace _Project.Scripts.Core.NPCs.Enemies.Worm
{
    public class Worm : BaseEnemy
    {
        [SerializeField] private WormSettings wormSettings;
        [SerializeField] private EnemyDetectionController enemyDetectionController;
        [SerializeField] private Transform initialPosition;
        [SerializeField] private Transform graphicsRoot;
        [SerializeField] private Transform undergroundPivot;
        [SerializeField] private Transform surfacePivot;
        [SerializeField] private Transform firePoint;
        [SerializeField] private ExampleDestructible destructible;

        private void Awake()
        {
            destructible.DamageTaken += OnDamageTaken;
            destructible.Destroyed += OnDestroyed;
        }
        
        private void OnDestroy()
        {
            _stateMachine.Dispose();
            
            destructible.DamageTaken += OnDamageTaken;
            destructible.Destroyed += OnDestroyed;
        }
        
        private void Start()
        {
            base.Initialize();
            
            var hiddenState = new Hidden();
            var emergingState = new Emerging(this, _scrapperController);
            var attackingState = new Attacking(this, _scrapperController, wormSettings);
            var searchingState = new Searching(this, _scrapperController);
            var retreatingState = new Retreating(this);

            AddTransition(hiddenState, emergingState, CharacterInsideRadius());
            AddTransition(emergingState, attackingState, () => emergingState.IsDone);
            
            AddTransition(attackingState, searchingState, FromSearchingToRetreating());
            AddTransition(searchingState, attackingState, CharacterInsideRadius());
            
            AddTransition(searchingState, retreatingState, () => searchingState.IsDone);
            AddTransition(retreatingState, hiddenState, () => retreatingState.IsDone);

            _stateMachine.SetState(hiddenState);
            Hide(false);
            
            
            Func<bool> CharacterInsideRadius() => () => enemyDetectionController.IsCharacterInsideAwarenessRadius;
            Func<bool> CharacterInSight() => () => enemyDetectionController.IsCharacterInSight;
            Func<bool> LostSightOfCharacter() => () => !enemyDetectionController.IsCharacterInSight;
            Func<bool> CharacterAtReach() => () => enemyDetectionController.IsCharacterInReach;
            Func<bool> CharacterNotAtReach() => () => !enemyDetectionController.IsCharacterInReach;
            Func<bool> FromSearchingToRetreating() => () => !enemyDetectionController.IsCharacterInsideAwarenessRadius || !enemyDetectionController.IsCharacterInSight;
        }
        
        private void AddTransition(IState from, IState to, Func<bool> condition) =>
            _stateMachine.AddTransition(from,to,condition);
        
        private void AddAnyTransition(IState to, Func<bool> condition) =>
            _stateMachine.AddAnyTransition(to, condition);
        

        private void Update()
        {
            _stateMachine.Tick();
        }

        public void LookAt(ScrapperController scrapper)
        {
            if (scrapper == null) return;

            Vector3 directionToTarget = scrapper.transform.position - transform.position;
            directionToTarget.y = 0f; // Aseguramos que la rotación solo ocurra en el eje Y

            if (directionToTarget != Vector3.zero)
            {
                Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, wormSettings.rotationSpeed * Time.deltaTime);
            }
        }

        public void RotateImmediately(Transform target)
        {
            if (target == null)
            {
                Debug.LogError("ScrapperController is null");
                return;
            }
            
            Vector3 directionToTarget = target.position - transform.position;
            directionToTarget.y = 0f;
            if (directionToTarget != Vector3.zero)
            {
                Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
                transform.rotation = targetRotation;
            }
        }
        
        public void Hide(bool skipAnimation)
        {
            graphicsRoot.DOMoveY(undergroundPivot.position.y,0.75f);
        }

        public void Emerge(bool skipAnimation)
        {
            graphicsRoot.DOMoveY(surfacePivot.position.y,0.75f);
        }

        public void ShotBullet()
        {
            Bullet bullet = Instantiate(wormSettings.bulletPrefab,
                firePoint.position,
                firePoint.rotation);
                
            bullet.Initialize(wormSettings.bulletSpeed, wormSettings.damage, wormSettings.attackLayer, 0);
            EventManager.Shared.Raise<GameEvents.CombatEvents.BulletFired>();
        }
        
        private void OnDestroyed()
        {
            Destroy(this.gameObject);
        }

        private void OnDamageTaken()
        {
        }
    }
}