﻿using _Project.Scripts.GameServices;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies.Worm
{
    public class Searching : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; private set; }
        public bool CanTransitionToSelf { get; set; }
        
        private Worm _worm;
        private ScrapperController _scrapperController;
        private CoroutineService _coroutineService = Services.Get<CoroutineService>();
        private float searchTime = 5;
        private float elapsedTime = 0;
        
        public Searching(Worm worm, ScrapperController scrapperController)
        {
            _worm = worm;
            _scrapperController = scrapperController;
        }
        
        public void OnEnter()
        {
            IsDone = false;
            elapsedTime = 0;
        }

        public void OnExit()
        {
        }

        public void Tick()
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= searchTime)
            {
                IsDone = true;
            }
        }
    }
}