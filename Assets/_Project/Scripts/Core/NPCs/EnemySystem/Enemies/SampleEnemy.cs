﻿using System;
using _Project.Scripts.Core.DamageSystem;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class SampleEnemy : BaseEnemy
    {
        [SerializeField] private EnemyDetectionController enemyDetectionController;

        private ScrapperController _scrapperController;

        private IdleLook _idleLook;
        
        private void Awake()
        {
            _scrapperController = FindObjectOfType<ScrapperController>(); //TODO: Inject with a service at some point
            _stateMachine = new StateMachine();
                        
            _idleLook = new IdleLook(this, EnemySettings, _aiPath);
            var patrol = new Patrol(this, _seeker, _aiPath, EnemySettings);
            var investigate = new Investigate(enemyDetectionController, _aiPath, _scrapperController.transform ,EnemySettings);
            var chase = new Chase(this, _aiPath, _scrapperController, EnemySettings);
            var meleeAttack = new SampleMeleeAttack(this, EnemySettings, _aiPath);
            
            AddTransition(_idleLook, patrol, IsDoneWaiting());
            AddTransition(patrol, _idleLook, CompletelyStoppedAndReachedDestination());
            AddTransition(_idleLook, investigate, CharacterInsideRadius());
            AddTransition(investigate, _idleLook, FromInvestigateToIdle());
            AddTransition(investigate, investigate, FromInvestigateToSelf());
            
            AddTransition(patrol, investigate, CharacterInsideRadius());
            
            AddAnyTransition(chase, CharacterInSight());
            AddTransition(chase, investigate, LostSightOfCharacter());

            AddAnyTransition(meleeAttack, CharacterAtReach());
            AddTransition(meleeAttack, chase, CharacterNotAtReach());
            AddTransition(meleeAttack, investigate, LostSightOfCharacter());

            
            _stateMachine.SetState(_idleLook);
            
            Func<bool> FromInvestigateToIdle() => () => _aiPath.reachedDestination && !enemyDetectionController.IsCharacterInsideAwarenessRadius;
            Func<bool> FromInvestigateToSelf() => () => _aiPath.reachedDestination && _aiPath.velocity.magnitude <= 0 && enemyDetectionController.IsCharacterInsideAwarenessRadius;
            Func<bool> IsDoneWaiting() => () => _idleLook.IsDone;
            Func<bool> ReachedDestination() => () => _aiPath.reachedDestination;
            Func<bool> CharacterInsideRadius() => () => enemyDetectionController.IsCharacterInsideAwarenessRadius;
            Func<bool> CharacterInSight() => () => enemyDetectionController.IsCharacterInSight;
            Func<bool> LostSightOfCharacter() => () => !enemyDetectionController.IsCharacterInSight;
            Func<bool> CharacterAtReach() => () => enemyDetectionController.IsCharacterInReach;
            Func<bool> CharacterNotAtReach() => () => !enemyDetectionController.IsCharacterInReach;
            Func<bool> CompletelyStopped() => () => _aiPath.velocity.magnitude <= 0;
            Func<bool> CompletelyStoppedAndReachedDestination() => () => _aiPath.reachedDestination && _aiPath.velocity.magnitude <= 0;
        }
        
        private void Update()
        {
            _stateMachine.Tick();
        }

        private void OnDestroy()
        {
            _stateMachine.Dispose();
        }

        private void AddTransition(IState to, IState from, Func<bool> condition) =>
            _stateMachine.AddTransition(to,from,condition);
        
        private void AddAnyTransition(IState to, Func<bool> condition) =>
            _stateMachine.AddAnyTransition(to, condition);
        
        public override void TryMeleeAttack()
        {
            if (!enemyDetectionController.IsCharacterInReach)
            {
                Debug.Log("Character not in reach.");
                return;
            }
            
            // TODO: Right now it will deal damage olways that is in the reach readius. Then we should make a real attack cast

            AttackData data = new AttackData(this.gameObject, this.transform.forward, EnemySettings.Attack.DamageAmount);
            _scrapperController.GetHit(data);
        }
    }
}