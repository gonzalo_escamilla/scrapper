﻿using System;
using _Project.Scripts.Core.DamageSystem;
using Pathfinding;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class BaseEnemy : MonoBehaviour
    {
        [SerializeField] protected EnemySettings EnemySettings;
        [SerializeField] protected Seeker _seeker;
        [SerializeField] protected AIPath _aiPath;
        [ShowInInspector] private string lastAIState => _stateMachine is { LastState: not null }? _stateMachine.LastState.GetType().Name :"Unitialized";
        [ShowInInspector] private string currentAIState => _stateMachine is { CurrentState: not null }? _stateMachine.CurrentState.GetType().Name :"Unitialized";

        protected StateMachine _stateMachine;
        protected ScrapperController _scrapperController;

        public bool IsDead { get; set; }
        
        protected float _health;
        public virtual float Health
        {
            get => _health;
            protected set
            {
                _health = value;

                if (_health <= 0)
                {
                    _health = 0;
                    Die();
                }
            }
        }

        public event Action<BaseEnemy> OnDie;
        
        public virtual void Initialize()
        {
            Health = EnemySettings.maxHealth;
            IsDead = false;
            
            _scrapperController = FindObjectOfType<ScrapperController>(); //TODO: Inject with a service at some point
            _stateMachine = new StateMachine();
        }
        
        public void SetIAMovementSettings(EnemySettings.MovementSettings settingsBaseMovementSettings)
        {
            _aiPath.maxSpeed = settingsBaseMovementSettings.speed;
            _aiPath.rotationSpeed = settingsBaseMovementSettings.rotationSpeed;
        }
        
        private Action<BaseEnemy> _releaseAction;
        
        public virtual void SetPoolReleaseAction(Action<BaseEnemy> releaseAction)
        {
            _releaseAction = releaseAction;
        }
        
        public virtual void GetHit(AttackData dmgData)
        {
            Health -= dmgData.Damage;
        }

        public virtual void Kill()
        {
            if (IsDead)
                return;
            
            IsDead = true;
            ReturnToPool();
            OnDie = null;
        }
        
        public virtual void TryMeleeAttack() { }
        
        protected virtual void Die()
        {
            IsDead = true;
            ReturnToPool();
            OnDie?.Invoke(this);
            OnDie = null;
        }

        protected virtual void ReturnToPool()
        {
            _releaseAction?.Invoke(this);
        }
    }
}