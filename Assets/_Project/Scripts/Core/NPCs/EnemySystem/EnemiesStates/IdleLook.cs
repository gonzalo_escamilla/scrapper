﻿using System;
using System.Collections;
using _Project.Scripts.GameServices;
using DG.Tweening;
using Pathfinding;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class IdleLook : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone => _isDoneRotation && _isWaitTimeExpired;
        public bool CanTransitionToSelf { get; set; }

        private BaseEnemy _enemy;
        private EnemySettings _settings;
        private AIPath _iaPath;
        private CoroutineService _coroutineService;
        private float _waitTime;
        private float _elapsedTime;
        private bool _isDoneRotation;
        private bool _isWaitTimeExpired;

        private Guid _rotateCoroutineGuid;
        private Guid _waitCoroutineGuid;

        public IdleLook(BaseEnemy enemy, EnemySettings settings, AIPath iaPath)
        {
            _enemy = enemy;
            _iaPath = iaPath;
            _settings = settings;
            _coroutineService = Services.Get<CoroutineService>();
        }

        public void Tick()
        {
            if (_elapsedTime >= _waitTime)
            {
                _isWaitTimeExpired = true;
            }

            _elapsedTime += Time.deltaTime;
        }

        public void OnEnter()
        {
            _waitTime = _settings.Idle.GetIdleWaitTime();
            _isWaitTimeExpired = false;
            _isDoneRotation = false;
            _elapsedTime = 0;
            _enemy.SetIAMovementSettings(_settings.BaseMovementSettings);

            SetBaseMovementValues();
            
            StartRotation();
        }

        private void SetBaseMovementValues()
        {
            _enemy.SetIAMovementSettings(_settings.BaseMovementSettings);
        }

        private void StartRotation(float delay = 0)
        {
            if (_isWaitTimeExpired || _isDoneRotation)
                return;
            
            if (_coroutineService.IsCoroutineActive(this, nameof(RotateOverTime)))
            {
                _coroutineService.StopCoroutine(this, _rotateCoroutineGuid);   
            }
            
            _rotateCoroutineGuid = _coroutineService.StartCoroutine(this, RotateOverTime(delay));
        }
        
        public IEnumerator RotateOverTime(float delay = 0)
        {
            _isDoneRotation = false;

            yield return new WaitUntil(CompletelyStopped);
            yield return new WaitForSeconds(1f);

            if (delay > 0)
            {
                yield return new WaitForSeconds(delay);
            }
            
            _iaPath.updateRotation = false;
            
            float currentYAngle = _enemy.transform.eulerAngles.y;

            // Calculate the target angle by adding the fixed rotation amount
            float targetYAngle = currentYAngle + _settings.Idle.baseRotationAngle;
            
            _enemy.transform.DORotate(Vector3.up * targetYAngle, _settings.Idle.rotationSpeed).OnComplete(OnRotationCompleted).SetSpeedBased();
            void OnRotationCompleted()
            {
                _iaPath.updateRotation = true;
                Quaternion newRotation = _iaPath.SimulateRotationTowards(_enemy.transform.forward,1000);
                _iaPath.rotation = newRotation;
                _iaPath.FinalizeMovement(_iaPath.position, newRotation);

                if (!_isWaitTimeExpired)
                {
                    StartRotation(_settings.Idle.rotationFrequency);
                    return;
                }

                EndIdle();
            }
        }

        private void EndIdle()
        {
            if (_coroutineService.IsCoroutineActive(this, nameof(WaitToTransition)))
            {
                _coroutineService.StopCoroutine(this, _waitCoroutineGuid);   
            }
            
            _waitCoroutineGuid = _coroutineService.StartCoroutine(this, WaitToTransition());
        }
        
        private IEnumerator WaitToTransition()
        {
            yield return new WaitForSeconds(_settings.Idle.rotationFrequency);
            _isDoneRotation = true;
        }

        private bool CompletelyStopped()
        {
            return _iaPath.velocity.magnitude <= 0;
        }

        public void OnExit()
        {
            if (_coroutineService.IsCoroutineActive(this, nameof(RotateOverTime)))
            {
                _coroutineService.StopCoroutine(this, _rotateCoroutineGuid);   
            }
            
            if (_coroutineService.IsCoroutineActive(this, nameof(WaitToTransition)))
            {
                _coroutineService.StopCoroutine(this, _waitCoroutineGuid);   
            }
            
            _iaPath.FinalizeMovement(_iaPath.position, _iaPath.rotation);
            _isWaitTimeExpired = true;
            _isDoneRotation = true;
        }
    }
}