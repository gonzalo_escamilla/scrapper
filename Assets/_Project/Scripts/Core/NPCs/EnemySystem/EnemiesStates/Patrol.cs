﻿using Pathfinding;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class Patrol : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; }
        public bool CanTransitionToSelf { get; set; }

        private SampleEnemy _enemy;
        private Seeker _seeker;
        private AIPath _aiPath;
        private EnemySettings _settings;

        public Patrol(SampleEnemy enemy, Seeker seeker, AIPath aiPath, EnemySettings settings)
        {
            _enemy = enemy;
            _seeker = seeker;
            _aiPath = aiPath;
            _settings = settings;
        }

        public void OnEnter()
        {
            _aiPath.SetPath(null);
            _aiPath.canSearch = false;
            _aiPath.canSearch = true;
            _aiPath.SetPath(GetRandomPath());
        }

        public void OnExit() { }

        public void Tick() { }
        
        private Path GetRandomPath()
        {
            Vector3 startPosition = _seeker.transform.position;
            int searchLength = _settings.setRandomDestinationRelativeToSelfSettings.searchDistance;
            RandomPath randomPath = RandomPath.Construct(startPosition, searchLength);
            randomPath.spread = _settings.setRandomDestinationRelativeToSelfSettings.pathSpread;
            return randomPath;
        }
    }
}