﻿using System.Collections;
using _Project.Scripts.GameServices;
using Pathfinding;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class SampleMeleeAttack : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; set; }
        public bool CanTransitionToSelf { get; set; }

        private BaseEnemy _enemy;
        private EnemySettings _settings;
        private CoroutineService _coroutineService;
        private AIPath _aiPath;

        public SampleMeleeAttack(BaseEnemy enemy, EnemySettings settings, AIPath aiPath)
        {
            _enemy = enemy;
            _settings = settings;
            _coroutineService = Services.Get<CoroutineService>();
            _aiPath = aiPath;
        }

        public void OnEnter()
        {
            HasExitTime = true;
            IsDone = false;
            
            _aiPath.FinalizeMovement(_aiPath.position, _aiPath.rotation);
            _aiPath.SetPath(null, true);
            _aiPath.isStopped = true;
            
            _coroutineService.StopAllCoroutinesFromOneOwner(this);
            _coroutineService.StartCoroutine(this, Attack());
        }

        private IEnumerator Attack()
        {
            _enemy.TryMeleeAttack();
            Debug.LogWarning("Attack");
            yield return new WaitForSeconds(_settings.Attack.waitToAttack);
            IsDone = true;
        }

        public void Tick()
        {
        }

        public void OnExit()
        {
            _aiPath.isStopped = false;
        }
    }
}