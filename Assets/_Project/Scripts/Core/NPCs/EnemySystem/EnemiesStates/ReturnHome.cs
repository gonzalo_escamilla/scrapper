﻿using System.Collections;
using _Project.Scripts.GameServices;
using Pathfinding;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class ReturnHome : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; }
        public bool CanTransitionToSelf { get; set; }
        
        public Vector3 InitialPosition { get; private set; }

        private AIPath _aiPath;
        private EnemySettings _settings;
        private CoroutineService _coroutineService;
        
        public ReturnHome(Vector3 initialPosition, AIPath aiPath, EnemySettings settings)
        {
            InitialPosition = initialPosition;
            _aiPath = aiPath;
            _settings = settings;
            _coroutineService = Services.Get<CoroutineService>();
        }
        
        public void OnEnter()
        {
            _coroutineService.StopAllCoroutinesFromOneOwner(this);

            _aiPath.isStopped = true;
            _aiPath.SetPath(null);
            _aiPath.destination = InitialPosition;
            _coroutineService.StartCoroutine(this, WaitAndLookAround());
        }

        public void OnExit()
        {
            _aiPath.SetPath(null);
            _aiPath.isStopped = false;
        }

        public void Tick() { }

        private IEnumerator WaitAndLookAround()
        {
            yield return new WaitForSeconds(_settings.Investigate.waitTimeToInvestigate);
            _aiPath.isStopped = false;
        }
    }
}