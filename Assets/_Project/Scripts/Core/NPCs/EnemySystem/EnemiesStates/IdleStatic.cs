﻿namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class IdleStatic : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; }
        public bool CanTransitionToSelf { get; set; }

        private BaseEnemy _enemy;
        private EnemySettings _settings;

        public IdleStatic(BaseEnemy enemy, EnemySettings settings)
        {
            _enemy = enemy;
            _settings = settings;
        }

        public void Tick() { }

        public void OnEnter()
        {
            _enemy.SetIAMovementSettings(_settings.BaseMovementSettings);
            SetBaseMovementValues();
        }

        private void SetBaseMovementValues()
        {
            _enemy.SetIAMovementSettings(_settings.BaseMovementSettings);
        }

        public void OnExit() { }
    }
}