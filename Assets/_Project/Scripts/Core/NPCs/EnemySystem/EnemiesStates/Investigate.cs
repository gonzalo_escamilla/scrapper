﻿using System.Collections;
using _Project.Scripts.GameServices;
using Pathfinding;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class Investigate : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; }
        public bool CanTransitionToSelf { get; set; }

        private EnemyDetectionController _detectionController;
        private AIPath _aiPath;
        private EnemySettings _settings;
        private CoroutineService _coroutineService;
        private Transform _target;

        public Investigate(EnemyDetectionController detectionController, AIPath aiPath, Transform target, EnemySettings settings)
        {
            _detectionController = detectionController;
            _aiPath = aiPath;
            _settings = settings;
            _target = target;
            _coroutineService = Services.Get<CoroutineService>();
            CanTransitionToSelf = true;
        }

        public void OnEnter()
        {
            _coroutineService.StopAllCoroutinesFromOneOwner(this);

            _aiPath.isStopped = true;
            _aiPath.SetPath(null);
            _aiPath.destination = _target.position;
            _coroutineService.StartCoroutine(this, WaitAndLookAround());
        }

        public void OnExit()
        {
            _aiPath.SetPath(null);
            _aiPath.isStopped = false;
        }

        public void Tick()
        {
        }

        private IEnumerator WaitAndLookAround()
        {
            yield return new WaitForSeconds(_settings.Investigate.waitTimeToInvestigate);
            // _aiPath.destination = _detectionController.LastCharacterDetectedPosition;
            _aiPath.isStopped = false;
        }
    }
}