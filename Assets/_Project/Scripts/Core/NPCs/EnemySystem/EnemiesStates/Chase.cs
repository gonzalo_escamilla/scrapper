﻿using System.Collections;
using _Project.Scripts.GameServices;
using Pathfinding;
using UnityEngine;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    public class Chase : IState
    {
        public bool HasExitTime { get; set; }
        public bool IsDone { get; }
        public bool CanTransitionToSelf { get; set; }
        
        private BaseEnemy _enemy;
        private AIPath _aiPath;
        private ScrapperController _scrapperController;
        private CoroutineService _coroutineService;
        private EnemySettings _settings;
        private bool _shouldFollow;

        public Chase(BaseEnemy enemy, AIPath aiPath, ScrapperController scrapperController, EnemySettings settings)
        {
            _enemy = enemy;
            _aiPath = aiPath;
            _scrapperController = scrapperController;
            _coroutineService = Services.Get<CoroutineService>();
            _settings = settings;
        }

        public void OnEnter()
        {
            _enemy.SetIAMovementSettings(_settings.EngageMovementSettings);
            
            _shouldFollow = true;
            _aiPath.destination = _scrapperController.transform.position;
            
            _coroutineService.StopAllCoroutinesFromOneOwner(this);
            _coroutineService.StartCoroutine(this, Follow());
        }
        
        public void OnExit()
        {
            _shouldFollow = false;
            _enemy.SetIAMovementSettings(_settings.BaseMovementSettings);
        }

        public void Tick() { }
        
        private IEnumerator Follow()
        {
            yield return null;
            while (_shouldFollow)
            {
                yield return new WaitForSeconds(_settings.followCharacterSettings.repathRate);
                _aiPath.destination = _scrapperController.transform.position;
            }
        }
    }
}