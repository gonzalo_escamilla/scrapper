using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace _Project.Scripts.Core.NPCs.Enemies
{
    [CreateAssetMenu(menuName = "Scrapper/Enemies/Create Enemy Settings", fileName = "EnemySettings", order = 0)]
    public class EnemySettings: ScriptableObject
    {
        [Title("Heal Settings")]
        public float maxHealth;

        [Title("Movement Settings")] 
        public MovementSettings BaseMovementSettings;
        public MovementSettings EngageMovementSettings;
        
        [Title("Enemy IA States Settings")] 
        public IdleSettings Idle;
        public SetRandomDestinationRelativeToSelfSettings setRandomDestinationRelativeToSelfSettings;
        public FollowCharacterSettings followCharacterSettings;
        public AttackSettings Attack;
        public InvestigateSettings Investigate;
        
        [System.Serializable]
        public class IdleSettings
        {
            public bool isWaitTimeFixed;
            [MinMaxSlider(0,20)] 
            [HideIf(nameof(isWaitTimeFixed))]
            public Vector2 waitTimeRange;
            [ShowIf(nameof(isWaitTimeFixed))]
            public float fixedWaitTime;
            public float baseRotationAngle;
            public float rotationFrequency;
            public float rotationSpeed;
            
            public float GetIdleWaitTime()
            {
                return isWaitTimeFixed ? fixedWaitTime : Random.Range(waitTimeRange.x, waitTimeRange.y);
            }
        }

        [System.Serializable]
        public class MovementSettings
        {
            [Tooltip("Max speed in world units per second")]
            public float speed;
            [Tooltip("Rotation speed in degrees per second. Rotation is calculated using Quaternion.RotateTowards. This variable represents the rotation speed in degrees per second. The higher it is, the faster the character will be able to rotate.")]
            public float rotationSpeed;
        }
        
        [System.Serializable]
        public class InvestigateSettings
        {
            [FormerlySerializedAs("waitTimeWhenStopped")] [Tooltip("Time that the enemy waits immediately after the character was detected to start investigating")]
            public float waitTimeToInvestigate;
        }
        
        [System.Serializable]
        public class SetRandomDestinationRelativeToSelfSettings
        {
            [Range(0,40000)] public int searchDistance;
            [Range(0,100000)] public int pathSpread;
        }
        
        [System.Serializable]
        public class FollowCharacterSettings
        {
            [Range(0.05f,1)] public float repathRate;
        }

        [System.Serializable]
        public class AttackSettings
        {
            public float DamageAmount;
            [Tooltip("Wait time before attacking (in seconds) when the player is within reach.")]
            [Range(0f,3f)] public float waitToAttack;
        }
    }
}