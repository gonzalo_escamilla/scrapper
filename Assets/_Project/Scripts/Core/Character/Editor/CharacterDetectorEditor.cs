﻿using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Core.Editor
{
    [CustomEditor(typeof(EnemyDetectionController))]
    public class CharacterDetectorEditor : UnityEditor.Editor
    {
        private EnemyDetectionController _enemyDetectionController;
        
        private const float SightEndAngleOffset = 0.25f;
        
        private void OnSceneGUI()
        {
            _enemyDetectionController = (EnemyDetectionController)target;
            if (!_enemyDetectionController == null || !_enemyDetectionController.DetectionSettings)
            {
                Debug.LogError("Dependencies are null.");
                return;
            }
            
            float sightDistance = _enemyDetectionController.DetectionSettings.sightDistance; 
            float radius = _enemyDetectionController.DetectionSettings.detectionRadiusDetectorSettings.radius; 
            float secondaryRadius = _enemyDetectionController.DetectionSettings.secondarySenseDetectorSettings.radius; 
            Transform transform = _enemyDetectionController.transform;
            float eulerAnglesY = transform.eulerAngles.y;
            float angle = _enemyDetectionController.DetectionAreaAngle;
            Vector3 position = _enemyDetectionController.transform.position;
            
            Vector3 scrapperPosition = position; 
            if (_enemyDetectionController.ScrapperController)
            {
                scrapperPosition = _enemyDetectionController.ScrapperController.transform.position; 
            }
            
            Handles.color = Color.white;
            Handles.DrawWireArc(position, Vector3.up, Vector3.forward, 360, radius);

            Vector3 viewAngle01 = DirectionFromAngle(eulerAnglesY, -angle / 2 - SightEndAngleOffset);
            Vector3 viewAngle02 = DirectionFromAngle(eulerAnglesY, angle / 2 + SightEndAngleOffset);
            
            Handles.color = Color.yellow;
            Handles.DrawLine(position, position + viewAngle01 * radius);
            Handles.DrawLine(position, position + viewAngle02 * radius);


            if (_enemyDetectionController.IsCharacterInsideAwarenessRadius)
            {
                Handles.color = new Color(255, 0, 0, 0.3f);
                Handles.DrawSolidDisc(position, Vector3.up, radius);
            }
            else
            {
                Handles.color = new Color(192, 192, 192, 0.3f);
                Handles.DrawSolidDisc(position, Vector3.up, radius);
            }
            
            if (_enemyDetectionController.IsCharacterInsideSecondarySenseRadius)
            {
                Handles.color = new Color(255, 165, 0, 0.3f);
                Handles.DrawSolidDisc(position + Vector3.up * 0.1f, Vector3.up, secondaryRadius);
            }
            else
            {
                Handles.color = new Color(0, 255, 0, 0.2f);
                Handles.DrawSolidDisc(position + Vector3.up * 0.1f, Vector3.up, secondaryRadius);
            }
            
            DrawReach(position);

            if (_enemyDetectionController.IsCharacterInSight)
            {
                Handles.color = Color.green;
                Handles.DrawLine(position + Vector3.up * 1.25f, scrapperPosition);

                Handles.color = new Color(255, 255, 0, 0.3f);
                Handles.DrawSolidArc(position, Vector3.up, transform.forward, angle / 2, sightDistance);
                Handles.DrawSolidArc(position, Vector3.up, transform.forward, -angle / 2, sightDistance);
            }
            else
            {
                Handles.color = new Color(0, 128, 255, 0.3f);
                Handles.DrawSolidArc(position, Vector3.up, transform.forward, angle / 2, sightDistance);
                Handles.DrawSolidArc(position, Vector3.up, transform.forward, -angle / 2, sightDistance);
            }
        }

        private void DrawReach(Vector3 position)
        {
            if (_enemyDetectionController.ScrapperController == null)
            {
                return;
            }
            
            float reachRadius = _enemyDetectionController.DetectionSettings.reach;
            if (_enemyDetectionController.IsCharacterInReach)
            {
                Handles.color = new Color(255, 0, 255, 0.4f);
                Handles.DrawSolidDisc(position, Vector3.up, reachRadius);
            }
            else
            {
                Handles.color = new Color(128, 0, 128, 0.3f);
                Handles.DrawSolidDisc(position, Vector3.up, reachRadius);
            }
        }

        private Vector3 DirectionFromAngle(float eulerY, float angleInDegress)
        {
            angleInDegress += eulerY;
            return new Vector3(Mathf.Sin(angleInDegress * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegress * Mathf.Deg2Rad));
        }
    }
}