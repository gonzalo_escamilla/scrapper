﻿using System;
using Sirenix.OdinInspector;

namespace _Project.Scripts.Core
{
    [Serializable]
    public class ScrapperStats
    {
        [ReadOnly] public float Endurance;
        [ReadOnly] public float Agility;
        public ScrapperStats(float endurance, float agility)
        {
            Endurance = endurance;
            Agility = agility;
        }
    }
}