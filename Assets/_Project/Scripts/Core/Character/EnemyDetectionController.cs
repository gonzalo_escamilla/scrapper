using System;
using _Project.Scripts.Core.UI;
using _Project.Scripts.GameServices;
using _Project.Scripts.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Logger = _Project.Scripts.GameServices.Logger;

namespace _Project.Scripts.Core
{
    public class EnemyDetectionController : MonoBehaviour
    {
        [Title("General")]
        [SerializeField] private EnemyDetectionControllerSettings detectionSettings;
        [SerializeField] private Detector<ScrapperController> secondarySenseDetector;
        [SerializeField] private Detector<ScrapperController> radiusDetector;
        [SerializeField] private Logger debug;

        [Title("UI")]
        [SerializeField] private PopUpImage imageStatusIcon;
        [SerializeField] private Sprite spriteExclamationIcon;
        [SerializeField] private Sprite spriteQuestionIcon;
        
        public EnemyDetectionControllerSettings DetectionSettings => detectionSettings;
        public ScrapperController ScrapperController => _scrapperController;
        public Vector3 LastCharacterDetectedPosition => _lastCharacterDetectedPosition;
        public float DetectionAreaAngle => detectionSettings.detectionAreaAngle;
        public bool IsCharacterInReach => Vector3.Distance(_scrapperController.transform.position, this.transform.position) <= Reach;
        public bool IsCharacterInSight => _isCharacterInSight;
        public bool IsCharacterInsideAwarenessRadius => _isCharacterInsideAwarenessRadius;
        public bool IsCharacterInsideSecondarySenseRadius => _isCharacterInsideSecondarySenseRadius;
        public bool IsCharacterInSightDistance => Vector3.Distance(_scrapperController.transform.position, this.transform.position) <= detectionSettings.sightDistance;

        public float Reach => detectionSettings.reach;

        private event Action PlayerSighted; 
        private event Action PlayerOutOfSight; 
        
        private ScrapperController _scrapperController;
        private Vector3 _lastCharacterDetectedPosition;
        private bool _isCharacterInsideAwarenessRadius;
        private bool _isCharacterInsideSecondarySenseRadius;
        private bool _isCharacterInSight;
        
        private void Awake()
        {
            _scrapperController = FindObjectOfType<ScrapperController>(); // TODO: THen we inject this dependency.
            
            radiusDetector = new Detector<ScrapperController>(transform, detectionSettings.detectionRadiusDetectorSettings);
            radiusDetector.OnTargetDetected += OnTargetDetected;
            radiusDetector.OnTargetLost += OnTargetLost;
            
            secondarySenseDetector = new Detector<ScrapperController>(transform, detectionSettings.secondarySenseDetectorSettings);
            secondarySenseDetector.OnTargetDetected += OnTargetDetectedBySecondarySense;
            secondarySenseDetector.OnTargetLost += OnTargetLostBySecondarySense;

            PlayerSighted += OnPlayerSighted;
            PlayerOutOfSight += OnPlayerOutOfSight;
        }
        
        private void OnDestroy()
        {
            PlayerSighted -= OnPlayerSighted;
            PlayerOutOfSight -= OnPlayerOutOfSight;
        }
        
        private void Update()
        {
            radiusDetector.Update();
            secondarySenseDetector.Update();

            if (IsCharacterInSightDistance)
            {
                CheckFieldOfView();
            }
        }
        
        private void CheckFieldOfView()
        {
            Vector3 directionToTarget = (_scrapperController.transform.position - transform.position).normalized;

            if (Vector3.Angle(transform.forward, directionToTarget) < DetectionAreaAngle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, _scrapperController.transform.position);

                bool wasInSight = _isCharacterInSight;
                
                _isCharacterInSight = !Physics.Raycast(transform.position, directionToTarget, distanceToTarget,
                    detectionSettings.obstaclesLayerMaks);

                if (!wasInSight && _isCharacterInSight)
                {
                    PlayerSighted?.Invoke();
                }
            }
            else
            {
                bool wasInSight = _isCharacterInSight;
                _isCharacterInSight = false;
                if (wasInSight)
                {
                    PlayerOutOfSight?.Invoke();
                }
            }

            debug.Log($"Is Character in sight: {_isCharacterInSight}");
        }

        private void OnTargetDetected(ScrapperController character)
        {
            _lastCharacterDetectedPosition = character.transform.position;
            _isCharacterInsideAwarenessRadius = true;

            UpdateUI();
            debug.Log("Character Detected");
        }
        
        private void OnTargetLost(ScrapperController character)
        {
            _isCharacterInsideAwarenessRadius = false;
            _isCharacterInSight = false;

            PlayerOutOfSight?.Invoke(); // Change this when we use other area for the sight

            UpdateUI();
            debug.Log("Character Lost");
        }

        private void OnTargetDetectedBySecondarySense(ScrapperController character)
        {
            _isCharacterInsideSecondarySenseRadius = true;
            debug.Log("Character entered secondary sense area.");
        }
        
        private void OnTargetLostBySecondarySense(ScrapperController character)
        {
            _isCharacterInsideSecondarySenseRadius = false;
            debug.Log("Character leaved secondary sense area.");
        }

        private void OnPlayerSighted()
        {
            UpdateUI();
        }
        
        private void OnPlayerOutOfSight()
        {            
            UpdateUI();
        }
        
        private void UpdateUI()
        {
            if (_isCharacterInSight)
            {
                imageStatusIcon.SetData(spriteExclamationIcon);
                imageStatusIcon.Show();
                return;
            }

            if (!_isCharacterInSight && _isCharacterInsideAwarenessRadius)
            {
                imageStatusIcon.SetData(spriteQuestionIcon);
                imageStatusIcon.Show();
                return;
            }
            
            imageStatusIcon.Hide();
        }
        
        private void OnDrawGizmos()
        {
            if (!detectionSettings || radiusDetector == null)
                return;
            
            radiusDetector.OnDrawGizmos();
        }
    }
}