using System.Collections.Generic;
using _Project.Scripts.Core.DamageSystem;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.ScavengeSystem;
using _Project.Scripts.GameServices;
using UnityEngine;

using Sirenix.OdinInspector;

using _Project.Scripts.Utilities;
using Enderlook.EventManager;

namespace _Project.Scripts.Core
{
    public interface IScrapperControllerProvider
    {
        ScrapperController Instance { get; }
    }

    public class ScrapperController : MonoBehaviour, IDamageable, IScrapperControllerProvider
    {
        [Title("Dependencies")]
        [SerializeField] private CharacterSettings settings;
        [SerializeField] private CharacterMovementController movementController;
        [SerializeField] private DetectorSettings interactablesDetectorSettings;
        [SerializeField] private Transform interactablesDetectorCenter;
        [SerializeField] private WeaponController weaponController;
        [SerializeField] private RadarController radarController;
        [SerializeField] private CharacterAnimationController animationController;

        [Title("Stats")] 
        [SerializeField] private ScrapperStats scrapperStats;
        
        [Title("Other Settings")] 
        [SerializeField] private bool alwaysDrawGizmos;

        public ScrapperController Instance => this;
        
        private float _speedAdditionalEffectsMultiplier;
        public float SpeedAdditionalEffectsMultiplier
        {
            get => _speedAdditionalEffectsMultiplier;
            set
            {
                _speedAdditionalEffectsMultiplier = value;
                if (_speedAdditionalEffectsMultiplier <= 0)
                    _speedAdditionalEffectsMultiplier = 1;
            }
        }

        public bool IsDashing => movementController.IsDashing;
        public bool IsDebugRun { get; set; }
        public bool IsOnScavengingProcess { get; set; }

        private Detector<BaseInteractable> _interactablesDetector;
        private BaseInteractable _currentInteractableTarget;
        private ScrapLevelObject _currentScrapLevelObjectTarget;
        
        private CharacterHealth _health;
        
        private Vector2 _movementInputVector;
        private Vector3 _characterFacingDirection;
        private Vector2 _aim;
    
        private bool _isRunPressed;
        private bool _isInteractionPressed;
        private bool _isRadarPressed;
        private bool _isInvulnerable;
        private bool _isInputEnable;
        private bool _isInsideExitZone;
        
        private void Awake()
        {
            Services.Add<IScrapperControllerProvider>(this);
            
            _isInputEnable = true;
            SpeedAdditionalEffectsMultiplier = 1;
            
            _interactablesDetector = new Detector<BaseInteractable>(interactablesDetectorCenter, interactablesDetectorSettings);
            _interactablesDetector.OnNewDetection += OnNewTargetsDetectionProcessed;
            
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);
        }

        private void OnDestroy()
        {
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
            EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);
        }

        public void Setup()
        {   
            SetData(settings);
            
            SetRandomStats();
        }
        
        public void SetData(CharacterSettings newSettings)
        {
            settings = newSettings;
            movementController.SetData(newSettings);
        }
        
        private void SetRandomStats()
        {
            ScrapperGenerator generator = new ScrapperGenerator(settings);
            scrapperStats = generator.GetRandomStats();

            float initialHealth = generator.GetInitialHealth(scrapperStats);
            _health = new CharacterHealth(initialHealth);
            _health.HealthDepleted += OnHealthDepleted;

            float initialSpeed = generator.GetInitialSpeed(scrapperStats);
            movementController.SetNewSpeed(initialSpeed);
            
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.StatsChanged(scrapperStats));
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.HealthAttributeChanged(initialHealth));
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.SpeedAttributeChanged(initialSpeed));
        }
        
        public void AddHealth(float totalHealthToAdd)
        {
            _health.AddHealth(totalHealthToAdd);
        }
        
        private void Update()
        {
            if (IsDashing)
                return;
            
            _interactablesDetector.Update();
            TryToInteract();
            TryChargeRadar();
            
            var moveData = new MoveInputData(
                moveInputVector: _movementInputVector,
                lookInputVector: _characterFacingDirection,
                runInputPressed: _isRunPressed
            );

            movementController.SetInputs(moveData);

            if (moveData.MoveInputVector.magnitude < settings.minMovementInputMagnitude)
            {
                moveData.MoveInputVector = Vector2.zero;
            }
            animationController.SetMovementAnimation(moveData);
        }
        
        public void GetHit(AttackData attackData)
        {
            _health.TakeDamage(attackData.Damage);
        }
        
        public void SetPosition(Vector3 position)
        {
            movementController.SetPosition(position);
        }
        
        public void DebugMove(Vector3 position)
        {
            movementController.MoveCharacter(position);            
        }
        
        public void Move(Vector2 moveInput)
        {
            if (!_isInputEnable)
                return;
            
            _movementInputVector = moveInput;
        }

        public void Rotate(Vector2 directionInput)
        {
            if (!_isInputEnable)
                return;
            
            _characterFacingDirection = directionInput;
        }
        
        public void Run(bool isRunning)
        {
            _isRunPressed = isRunning;
            animationController.SetRunningAnimation(isRunning);
        }
        
        public void Dash()
        {
            if (!_isInputEnable && !weaponController.IsAttacking)
                return;

            animationController.SetDashingAnimation();
            movementController.Dash();
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.CharacterDashStarted());
        }
        
        public void HandleInteraction(bool isInteractionPressed)
        {
            _isInteractionPressed = isInteractionPressed;
        }
        
        public void StartAttacking()
        {
            if (!_isInputEnable)
                return;

            weaponController.StartAttacking();
        }
        
        
        public void StopAttacking()
        {
            if (!_isInputEnable)
                return;
            
            weaponController.StopAttacking();
        }
        
        public void TryToInteract()
        {
            if (!_isInputEnable || !_isInteractionPressed || !_currentInteractableTarget)
            {
                animationController.SetScrappingAnimation(false || IsOnScavengingProcess);
                return;
            }

            // TODO: This would benefit from a refactor: Use event instead of polling
            if (_currentScrapLevelObjectTarget)
            {
                HandleGrabScrap();
                animationController.SetScrappingAnimation(true);
                return;
            }
            
            _currentInteractableTarget.Interact(gameObject, out var resultData);
        }
        
        // Note: This is only for the purpose of doing things fast, its gona change. 
        public void HandleGrabScrap()
        {
            if (!_currentScrapLevelObjectTarget.Interact(gameObject, out InteractionResultData resultData))
            {
                return;
            }

            var scrapLevelObject = resultData.Data as ScrapLevelObject;
            if (scrapLevelObject == null)
            {
                Debug.LogError("Scavenge data is null.");
                return;
            }

            IsOnScavengingProcess = true;
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.ScrapInteractionCompleted(scrapLevelObject));
        }
        
        private void OnNewTargetsDetectionProcessed(List<BaseInteractable> detectedComponents)
        {
            if (detectedComponents == null || detectedComponents.Count <= 0)
            {
                _currentInteractableTarget = null;
                return;
            }

            if (_currentInteractableTarget != detectedComponents[0])
            {
                _currentInteractableTarget = detectedComponents[0];
                return;
            }

            _currentScrapLevelObjectTarget = null;
            if (_currentInteractableTarget is ScrapLevelObject newDetectedScrap)
            {
                _currentScrapLevelObjectTarget = newDetectedScrap;
            }
        }

        public void RadarStarted()
        {
            _isRadarPressed = true;
            radarController.BeginCharge();
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.RadarChargeStarted());
        }
        
        public void RadarCanceled()
        {
            _isRadarPressed = false;
            radarController.EndCharge();
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.RadarChargeReleased());
        }
        
        private void TryChargeRadar()
        {
            if (!_isInputEnable || !_isRadarPressed)
                return;
            
            radarController.Charge();
        }
        
        private void OnCharacterEnteredExitZone(GameEvents.CharacterEvents.CharacterEnteredExitZone obj)
        {
            _isInsideExitZone = true;
        }
        
        private void OnCharacterLeavedExitZone(GameEvents.CharacterEvents.CharacterLeavedExitZone obj)
        {
            _isInsideExitZone = false;
        }
        
        public void EnableInputs()
        {
            _isInputEnable = true;
        }
        
        public void StopInputs()
        {
            _isInputEnable = false;
            
            _isRunPressed = false;
            _isInteractionPressed = false;
            _isRadarPressed = false;
            _movementInputVector = Vector2.zero;
        }
        
        private void OnHealthDepleted()
        {
            _health.HealthDepleted -= OnHealthDepleted;

            StopInputs();
            StopAllCoroutines();
            EventManager.Shared.Raise<GameEvents.CharacterEvents.CharacterDied>();
        }

        private void OnDrawGizmos()
        {
            if (alwaysDrawGizmos)
            {
                OnDrawGizmosSelected();
            }
        }

        private void OnDrawGizmosSelected()
        {
            if (_interactablesDetector != null)
            {
                _interactablesDetector.OnDrawGizmos();
            }
        }

    }
}
