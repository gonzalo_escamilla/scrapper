using System;
using _Project.Scripts.Utilities;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace _Project.Scripts.Core
{
    [CreateAssetMenu(menuName = "Scrapper/Create Character Settings", fileName = "CharacterSettings", order = 0)]
    public class CharacterSettings : ScriptableObject
    {
        public float maxStableMoveSpeed => movementSettings.maxSpeed;
        public float minStableMoveSpeed => movementSettings.minSpeed;
        public float maxStableMoveSpeedWhenAttacking => movementSettings.attackSpeedModifier;
        public float attackImpulse => movementSettings.attackRecoil;
        public float minMovementInputMagnitude => movementSettings.minMovementInputMagnitude;
        public bool normalizeMovement => movementSettings.normalizeMovement;
        public bool normalizeLook => movementSettings.normalizeLook;
        public int stableMovementSharpness => movementSettings.inertia;
        public float orientationSharpness => movementSettings.rotationSpeed;
        public bool orientTowardsGravity => otherSettings.orientTowardsGravity;
        public Vector3 gravity => otherSettings.gravity;
        public float runMultiplier => movementSettings.runMultiplier;
        public float runEnergyCost => movementSettings.runEnergyCost;

        public float maxAirMoveSpeed => otherSettings.maxAirMoveSpeed;
        public float airAccelerationSpeed=> otherSettings.airAccelerationSpeed;
        public float drag => otherSettings.drag;

        public bool usesDashPoints => dashSettings.usesDashPoints;
        public int dashPoints => dashSettings.dashPoints;
        public float dashEnergyCost => dashSettings.dashEnergyCost;
        public float dashDuration => dashSettings.duration;
        public float maxStableDashMoveSpeed => dashSettings.speed;
        public float dashCooldown => dashSettings.cooldown;
        // public float bufferedDashPercentage => dashSettings.bufferedDashPercentage;
        public Vector2 dashInvulnerabilityRange => dashSettings.invulnerabilityRange;


        [TabGroup("General")]
        [SerializeField]
        private MovementSettings movementSettings;
        
        [TabGroup("General")]
        [SerializeField]
        private DashSettings dashSettings;
        
        [TabGroup("Health")]
        [SerializeField]
        public HealthSettings healthSettings;
        
        [TabGroup("Stats")]
        [SerializeField]
        public StatsSettings statsSettings;
        
        
        [TabGroup("Advanced")]
        public OtherSettings otherSettings;
        
        [Serializable]
        [HideLabel]
        private struct MovementSettings
        {
            [TitleGroup("Movement Settings")]
            public float maxSpeed;
            public float minSpeed;
            public int inertia;
            public float rotationSpeed;
            public float attackSpeedModifier;
            public float attackRecoil;
            [Range(0,1)] public float minMovementInputMagnitude;
            public bool normalizeMovement;
            public bool normalizeLook;
            [TitleGroup("Run Settings")]
            public float runMultiplier;
            public float runEnergyCost;
        }
        
        [Serializable]
        [HideLabel]
        public class DashSettings
        {
            [TitleGroup("Dash Settings")]
            [OnValueChanged(nameof(UpdateDashDistanceValue))] public float duration;
            [OnValueChanged(nameof(UpdateDashDistanceValue))] public float speed;
            [ReadOnly] public float distance;
            public float cooldown;
            // [SerializeField] [Range(0.0f, 1.0f)] public float bufferedDashPercentage;
            public bool usesDashPoints;
            public int dashPoints;
            public float dashEnergyCost;
            [MinMaxSlider(0f,1f)][SerializeField] public Vector2 invulnerabilityRange;
            
            private void UpdateDashDistanceValue()
            {
                distance = speed * duration;
            }
        }

        [Serializable]
        [HideLabel]
        public class HealthSettings
        {
            public float InitialHealth;
            public float DamagePerSecond;
        }
        [Serializable]
        [HideLabel]
        public class StatsSettings
        {
            [TitleGroup("Endurance")]

            public float healthBaseValue;
            public float enduranceToHealthScale;
            public Vector2 enduranceMinMaxRange;

            [TitleGroup("Agility")]
            public bool usesInGameGeneratedSpeed;
            public float speedBaseValue;
            public float agilityToSpeedScale;
            public Vector2 speedMinMaxRange;
            
            [TitleGroup("Debug")]
            [SerializeField] private ScrapperStats randomStats;
            [SerializeField] private float debugInitialHealth;
            [SerializeField] private float debugInitialSpeed;

            public ScrapperGenerator Generator;

            public void SetGenerator(ScrapperGenerator generator)
            {
                Generator = generator;
            }
            
            [Button("Randomize")]
            public void Randomize()
            {
                randomStats = Generator.GetRandomStats();
                debugInitialHealth = Generator.GetInitialHealth(randomStats);
                debugInitialSpeed = Generator.GetInitialSpeed(randomStats);
            }
        }
        
        [Serializable]
        [HideLabel]
        public struct OtherSettings
        {
            [TitleGroup("Air Settings")]
            public float maxAirMoveSpeed;
            public float airAccelerationSpeed;
            public float drag;
            
            [TitleGroup("Other Settings")]
            public bool orientTowardsGravity;
            public Vector3 gravity;
        }

        private void OnValidate()
        {
            if (this == null || Application.isPlaying)
            {
                return;
            }
            
            if (statsSettings is { Generator: null })
            {
                statsSettings.Generator = new ScrapperGenerator(this);
            }
        }

#if UNITY_EDITOR
        [PropertySpace(25)]
        [EnableIf(nameof(IsEditorPlaying))]
        [Button("Use This Setting",ButtonSizes.Large), GUIColor(0.4f, 0.8f, 1)]
        private void UseThisSetting()
        {
            DeveloperTools.GetCurrentCharacter()?.SetData(this);
        }

        bool IsEditorPlaying()
        {
            return Application.isPlaying;
        }
#endif
    }
}