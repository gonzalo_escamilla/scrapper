﻿using System;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.NarrativeSystem;
using Enderlook.EventManager;

namespace _Project.Scripts.Core
{
    public class CharacterHealth
    {
        public Action HealthDepleted;

        private float _maxHealth;
        private float _previousValue;
        private float _currentValue;
        
        public float CurrentValue
        {
            get => _currentValue;
            private set
            {
                if (value < _currentValue) 
                {
                    _previousValue = _currentValue; 
                    _currentValue = value;

                    if (_currentValue <= 0)
                    {
                        _currentValue = 0;
                        HealthDepleted?.Invoke();
                    }
                    EventManager.Shared.Raise(new GameEvents.CharacterEvents.HealthLost(_currentValue));
                }
                else
                {
                    _currentValue = value;
                }
                
                EventManager.Shared.Raise(new GameEvents.CharacterEvents.HealthChanged(_currentValue));
                
                // Note: New Maximum health.
                if (_currentValue > _maxHealth)
                {
                    _maxHealth = _currentValue;
                }
            }
        }

        public CharacterHealth(float maxHealth)
        {
            CurrentValue = maxHealth;
            _maxHealth = maxHealth;
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.InitialHealthSet(_currentValue));
            EventManager.Shared.Raise(new GameEvents.Dialogue.InkVariableUpdated(InkConstants.CurrentHealthPercentage, 1));   
        }
        
        public void TakeDamage(float healthSettingsDamagePerSecond)
        {
            CurrentValue -= healthSettingsDamagePerSecond;

            EventManager.Shared.Raise(new GameEvents.CharacterEvents.InitialHealthSet(_currentValue));
            EventManager.Shared.Raise(new GameEvents.Dialogue.InkVariableUpdated(InkConstants.CurrentHealthPercentage, CurrentValue / _maxHealth));  
        }

        public void AddHealth(float totalHealthToAdd)
        {
            CurrentValue += totalHealthToAdd;
        }
    }
}