﻿using _Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.Core
{
    [CreateAssetMenu(menuName = "Scrapper/Enemies/Character Detector Settings", fileName = "EnemySettings", order = 0)]
    public class EnemyDetectionControllerSettings : ScriptableObject
    {
        public float sightDistance;
        [Range(0,360)] public float detectionAreaAngle;
        public LayerMask obstaclesLayerMaks;
        public DetectorSettings detectionRadiusDetectorSettings;
        public DetectorSettings secondarySenseDetectorSettings;
        public float reach;
    }
}