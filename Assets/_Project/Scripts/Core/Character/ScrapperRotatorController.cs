﻿using UnityEngine;

namespace _Project.Scripts.Core
{
    public class ScrapperRotatorController : MonoBehaviour
    {
        [SerializeField] private Transform graphicsTransform ;
        public Vector3 Forward => graphicsTransform.forward;
        public Vector3 Up  => graphicsTransform.up;

        public void SetRotation(Quaternion currentGraphicsRotation)
        {
            graphicsTransform.rotation = currentGraphicsRotation;
        }
    }
}