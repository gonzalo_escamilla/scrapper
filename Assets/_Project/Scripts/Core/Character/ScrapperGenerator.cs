﻿using UnityEngine;

namespace _Project.Scripts.Core
{
    public class ScrapperGenerator
    {
        private CharacterSettings _settings;

        public ScrapperGenerator(CharacterSettings settings)
        {
            _settings = settings;
        }

        public float GetInitialHealth(ScrapperStats stats)
        {
            float enduranceMultiplier = (stats.Endurance + _settings.statsSettings.enduranceToHealthScale) 
                                        / _settings.statsSettings.enduranceToHealthScale;

            return _settings.statsSettings.healthBaseValue * enduranceMultiplier;
        }

        public float GetInitialSpeed(ScrapperStats stats)
        {
            float agilityMultiplier = (stats.Agility + _settings.statsSettings.agilityToSpeedScale) 
                                        / _settings.statsSettings.enduranceToHealthScale;

            return _settings.statsSettings.speedBaseValue * agilityMultiplier;
        }
        
        public ScrapperStats GetRandomStats()
        {
            float randomEndurance = GetRandomEndurance();
            float randomAgility = GetRandomAgility();

            ScrapperStats newStats = new ScrapperStats(randomEndurance, randomAgility);
            return newStats;
        }

        private float GetRandomAgility()
        {
            float randomAgility = Random.Range(_settings.statsSettings.speedMinMaxRange.x,
                _settings.statsSettings.speedMinMaxRange.y);
            
            return randomAgility;
        }

        private float GetRandomEndurance()
        {
            float randomEndurance = Random.Range(_settings.statsSettings.enduranceMinMaxRange.x,
                _settings.statsSettings.enduranceMinMaxRange.y);
            
            return randomEndurance;
        }
    }
}