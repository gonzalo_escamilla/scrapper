﻿using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Utilities;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public class CharacterAudioController : MonoBehaviour
    {
        [SerializeField] private AnimatorEventListener animatorEventListener;

        private const string FootstepEventKey = "Footstep";
        
        private void OnEnable()
        {
            animatorEventListener.AddListener(FootstepEventKey, OnFootstep);
        }

        private void OnDisable()
        {
            animatorEventListener.ClearEvents();
        }

        private void OnFootstep(AnimationEventData data)
        {
            AkSoundEngine.PostEvent(data.EventKey, gameObject);
        }
    }
}