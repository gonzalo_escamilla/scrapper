using System;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public class CharacterAnimationController : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        private readonly int _hashIsMoving = Animator.StringToHash("IsMoving");
        private readonly int _hashVelocity = Animator.StringToHash("Velocity");
        private readonly int _hashIsRunning = Animator.StringToHash("IsRunning");
        private readonly int _hashDashTrigger = Animator.StringToHash("Dash");
        private readonly int _hashIsScrapping = Animator.StringToHash("IsScrapping");
        private readonly int _hashIsDead = Animator.StringToHash("IsDead");

        public void SetMovementAnimation(MoveInputData moveData)
        {
            animator.SetBool(_hashIsMoving, moveData.MoveInputVector.magnitude > 0);
            animator.SetFloat(_hashVelocity, moveData.MoveInputVector.magnitude);
        }

        public void SetRunningAnimation(bool isRunning)
        {
            animator.SetBool(_hashIsRunning, isRunning);
        }

        public void SetDashingAnimation()
        {
            animator.SetTrigger(_hashDashTrigger);
        }

        public void SetScrappingAnimation(bool isScrapping)
        {
            animator.SetBool(_hashIsScrapping, isScrapping);
        }

        public void SetDeathAnimation(bool isDead)
        {
            animator.SetBool(_hashIsDead, isDead);
        }
    }
}