﻿using System;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.Player;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public class PlayerDebugHelper : MonoBehaviour
    {
        [SerializeField] private PlayerController playerController;
        [SerializeField] private ScrapperController scrapperController;
        [SerializeField] private Vector2 _planarMovementVector;
        [SerializeField] private float _verticalMovement;
        [SerializeField] private Transform debugMoveTarget;
        [SerializeField] private float speed;
        [SerializeField] private float bustSpeed;
        
        LevelController _levelController;
        private Camera _camera;
        private float _currentSpeed;
        private bool _isCharacterDebugMode;
        
        private void Awake()
        {
            playerController ??= FindObjectOfType<PlayerController>();
            scrapperController ??= FindObjectOfType<ScrapperController>();
            _camera ??= Camera.main;
        }

        private void Start()
        {
            Services.WaitFor<ILevelControllerProvider>(OnLevelControllerProvider);
            _levelController ??= FindObjectOfType<LevelController>();
        }

        private void OnLevelControllerProvider(ILevelControllerProvider provider)
        {
            _levelController = provider.LevelControllerInstance;
        }

        private void Update()
        {
            return;
            
            if (_levelController == null)
            {
                return;
            }
            
            _planarMovementVector = Vector2.zero;
            _verticalMovement = 0;

            HandleSpawnTransition();
            HandleScrapSwitchTransition();
            
            // if (Input.GetKeyDown(KeyCode.Escape))
            //     EventManager.Shared.Raise<GameEvents.GameStateEvents.ApplicationResetTriggered>();
            
            if (Input.GetKeyDown(KeyCode.H))
            {
                _isCharacterDebugMode = !_isCharacterDebugMode;
                SetDebugMode();
            }
            
            if (!_isCharacterDebugMode)
                return;

            if (Input.GetKey(KeyCode.W))
            {
                _planarMovementVector += Vector2.up;
            }
            
            if (Input.GetKey(KeyCode.A))
            {
                _planarMovementVector += Vector2.left;
            }
            
            if (Input.GetKey(KeyCode.S))
            {
                _planarMovementVector += Vector2.down;
            }
            
            if (Input.GetKey(KeyCode.D))
            {
                _planarMovementVector += Vector2.right;
            }
            
            if (Input.GetKey(KeyCode.Q))
            {
                _verticalMovement = -1;
            }
            
            if (Input.GetKey(KeyCode.E))
            {
                _verticalMovement = 1;
            }

           
            _currentSpeed = Input.GetKey(KeyCode.LeftShift) ? bustSpeed : speed;
            _planarMovementVector.Normalize();

            MoveDebugTarget();
            MoveCharacter();
        }

        private void HandleScrapSwitchTransition()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                SetGameplayContainer(0);
            if (Input.GetKeyDown(KeyCode.Alpha2))
                SetGameplayContainer(1);
        }

        private void HandleSpawnTransition()
        {
            SetRandomExitZonePosition();
        }

        private void SetRandomExitZonePosition()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                scrapperController.SetPosition(_levelController.GetRandomExitZone().Center);
            }
        }

        private void SetGameplayContainer(int index)
        {
            _levelController.ForceSetupGameplayContainer(index);
        }

        private void SetDebugMode()
        {
            if (_isCharacterDebugMode)
            {
                scrapperController.StopInputs();
                debugMoveTarget.position = scrapperController.transform.position;
                return;
            }
            scrapperController.EnableInputs();
            scrapperController.DebugMove(debugMoveTarget.position);
        }
        
        private void MoveDebugTarget()
        {
            if (!_isCharacterDebugMode)
                return;

            // Get the camera's forward and right directions
            Vector3 cameraForward = _camera.transform.forward;
            Vector3 cameraRight = _camera.transform.right;

            // Ignore the vertical component of the camera's forward direction
            cameraForward.y = 0f;
            cameraForward.Normalize();
            cameraRight.y = 0f;
            cameraRight.Normalize();

            // Calculate the movement vector based on camera orientation
            Vector3 movement = cameraForward * _planarMovementVector.y + cameraRight * _planarMovementVector.x;
            movement.y = _verticalMovement;

            // Update the position of the debug target
            debugMoveTarget.position += movement * (_currentSpeed * Time.deltaTime);
        }
        
        private void MoveCharacter()
        {
            scrapperController.DebugMove(debugMoveTarget.position);
        }
    }
}