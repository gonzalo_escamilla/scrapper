﻿namespace _Project.Scripts.Core
{
    public interface IActionable
    {
        bool IsActionable { get; }
        void PerformAction();
        void UndoAction();
    }
}