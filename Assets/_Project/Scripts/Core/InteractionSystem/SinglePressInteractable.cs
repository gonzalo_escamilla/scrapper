using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace _Project.Scripts.Core
{
    /// <summary>
    /// For single use Interactables
    /// </summary>
    public class SinglePressInteractable : BaseInteractable
    {
        [SerializeField] private bool enableOnAwake;
        [SerializeField] private bool singleUse;
        [HideIf(nameof(singleUse))][SerializeField] private float cooldown;
        
        public UnityEvent OnInteract;

        public bool IsEnable { get; set; }

        private int _usesAmount;
        private bool _isCoolingDown;
        
        public virtual void Start()
        {
            if (!enableOnAwake)
                return;

            IsEnable = true;
        }

        public void Reset()
        {
            StopAllCoroutines();

            _usesAmount = 0;
        }

        public override bool Interact(GameObject interactionSource, out InteractionResultData interactionResultData)
        {
            interactionResultData = new InteractionResultData();
            
            if (!IsEnable || _isCoolingDown)
                return false;

            if (singleUse && _usesAmount > 0)
                return false;

            _usesAmount++;

            Debug.Log($"Interacted by {interactionSource}", interactionSource);
            OnInteract?.Invoke();
            StartCoroutine(Cooldown());
            return true;
        }
        private IEnumerator Cooldown()
        {
            _isCoolingDown = true;
            yield return new WaitForSeconds(cooldown);
            _isCoolingDown = false;
        }
    }
}