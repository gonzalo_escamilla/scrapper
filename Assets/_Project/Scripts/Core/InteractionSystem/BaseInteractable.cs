using UnityEngine;

namespace _Project.Scripts.Core
{
    public struct InteractionResultData
    {
        /// <summary>
        /// Indicates whether the interaction was successful.
        /// </summary>
        public bool IsSuccessful;

        /// <summary>
        /// A message or additional information about the result of the interaction.
        /// </summary>
        public string Message;

        /// <summary>
        /// Any other data related to the interaction result.
        /// </summary>
        public object Data;

        public InteractionResultData(bool isSuccessful, string message, object data = null)
        {
            IsSuccessful = isSuccessful;
            Message = message;
            Data = data;
        }
    }
    
    public abstract class BaseInteractable : MonoBehaviour
    {
        /// <summary>
        /// Handles the interaction between the provided source and the target, returning a result if successful.
        /// </summary>
        /// <param name="interactionSource">The game object that is initiating the interaction.</param>
        /// <param name="interactionResultData">The result of the interaction, if any, output as an object.</param>
        /// <returns>Returns true if the interaction was successful or completed, false otherwise.</returns>
        public abstract bool Interact(GameObject interactionSource, out InteractionResultData interactionResultData);
    }
}