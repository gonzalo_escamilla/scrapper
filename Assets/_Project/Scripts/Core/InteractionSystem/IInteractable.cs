using UnityEngine;

namespace _Project.Scripts.Core
{
    public interface IInteractable
    {
        public void Interact(GameObject interactionSource);
    }
}