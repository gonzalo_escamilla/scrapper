using _Project.Scripts.Core;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.GameStates;
using _Project.Scripts.Core.NarrativeSystem;
using _Project.Scripts.GameServices;
using UnityEngine;
using Logger = _Project.Scripts.GameServices.Logger;

public class Bootstrapper : MonoBehaviour
{
    [SerializeField] private RootCanvasView rootCanvasView;
    [SerializeField] private InputProvider inputProvider;
    [SerializeField] private Logger globalLogger;
    [SerializeField] private GameStateController gameStateController;
    [SerializeField] private FadeScreenController fadeScreenController;
    [SerializeField] private GameSettingsProvider gameSettingsProvider;
    [SerializeField] private DialogueController dialogueController;
    
    private CoroutineService _coroutineService;
    private IMenuInstanceProvider _menuInstanceProvider;
    private ISceneLoader _sceneLoader;
    
    private void Awake()
    {
        DontDestroyOnLoad(this);
        
        Services.Add<IDebug>(globalLogger);
        Services.Add<IGameSettingsProvider>(gameSettingsProvider);
        Services.Add<IInputProvider>(inputProvider);
        Services.Add<IRootCanvasProvider>(rootCanvasView);
        Services.Add<IFadeScreenService>(fadeScreenController);

        _menuInstanceProvider = new MenuInstanceFactory();
        Services.Add<IMenuInstanceProvider>(_menuInstanceProvider);
        
        Services.Add<GameStateController>(gameStateController);
        
        _coroutineService = new CoroutineService(this);
        Services.Add<CoroutineService>(_coroutineService);
        
        _sceneLoader = new SceneLoader();
        Services.Add<ISceneLoader>(_sceneLoader);
        
        Services.Add<DialogueController>(dialogueController);
    }

    private void Start()
    {
        StartGame();
    }
    
    private void StartGame()
    {
        gameStateController.Initialize();
    }
}
