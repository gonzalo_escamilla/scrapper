﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.InventorySystem
{
    public class InventoryItem : MonoBehaviour
    {
        [SerializeField] private InventoryItemSettings inventoryItemSettings;
        [SerializeField] private Image image;
        
        public InventoryItemShape CurrentShape { get; set; }
        public List<InventorySlot> OverlapedSlots { get; set; }
        public RectTransform RectTransform => _rectTransform;

        private InventoryItemShape _baseShape;
        private RectTransform _rectTransform;
        private RectTransform _imageRectTransform;
        
        public void SetData(InventoryItemSettings settings)
        {
            inventoryItemSettings = settings;
            
            _rectTransform = GetComponent<RectTransform>();
            _imageRectTransform = image.GetComponent<RectTransform>();
                
            _baseShape = new InventoryItemShape();
            _baseShape.SetData(inventoryItemSettings.ShapeSettings);

            CurrentShape = new InventoryItemShape();
            CurrentShape.SetData(inventoryItemSettings.ShapeSettings);

            image.sprite = inventoryItemSettings.ItemImage;
            image.SetNativeSize();
            _rectTransform.localScale = Vector3.one * 0.6f;
            
            OverlapedSlots = new();
            _rectTransform = GetComponent<RectTransform>();
        }

        public void SetGridPosition(Vector2 position, RectTransform parent)
        {
            _rectTransform.SetParent(parent);
            _rectTransform.anchoredPosition = position;
        }
        
        public void Rotate()
        {
            RotateShape();
            RotateImage();
        }
        
        private void RotateShape()
        {
            // Ensure _currentShape is not null and has data
            if (CurrentShape != null && CurrentShape.Data != null)
            {
                int size = CurrentShape.Data.GetLength(0);
                bool[,] rotatedData = new bool[size, size];

                // Rotate the shape data 90 degrees clockwise around the top-left corner (0,0)
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        rotatedData[j, size - 1 - i] = CurrentShape.Data[i, j];
                    }
                }

                // Shift the rotated data to the top-left corner as much as possible
                rotatedData = ShiftToTopLeft(rotatedData);

                // Update _currentShape with rotated and shifted data
                CurrentShape.Data = rotatedData;
            }
            else
            {
                Debug.LogWarning("Current shape or its data is null.");
            }
        }

        private void RotateImage()
        {
            _imageRectTransform.Rotate(0, 0, 90);
        }
        
        private bool[,] ShiftToTopLeft(bool[,] data)
        {
            int size = data.GetLength(0);
            int minX = size;
            int minY = size;

            // Find the minimum x and y indices with a true value
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (data[i, j])
                    {
                        if (i < minX)
                            minX = i;
                        if (j < minY)
                            minY = j;
                    }
                }
            }

            // Shift the data to the top-left corner based on minX and minY
            bool[,] shiftedData = new bool[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i + minX < size && j + minY < size)
                        shiftedData[i, j] = data[i + minX, j + minY];
                }
            }

            return shiftedData;
        }
    }
}