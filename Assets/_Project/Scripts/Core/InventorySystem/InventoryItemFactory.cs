using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Pool;

namespace _Project.Scripts.Core.InventorySystem
{
    public class InventoryItemFactory
    {
        private InventoryItem _itemPrefab;
        private ObjectPool<InventoryItem> _objectPool;
        
        public void Init(InventoryItem itemPrefab)
        {
            _itemPrefab = itemPrefab;
            
            _objectPool = new ObjectPool<InventoryItem>(
                CreateItem,
                OnGetItem,
                OnReturnItem,
                OnDestroyItem,
                false,
                20,
                30);
        }

        public InventoryItem GetItem(InventoryItemSettings settings)
        {
            var item = _objectPool.Get();
            item.SetData(settings);
            
            return item;
        }
        
        private InventoryItem CreateItem()
        {
            var instance = Object.Instantiate(_itemPrefab, Vector3.zero, quaternion.identity);
            return instance;
        }

        private void OnGetItem(InventoryItem instance)
        {
            instance.gameObject.SetActive(true);
        }
        
        private void OnReturnItem(InventoryItem instance)
        {
            instance.gameObject.SetActive((false));
        }
        private void OnDestroyItem(InventoryItem instance)
        {
            Object.Destroy(instance);
            Object.Destroy(instance.gameObject);
        }
    }
}