﻿using UnityEngine;

namespace _Project.Scripts.Core.InventorySystem
{
    public static class Grid2DUtils
    {
        public static Vector2Int GetMovementIndex(GridDirection direction, Vector2Int currentIndex)
        {
            Vector2Int movementVector = direction switch
            {
                GridDirection.Up => new Vector2Int(0, -1),
                GridDirection.Down => new Vector2Int(0, 1),
                GridDirection.Left => new Vector2Int(-1, 0),
                GridDirection.Right => new Vector2Int(1, 0),
                _ => Vector2Int.zero
            };

            Vector2Int newSelectionIndex = currentIndex + movementVector;
            return newSelectionIndex;
        }
    }
}