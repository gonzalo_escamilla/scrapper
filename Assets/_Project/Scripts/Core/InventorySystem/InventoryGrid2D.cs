﻿using System.Collections.Generic;
using _Project.Scripts.Core.InventorySystem;
using UnityEngine;

namespace _Project.Scripts.Core
{
    public struct ItemPlacementData
    {
        public InventoryItem Item;
        public bool CanFit;
        public List<Vector2Int> availableIdexes;
        public List<Vector2Int> occupiedIndexes;

        public ItemPlacementData(InventoryItem item, bool canFit, List<Vector2Int> availableIdexes, List<Vector2Int> occupiedIndexes)
        {
            Item = item;
            CanFit = canFit;
            this.availableIdexes = availableIdexes;
            this.occupiedIndexes = occupiedIndexes;
        }
    }
    
    public class InventoryGrid2D
    {
        private InventorySlot[,] grid;
        private int width;
        private int height;

        public int Width => width;
        public int Height => height;

        public InventoryGrid2D(int width, int height)
        {
            this.width = width;
            this.height = height;
            grid = new InventorySlot[width, height];
        }

        // Indexer to access grid elements
        public InventorySlot this[int x, int y]
        {
            get
            {
                if (IsInsideGrid(x, y))
                {
                    return grid[x, y];
                }
                Debug.LogError($"Position outside the grid ({x},{y})");
                return null;
            }
            set
            {
                if (IsInsideGrid(x, y))
                {
                    grid[x, y] = value;
                }
                else
                {
                    Debug.LogError($"Position outside the grid ({x},{y})");
                }
            }
        }

        // Method to check if a position is within the bounds of the grid
        public bool IsInsideGrid(int x, int y)
        {
            bool isInside = x >= 0 && y >= 0 && x < width && y < height;
            return isInside;
        }

        public bool IsSlotAvailable(int x, int y)
        {
            return this[x,y].IsAvailable;
        }
        
        // Method to clear the grid
        public void Clear()
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    grid[x, y] = default(InventorySlot);
                }
            }
        }

        // Method to fill the grid with a specific value
        public void Fill(InventorySlot value)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    grid[x, y] = value;
                }
            }
        }

        // Method to get a row of the grid
        public InventorySlot[] GetRow(int y)
        {
            if (y < 0 || y >= height)
            {
                throw new System.IndexOutOfRangeException($"Invalid row index: {y}");
            }

            InventorySlot[] row = new InventorySlot[width];
            for (int x = 0; x < width; x++)
            {
                row[x] = grid[x, y];
            }
            return row;
        }

        // Method to get a column of the grid
        public InventorySlot[] GetColumn(int x)
        {
            if (x < 0 || x >= width)
            {
                throw new System.IndexOutOfRangeException($"Invalid column index: {x}");
            }

            InventorySlot[] column = new InventorySlot[height];
            for (int y = 0; y < height; y++)
            {
                column[y] = grid[x, y];
            }
            return column;
        }

        // Method to get the entire grid as a 2D array
        public InventorySlot[,] GetGrid()
        {
            return grid;
        }

        // Method to set a range of values in the grid
        public void SetValues(int startX, int startY, InventorySlot[,] values)
        {
            int valuesWidth = values.GetLength(0);
            int valuesHeight = values.GetLength(1);

            if (!IsInsideGrid(startX, startY) ||
                !IsInsideGrid(startX + valuesWidth - 1, startY + valuesHeight - 1))
            {
                throw new System.IndexOutOfRangeException("Invalid range to set values");
            }

            for (int x = 0; x < valuesWidth; x++)
            {
                for (int y = 0; y < valuesHeight; y++)
                {
                    grid[startX + x, startY + y] = values[x, y];
                }
            }
        }

        public ItemPlacementData GetPlacementData(InventoryItem inventoryItem, int xIndex, int yIndex)
        {
            var data = inventoryItem.CurrentShape.Data;
            bool anyInvalidPosition = false;
            bool allAvailablePositions = true;
            List<Vector2Int> availableIndexes = new();
            List<Vector2Int> unAvailableIndexes = new();
            
            for (int x = 0; x < data.GetLength(0); x++)
            {
                for (int y = 0; y < data.GetLength(1); y++)
                {
                    if (!data[x,y])
                    {
                        continue;
                    }
                    
                    Vector2Int indexToCheck = new Vector2Int(x + xIndex, y + yIndex);
                    bool isInside = IsInsideGrid(indexToCheck.x, indexToCheck.y);

                    if (!isInside)
                    {
                        anyInvalidPosition = true;
                    }
                    else
                    {
                        bool isAvailable = IsSlotAvailable(indexToCheck.x, indexToCheck.y);
                        if (!isAvailable)
                        {
                            unAvailableIndexes.Add(indexToCheck);
                            allAvailablePositions = false;
                        }
                        else
                        {
                            availableIndexes.Add(indexToCheck);
                        }
                    }
                }
            }

            bool canBePlaced = !anyInvalidPosition && allAvailablePositions;

            ItemPlacementData placementData = new ItemPlacementData(inventoryItem, canBePlaced, availableIndexes, unAvailableIndexes);
            
            return placementData;
        }
    }
}
