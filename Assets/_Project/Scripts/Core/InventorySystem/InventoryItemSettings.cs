﻿using UnityEngine;

namespace _Project.Scripts.Core.InventorySystem
{
    [CreateAssetMenu(menuName = "Scrapper/Inventory/Create Inventory Item Setting", fileName = "InventoryItemSetting", order = 0)]
    public class InventoryItemSettings : ScriptableObject
    {
        [SerializeField] private ShapeSettings _shapeSettings;
        [SerializeField] private Sprite _itemImage;
        
        public ShapeSettings ShapeSettings => _shapeSettings;
        public Sprite ItemImage => _itemImage;
    }
}