﻿using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Core.InventorySystem
{
    public enum GridDirection
    {
        Up,
        Left,
        Down,
        Right
    }
    
    public static class InventoryUtilities
    {
        public static GridDirection FromDirectionToGridDirection(Vector2 direction)
        {
            direction.Normalize();

            if (Vector2.Dot(direction, Vector2.up) > 0.707f)
                return GridDirection.Up;
            if (Vector2.Dot(direction, Vector2.down) > 0.707f)
                return GridDirection.Down;
            if (Vector2.Dot(direction, Vector2.left) > 0.707f)
                return GridDirection.Left;
            if (Vector2.Dot(direction, Vector2.right) > 0.707f)
                return GridDirection.Right;

            throw new System.ArgumentException("Invalid direction vector");
        }
        
        public static bool IsInsideCurrentGrid(Vector2Int newSelectionIndex, InventoryGrid2D grid)
        {
            return grid.IsInsideGrid(newSelectionIndex.x, newSelectionIndex.y);
        }
    }
}