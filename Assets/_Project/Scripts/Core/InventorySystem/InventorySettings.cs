﻿using System;
using UnityEngine;

namespace _Project.Scripts.Core.InventorySystem
{
    [CreateAssetMenu(menuName = "Scrapper/Inventory/Create Inventory Settings", fileName = "InventorySettings", order = 0)]
    public class InventorySettings : ScriptableObject
    {
        [SerializeField] private InventorySlot inventorySlotPrefab;
        [SerializeField] private InventoryItem inventoryItemPrefab;
        [SerializeField] private int width;
        [SerializeField] private int height;
        [SerializeField] private int stashWidth;
        [SerializeField] private int stashHeight;
        [SerializeField] private int maxWeight; // Optional: Maximum weight the inventory can hold
        [SerializeField] private float slotSize; // Optional: Size of each inventory slot
        [SerializeField] private Vector2 slotSpacing; // Optional: Size of each inventory slot
        [SerializeField] private Vector2 slotOffset; // Optional: Size of each inventory slot
        [SerializeField] private Vector2 stashSlotOffset; // Optional: Size of each inventory slot
        [SerializeField] private Vector2 containerPadding; // Optional: Size of each inventory slot
        [SerializeField] private Vector2 stashContainerPadding; // Optional: Size of each inventory slot

        public InventorySlot InventorySlotPrefab => inventorySlotPrefab;
        public InventoryItem InventoryItemPrefab => inventoryItemPrefab;
        public int Width => width;
        public int Height => height;
        public int StashWidth => stashWidth;
        public int StashHeight => stashHeight;
        public int MaxWeight => maxWeight; // Optional
        public float SlotSize => slotSize; // Optional
        public Vector2 SlotSpacing => slotSpacing;
        public Vector2 SlotOffset => slotOffset;
        public Vector2 StashSlotOffset => stashSlotOffset;
        public Vector2 ContainerPadding => containerPadding;
        public Vector2 StashContainerPadding => stashContainerPadding;
    }
}