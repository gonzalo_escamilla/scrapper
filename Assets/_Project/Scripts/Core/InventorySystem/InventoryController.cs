﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.ScavengeSystem;
using _Project.Scripts.Core.ScavengeSystem.Items;
using DG.Tweening;
using Enderlook.EventManager;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Core.InventorySystem
{
    public enum InventoryState
    {
        None,
        HoveringInventory,
        HoveringStash
    }

    public class InventoryController : MonoBehaviour
    {
        [SerializeField] private InventorySettings inventorySettings;
        [SerializeField] private RectTransform inventoryContainerRect;
        [SerializeField] private CanvasGroup inventoryCanvasGroup;
        [SerializeField] private RectTransform stashContainerRect;
        [SerializeField] private CanvasGroup stashCanvasGroup;
        [SerializeField] private RectTransform selectionIndicator;
        
        public InventoryItem CurrentGrabbedItem => _currentGrabbedItem;
        public InventoryState CurrentInventoryState => _currentInventoryState;
        
        private InventoryGrid2D _currentUsedGrid;

        private InventoryGrid2D _inventoryGrid;
        private InventoryGrid2D _stashGrid;

        private InventoryItemFactory _inventoryItemFactory;
        
        private List<InventorySlot> _allInventorySlots = new();
        private List<InventorySlot> _allStashSlots = new();
        private List<InventoryItem> _itemsInInventory = new();
        
        private InventoryItem _currentGrabbedItem;
        
        private Vector2Int _currentSelectionIndex;

        private InventoryState _currentInventoryState;

        // Note: only for the proto
        private List<ScrapItemData> _protoInventory;
        
        private void Awake()
        {
            SetupInventoryGrid();
            SetupStashGrid();
            
            _currentSelectionIndex = Vector2Int.zero;

            _currentInventoryState = InventoryState.HoveringInventory; //TODO: move into a whoel property?
            _currentUsedGrid = _inventoryGrid;

            _inventoryItemFactory = new InventoryItemFactory();
            _inventoryItemFactory.Init(inventorySettings.InventoryItemPrefab);

            _protoInventory = new();
            
            EventManager.Shared.Subscribe<GameEvents.CharacterEvents.ScrapGrabbed>(OnScrapGrabbed);
        }
        
        public void MoveSelectionIndex(Vector2Int index)
        {
            bool isInside = InventoryUtilities.IsInsideCurrentGrid(index, _currentUsedGrid);
            if (!isInside) 
                return;
            
            _currentSelectionIndex = index;

            SetSelectionIndicatorPosition(_currentSelectionIndex);
        }
        
        public void MoveSelectionIndex(GridDirection direction)
        {
            Vector2Int newSelectionIndex = Grid2DUtils.GetMovementIndex(direction, _currentSelectionIndex);

            bool isInside = InventoryUtilities.IsInsideCurrentGrid(newSelectionIndex, _currentUsedGrid);
            if (!isInside) 
                return;
            
            _currentSelectionIndex = newSelectionIndex;

            SetSelectionIndicatorPosition(_currentSelectionIndex);
        }
        
        private void SetSelectionIndicatorPosition(Vector2Int currentSelectionIndex)
        {
            switch (_currentInventoryState)
            {
                case InventoryState.HoveringInventory:
                    selectionIndicator.SetParent(inventoryContainerRect);
                    selectionIndicator.anchoredPosition = _inventoryGrid[_currentSelectionIndex.x, _currentSelectionIndex.y].AnchoredPosition;
                    break;
                case InventoryState.HoveringStash:
                    selectionIndicator.SetParent(stashContainerRect);
                    selectionIndicator.anchoredPosition = _stashGrid[_currentSelectionIndex.x, _currentSelectionIndex.y].AnchoredPosition;
                break;
            }
        }
        
        public void Show()
        {
            inventoryCanvasGroup.interactable = true;
            inventoryCanvasGroup.blocksRaycasts = true;
            inventoryCanvasGroup.DOFade(1, 0.1f);
            
            stashCanvasGroup.interactable = true;
            stashCanvasGroup.blocksRaycasts = true;
            stashCanvasGroup.DOFade(1, 0.1f);
        }

        public void Hide()
        {
            inventoryCanvasGroup.interactable = false;
            inventoryCanvasGroup.blocksRaycasts = false;
            inventoryCanvasGroup.DOFade(0, 0.1f);
            
            stashCanvasGroup.interactable = false;
            stashCanvasGroup.blocksRaycasts = false;
            stashCanvasGroup.DOFade(0, 0.1f);
        }

        public void DropItem()
        {
            _currentGrabbedItem = null;
        }
        
        public void PreviewGrabbedItemPlacement()
        {
            if (!CurrentGrabbedItem)
            {
                Debug.LogError("There is non grabbed item.");
                return;
            }
            
            ClearPlacementForItem(CurrentGrabbedItem);
            var placementData = _currentUsedGrid.GetPlacementData(CurrentGrabbedItem, _currentSelectionIndex.x, _currentSelectionIndex.y);
            CurrentGrabbedItem.OverlapedSlots.Clear();
            foreach (var index in placementData.availableIdexes)
            {
                CurrentGrabbedItem.OverlapedSlots.Add(_currentUsedGrid[index.x,index.y]);
            }
            ClearPlacementForItem(CurrentGrabbedItem);

            MoveGrabbedItem(placementData);
            PaintPlacement(placementData);
        }

        private void MoveGrabbedItem(ItemPlacementData placementData)
        {
            RectTransform parent = inventoryContainerRect;

            if (_currentInventoryState == InventoryState.HoveringStash)
            {
                parent = stashContainerRect;
            }
            
            _currentGrabbedItem.SetGridPosition(_currentUsedGrid[_currentSelectionIndex.x, _currentSelectionIndex.y].AnchoredPosition, parent);
        }

        public ItemPlacementData TrySetGrabbedItemOnCurrentSelectedIndex()
        {
            if (!CurrentGrabbedItem)
            {
                return default(ItemPlacementData);
            }

            return TrySetItemAt(CurrentGrabbedItem, _currentSelectionIndex.x, _currentSelectionIndex.y);
        }
        
        public ItemPlacementData TrySetItemAt(InventoryItem inventoryItem, int x, int y)
        {
            var placementData = _currentUsedGrid.GetPlacementData(inventoryItem, x, y);
            if (!placementData.CanFit)
            {
                return placementData;
            }

            foreach (var index in placementData.availableIdexes)
            {
                _currentUsedGrid[index.x, index.y].ItemInside = inventoryItem;
            }

            inventoryItem.OverlapedSlots.Clear();
            foreach (var index in placementData.availableIdexes)
            {
                inventoryItem.OverlapedSlots.Add(_currentUsedGrid[index.x,index.y]);
            }
            
            return placementData;
        }

        public void TryGrabItem()
        {
            InventoryItem item = _currentUsedGrid[_currentSelectionIndex.x, _currentSelectionIndex.y].ItemInside;

            if (item == null) 
                return;
            
            ClearPlacementForItem(item);
            _currentGrabbedItem = item;
            PreviewGrabbedItemPlacement();
        }
        
        public void ClearPlacementForItem(InventoryItem inventoryItem)
        {
            foreach (var slot in inventoryItem.OverlapedSlots)
            {
                slot.ItemInside = null;
                slot.SetColor(Color.white);
            }
        }

        public void ClearPlacementColorForItem(InventoryItem inventoryItem)
        {
            foreach (var slot in inventoryItem.OverlapedSlots)
            {
                slot.SetColor(Color.white);
            }
        }
        
        public void RotateCurrentGrabbedItem()
        {
            if (!CurrentGrabbedItem)
            {
                return;
            }

            CurrentGrabbedItem.Rotate();
        }

        public void HandleStashInventorySwitch()
        {
            switch (_currentInventoryState)
            {
                case InventoryState.HoveringInventory:
                    _currentInventoryState = InventoryState.HoveringStash;
                    _currentUsedGrid = _stashGrid;
                    MoveSelectionIndex(Vector2Int.zero);
                    break;
                case InventoryState.HoveringStash:
                    _currentInventoryState = InventoryState.HoveringInventory;
                    _currentUsedGrid = _inventoryGrid;
                    MoveSelectionIndex(Vector2Int.zero);
                    break;
            }
        }
        
        private void PaintPlacement(ItemPlacementData placementData)
        {
            foreach (var availableIdex in placementData.availableIdexes)
            {
                _currentUsedGrid[availableIdex.x, availableIdex.y].SetColor(Color.green);
            }

            foreach (var occupiedIndex in placementData.occupiedIndexes)
            {
                InventorySlot slot = _currentUsedGrid[occupiedIndex.x, occupiedIndex.y]; 
                slot.SetColor(Color.red);
            }
        }

        private void SetupInventoryGrid()
        {
            _inventoryGrid = new InventoryGrid2D(inventorySettings.Width, inventorySettings.Height);
            
            float slotSize = inventorySettings.SlotSize;
            Vector2 slotSpacing = inventorySettings.SlotSpacing;
            Vector2 slotOffset = inventorySettings.SlotOffset;
            Vector2 containerPadding = inventorySettings.ContainerPadding;
            
            for (int y = 0; y < _inventoryGrid.Height; y++)
            {
                for (int x = 0; x < _inventoryGrid.Width; x++)
                {
                    var newSlot = GameObject.Instantiate(inventorySettings.InventorySlotPrefab);
                    RectTransform slotRectTransform = newSlot.GetComponent<RectTransform>();

                    // Calculate position based on grid coordinates, slot size, and spacing
                    float posX = slotOffset.x + (x * (slotSize + slotSpacing.x));
                    float posY = slotOffset.y + (y * (slotSize + slotSpacing.y));
                    Vector2 slotPosition = new Vector2(posX, -posY); // Negative y to account for Unity's UI coordinate system

                    // Set position within the container panel
                    slotRectTransform.SetParent(inventoryContainerRect);
                    slotRectTransform.localPosition = slotPosition;

                    // Assign to grid
                    newSlot.Coordinate = new Vector2(x, y);
                    _inventoryGrid[x, y] = newSlot;
                    _allInventorySlots.Add(newSlot);
                }
            }
            // Adjust container size to fit all slots with padding
            float containerWidth = _inventoryGrid.Width * (slotSize + slotSpacing.x) + containerPadding.x;
            float containerHeight = _inventoryGrid.Height * (slotSize + slotSpacing.y) + containerPadding.y;
            inventoryContainerRect.sizeDelta = new Vector2(containerWidth, containerHeight);
            
            selectionIndicator.SetAsLastSibling();
        }

        private void SetupStashGrid()
        {
            _stashGrid = new InventoryGrid2D(inventorySettings.StashWidth, inventorySettings.StashHeight);
            
            float slotSize = inventorySettings.SlotSize;
            Vector2 slotSpacing = inventorySettings.SlotSpacing;
            Vector2 slotOffset = inventorySettings.StashSlotOffset;
            Vector2 containerPadding = inventorySettings.StashContainerPadding;
            
            for (int y = 0; y < _stashGrid.Height; y++)
            {
                for (int x = 0; x < _stashGrid.Width; x++)
                {
                    var newSlot = GameObject.Instantiate(inventorySettings.InventorySlotPrefab, stashContainerRect);
                    RectTransform slotRectTransform = newSlot.GetComponent<RectTransform>();

                    // Calculate position based on grid coordinates, slot size, and spacing
                    float posX = slotOffset.x + (x * (slotSize + slotSpacing.x));
                    float posY = slotOffset.y + (y * (slotSize + slotSpacing.y));
                    Vector2 slotPosition = new Vector2(posX, -posY); // Negative y to account for Unity's UI coordinate system

                    // Set position within the container panel
                    slotRectTransform.SetParent(stashContainerRect);
                    slotRectTransform.localPosition = slotPosition;

                    // Assign to grid
                    newSlot.Coordinate = new Vector2(x, y);
                    _stashGrid[x, y] = newSlot;
                    _allStashSlots.Add(newSlot);
                }
            }
            // Adjust container size to fit all slots with padding
            float containerWidth = _stashGrid.Width * (slotSize + slotSpacing.x) + containerPadding.x;
            float containerHeight = _stashGrid.Height * (slotSize + slotSpacing.y) + containerPadding.y;
            stashContainerRect.sizeDelta = new Vector2(containerWidth, containerHeight);
        }

        [Button("Reset")]
        private void Reset()
        {
            if (!Application.isPlaying)
            {
                return;
            }
            Clear();
            SetupInventoryGrid();
            SetupStashGrid();
        }
        
        private void Clear()
        {
            foreach (var slot in _allInventorySlots)
            {
                Destroy(slot.gameObject);
            }

            foreach (var slot in _allStashSlots)
            {
                Destroy(slot.gameObject);
            }
            
            _allInventorySlots.Clear();
            _allStashSlots.Clear();
            _inventoryGrid.Clear();
            _inventoryGrid = null;

            _protoInventory.Clear();
        }
        
        private void OnScrapGrabbed(GameEvents.CharacterEvents.ScrapGrabbed data)
        {
            _protoInventory.Add(data.GrabbedScrapItem);
        }
        
        public List<ScrapItemData> GetAndClearAllScrap()
        {
            var scrap = new List<ScrapItemData>(_protoInventory); 
            _protoInventory.Clear();
            return scrap;
        }
    }
}