﻿namespace _Project.Scripts.Core.InventorySystem
{
    public class InventoryItemShape
    {
        public bool[,] Data { get; set; }

        public void SetData(ShapeSettings settings)
        {
            int size = settings.ShapeArray.GetLength(0); // Assuming a square array

            Data = new bool[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Data[i, j] = settings.ShapeArray[i, j];
                }
            }
        }
    }
}