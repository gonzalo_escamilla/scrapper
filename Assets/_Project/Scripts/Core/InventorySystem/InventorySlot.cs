﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.InventorySystem
{
    public class InventorySlot : MonoBehaviour
    {
        [SerializeField] private Image image;
        
        public Vector2 Coordinate { get; set; }
        public InventoryItem ItemInside { get; set; }

        public Vector2 AnchoredPosition => _rectTransform.anchoredPosition;

        RectTransform _rectTransform;
        
        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        /// <summary>
        /// Indicates if the slot is available for use. A slot is available if it is not forbidden,
        /// not occupied, not locked, and does not contain any item inside.
        /// </summary>
        public bool IsAvailable => !IsForbidden && !IsOccupied && !IsLocked;
    
        /// <summary>
        /// Indicates if the slot is part of the usable inventory grid. Forbidden slots are hidden and not usable.
        /// </summary>
        public bool IsForbidden { get; set; }

        /// <summary>
        /// Indicates if the slot currently contains an item.
        /// </summary>
        public bool IsOccupied => ItemInside != null;

        /// <summary>
        /// Indicates if the slot is shown but not usable at the moment.
        /// </summary>
        public bool IsLocked { get; set; }

        public void SetColor(Color color)
        {
            image.color = color;
        }
    }
}