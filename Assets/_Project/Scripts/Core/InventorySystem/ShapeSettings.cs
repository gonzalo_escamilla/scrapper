﻿using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

namespace _Project.Scripts.Core.InventorySystem
{
    [CreateAssetMenu(menuName = "Scrapper/Inventory/Create Inventory Shape Setting", fileName = "InventoryShapeSetting", order = 0)]
    public class ShapeSettings : ScriptableObject, ISerializationCallbackReceiver
    {
        // Property to get a copy of the shape array
        public bool[,] ShapeArray => GetCopyOfCustomCellDrawing();

        private const int ROW_HEIGHT = 60;
        private const int MAX_SIZE = GameConstants.MAX_ITEM_SIZE;

        [ShowInInspector, DoNotDrawAsReference]
        [TableMatrix(HorizontalTitle = "Custom Cell Drawing", DrawElementMethod = "DrawColoredEnumElement", ResizableColumns = false, RowHeight = ROW_HEIGHT)]
        private bool[,] CustomCellDrawing;

        [SerializeField] // Serialize the flattened array
        private bool[] flattenedCellDrawing;

        private void OnEnable()
        {
            // Convert the flattened array back to the 2D array on enable
            if (flattenedCellDrawing == null || flattenedCellDrawing.Length != MAX_SIZE * MAX_SIZE)
            {
                CreateData();
            }
            else
            {
                CustomCellDrawing = new bool[MAX_SIZE, MAX_SIZE];
                for (int i = 0; i < MAX_SIZE; i++)
                {
                    for (int j = 0; j < MAX_SIZE; j++)
                    {
                        CustomCellDrawing[i, j] = flattenedCellDrawing[i * MAX_SIZE + j];
                    }
                }
            }
        }

#if UNITY_EDITOR

        private static bool DrawColoredEnumElement(Rect rect, bool value)
        {
            if (Event.current.type == EventType.MouseDown && rect.Contains(Event.current.mousePosition))
            {
                value = !value;
                GUI.changed = true;
                Event.current.Use();
            }

            UnityEditor.EditorGUI.DrawRect(rect.Padding(1), value ? new Color(0.1f, 0.8f, 0.2f) : new Color(0, 0, 0, 0.5f));

            return value;
        }
#endif

        [Button("Reset")]
        private void Reset()
        {
            CreateData();
        }

        private void CreateData()
        {
            CustomCellDrawing = new bool[MAX_SIZE, MAX_SIZE];
            flattenedCellDrawing = new bool[MAX_SIZE * MAX_SIZE];
        }

        private bool[,] GetCopyOfCustomCellDrawing()
        {
            bool[,] copy = new bool[MAX_SIZE, MAX_SIZE];
            for (int i = 0; i < MAX_SIZE; i++)
            {
                for (int j = 0; j < MAX_SIZE; j++)
                {
                    copy[i, j] = CustomCellDrawing[i, j];
                }
            }
            return copy;
        }

        public void OnBeforeSerialize()
        {
            // Flatten the 2D array before serialization
            if (CustomCellDrawing != null)
            {
                flattenedCellDrawing = new bool[MAX_SIZE * MAX_SIZE];
                for (int i = 0; i < MAX_SIZE; i++)
                {
                    for (int j = 0; j < MAX_SIZE; j++)
                    {
                        flattenedCellDrawing[i * MAX_SIZE + j] = CustomCellDrawing[i, j];
                    }
                }
            }
        }

        public void OnAfterDeserialize()
        {
            // Ensure the 2D array is initialized after deserialization
            if (flattenedCellDrawing != null && flattenedCellDrawing.Length == MAX_SIZE * MAX_SIZE)
            {
                CustomCellDrawing = new bool[MAX_SIZE, MAX_SIZE];
                for (int i = 0; i < MAX_SIZE; i++)
                {
                    for (int j = 0; j < MAX_SIZE; j++)
                    {
                        CustomCellDrawing[i, j] = flattenedCellDrawing[i * MAX_SIZE + j];
                    }
                }
            }
        }
    }
}
