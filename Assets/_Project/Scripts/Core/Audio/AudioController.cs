using UnityEngine;

using Enderlook.EventManager;

using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.ScavengeSystem.Items;

public class AudioController : MonoBehaviour
{
    private float healthThreshold = 50;
    private string nameEvent;
    private bool bDone = false;
    public AK.Wwise.RTPC ScrapRarityRTPC;
    private float scrapedRarity = 0;
    private float rarityMusicStart = 0f;
    private float rarityMusicColdown = 0f;
    private float scrappingCommentStart = 0f;
    private float scrappingCommentColdown = 0f;
    
    private void OnEnable()
    {
        EventManager.Shared.Subscribe<GameEvents.CharacterEvents.HealthChanged>(OnHealthChanged);        
        EventManager.Shared.Subscribe<GameEvents.CharacterEvents.HealthLost>(OnHealthLost);
        EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterDied>(OnCharacterDied);
        EventManager.Shared.Subscribe<GameEvents.CharacterEvents.ScrapGrabbed>(OnScrapGrabbed);
        EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
        EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);
    }
    
    private void OnDisable()
    {
        EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.HealthChanged>(OnHealthChanged);        
        EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.HealthLost>(OnHealthLost);
        EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterDied>(OnCharacterDied);
        EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.ScrapGrabbed>(OnScrapGrabbed);
        EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
        EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterLeavedExitZone>(OnCharacterLeavedExitZone);
    }
    
    private void OnHealthChanged(GameEvents.CharacterEvents.HealthChanged data)
    {
        // Debug.Log($"OnHealthLost {data.CurrentHealth}");
    }
    
    private void OnHealthLost(GameEvents.CharacterEvents.HealthLost data)
    {
        if (data.CurrentHealth <= healthThreshold)
        {
            //Debug.Log($"LowOnTime SoundWarning");
            if (!bDone) 
            { 
                nameEvent = "Low_On_Time_Dialog";
                AkSoundEngine.PostEvent(nameEvent, GameObject.Find("WwiseGlobal"));
                    bDone = true;
            }
        }
        else {bDone = false;}
    }

    private void OnCharacterDied(GameEvents.CharacterEvents.CharacterDied data)
    {
        nameEvent = "Death";
        AkSoundEngine.PostEvent(nameEvent, GameObject.Find("WwiseGlobal"));
    }
    private void OnScrapGrabbed(GameEvents.CharacterEvents.ScrapGrabbed data)
    {
        scrapedRarity = (int)data.GrabbedScrapItem.Rarity;
        nameEvent = "Scrapped";
        ScrapRarityRTPC.SetGlobalValue(scrapedRarity);
        AkSoundEngine.PostEvent(nameEvent, GameObject.Find("WwiseGlobal"));
        
        if ((ScrapRarity)scrapedRarity == ScrapRarity.Legendary && Time.time > rarityMusicStart + rarityMusicColdown)
        {
            rarityMusicColdown = 300f;
            rarityMusicStart = Time.time;
            nameEvent = "Play_XXLScrap";
            AkSoundEngine.PostEvent(nameEvent, GameObject.Find("WwiseGlobal"));
        }
                
        if (Time.time > scrappingCommentStart + scrappingCommentColdown)
        {
            scrappingCommentColdown = 60f;
            scrappingCommentStart = Time.time;
            nameEvent = "ScrappedComment";
            AkSoundEngine.PostEvent(nameEvent, GameObject.Find("WwiseGlobal"));
        }
    }
    private void OnCharacterEnteredExitZone(GameEvents.CharacterEvents.CharacterEnteredExitZone data)
    {
        nameEvent = "EnteredExitZone";
        AkSoundEngine.PostEvent(nameEvent, GameObject.Find("WwiseGlobal"));
    }
    private void OnCharacterLeavedExitZone(GameEvents.CharacterEvents.CharacterLeavedExitZone data)
    {
        nameEvent = "LeavedExitZone";
        AkSoundEngine.PostEvent(nameEvent, GameObject.Find("WwiseGlobal"));
    }
}

