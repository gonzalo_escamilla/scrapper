using UnityEngine;

public class CopyPositionWithOffset : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float heightOffset = 1000f;
    private Transform camera;

    public void Start()
    {
        camera = Camera.main.transform;
    }
    private void Update()
    {
        if (target != null)
        {
            transform.position = new Vector3(target.position.x, target.position.y + heightOffset, target.position.z);
            transform.rotation = camera.rotation;
        }
    }
}