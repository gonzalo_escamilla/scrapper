﻿namespace _Project.Scripts.Core.NPCs
{
    public interface IState
    {
        bool HasExitTime { get; set; }
        bool IsDone { get; }
        bool CanTransitionToSelf { get; set; }
        
        void OnEnter();
        void OnExit();
        void Tick();
    }
}