﻿using System;
using System.Collections.Generic;

namespace _Project.Scripts.Core.NPCs
{
    public class StateMachine : IDisposable
    {
        public IState LastState => _lastState;
        public IState CurrentState => _currentState;
        private IState _lastState;
        private IState _currentState;
        private Dictionary<Type, List<Transition>> _transitions = new();
        private List<Transition> _currentTransitions = new();
        private List<Transition> _anyTransitions = new();
        private static readonly List<Transition> EmptyTransitions = new();

        private bool _disposed;

        public void Tick()
        {
            if (_disposed) throw new ObjectDisposedException(nameof(StateMachine));

            var transition = GetTransition();
            if (transition != null)
            {
                if (!_currentState.HasExitTime)
                {
                    SetState(transition.To);
                }
                else if (_currentState.IsDone)
                {
                    SetState(transition.To);
                }
            }

            _currentState?.Tick();
        }

        public void SetState(IState state)
        {
            if (_disposed) throw new ObjectDisposedException(nameof(StateMachine));

            _lastState = _currentState;
            if (state == _currentState && !state.CanTransitionToSelf)
            {
                return;
            }

            _currentState?.OnExit();
            _currentState = state;

            _transitions.TryGetValue(_currentState.GetType(), out _currentTransitions);
            _currentTransitions ??= EmptyTransitions;

            _currentState.OnEnter();
        }

        public void AddTransition(IState from, IState to, Func<bool> predicate)
        {
            if (_disposed) throw new ObjectDisposedException(nameof(StateMachine));

            if (_transitions.TryGetValue(from.GetType(), out var transitions) == false)
            {
                transitions = new List<Transition>();
                _transitions[from.GetType()] = transitions;
            }
            transitions.Add(new Transition(to, predicate));
        }

        public void AddAnyTransition(IState state, Func<bool> predicate)
        {
            if (_disposed) throw new ObjectDisposedException(nameof(StateMachine));

            _anyTransitions.Add(new Transition(state, predicate));
        }

        private class Transition
        {
            public Func<bool> Condition { get; }
            public IState To { get; }

            public Transition(IState to, Func<bool> condition)
            {
                To = to;
                Condition = condition;
            }
        }

        private Transition GetTransition()
        {
            foreach (var transition in _anyTransitions)
                if (transition.Condition())
                    return transition;

            foreach (var transition in _currentTransitions)
                if (transition.Condition())
                    return transition;

            return null;
        }

        public void Dispose()
        {
            if (_disposed) return;

            // Dispose current state if it implements IDisposable
            if (_currentState is IDisposable disposableState)
            {
                disposableState.Dispose();
            }

            // Clear collections to release references
            _transitions.Clear();
            _currentTransitions.Clear();
            _anyTransitions.Clear();

            // Mark as disposed
            _disposed = true;

            // Suppress finalization
            GC.SuppressFinalize(this);
        }
    }
}