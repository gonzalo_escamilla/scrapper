﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Project.Scripts.Core
{
    public interface ISceneLoader
    {
        UniTask LoadSceneAsyncAdditive(int sceneIndex);
        UniTask LoadSceneAsync(int sceneIndex);
        UniTask UnloadAllScenes();
        bool IsThereAnyLoadedPlayableScene();
    }
    
    public class SceneLoader: ISceneLoader
    {
        private List<int> _loadedScenes;

        public SceneLoader()
        {
            _loadedScenes = new List<int>();
        }
        
        public async UniTask LoadSceneAsyncAdditive(int sceneIndex)
        {
            bool isLoaded = IsSceneLoaded(sceneIndex);
            if (isLoaded)
            {
                Debug.LogWarning($"Scene {sceneIndex} is already loaded..");
                return;
            }
            await SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Additive);
            _loadedScenes.Add(sceneIndex);
        }
        
        public async UniTask LoadSceneAsync(int sceneIndex)
        {
            bool isLoaded = IsSceneLoaded(sceneIndex);
            if (isLoaded)
            {
                Debug.LogWarning($"Scene {sceneIndex} is already loaded..");
                return;
            }
            
            await SceneManager.LoadSceneAsync(sceneIndex, LoadSceneMode.Single);
            _loadedScenes.Add(sceneIndex);
        }

        public async UniTask UnloadAllScenes()
        {
            foreach (var scene in _loadedScenes)
            {
                await SceneManager.UnloadSceneAsync(scene);
            }
        }

        public bool IsThereAnyLoadedPlayableScene()
        {
            return SceneManager.sceneCount > 2;
        }

        private bool IsSceneLoaded(int sceneIndex)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (scene.buildIndex == sceneIndex)
                {
                    if (!_loadedScenes.Contains(sceneIndex))
                        _loadedScenes.Add(sceneIndex);
                    
                    return true;
                }
            }
            
            return false;
        }
    }
}