﻿#if UNITY_EDITOR

using _Project.Scripts.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
public static class DeveloperTools
{
    private const string PlayerInitialPositionTag = "PlayerInitialPosition";
    
    static DeveloperTools()
    {
        EditorSceneManager.sceneOpened += OnSceneOpened;
    }

    public static ScrapperController GetCurrentCharacter()
    {
        return Object.FindObjectOfType<ScrapperController>();
    }
    
    private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
    {
        var character = GetCurrentCharacter();
        var initialPositionPivot = GameObject.FindGameObjectWithTag(PlayerInitialPositionTag);

        if (!character || !initialPositionPivot)
        {
            return;
        }
        character.transform.SetPositionAndRotation(initialPositionPivot.transform.position, Quaternion.identity);
    }
}

#endif
