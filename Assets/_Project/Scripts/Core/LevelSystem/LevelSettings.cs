﻿using System.Collections.Generic;
using _Project.Scripts.Core.ScavengeSystem;
using _Project.Scripts.Core.ScavengeSystem.Items;
using UnityEngine;

[CreateAssetMenu(menuName = "Scrapper/Level/Create Level Settings", fileName = "LevelSettings", order = 0)]
public class LevelSettings : ScriptableObject
{
    [Range(0,1)]
    [SerializeField] 
    private float percentageOfScrapToSpawn;

    [SerializeField]
    private List<HealthRecoveryByRarity> healthRecoveryConfigurations;
    
    [SerializeField]
    private bool restartLevelOnExchange;
    
    public float PercentageOfScrapToSpawn => percentageOfScrapToSpawn;
    public bool RestartLevelOnExchange => restartLevelOnExchange;

    public int GetHealthFor(ScrapRarity rarity)
    {
        foreach (var config in healthRecoveryConfigurations)
        {
            if (config.Rarity == rarity)
                return config.HealthToRecover;
        }
        return 0; // Default if no match
    }

    public int GetAmountToSpawn(int spawnersAmount)
    {
        return Mathf.CeilToInt(spawnersAmount * percentageOfScrapToSpawn);
    }

    public float GetAllHealthFrom(List<ScrapItemData> allScrapGathered)
    {
        float totalHealthToRecover = 0;
        foreach (var scrapData in allScrapGathered)
        {
            totalHealthToRecover += GetHealthFor(scrapData.Rarity);
        }

        return totalHealthToRecover;
    }
}