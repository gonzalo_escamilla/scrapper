﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.GameStates;
using _Project.Scripts.Core.ScavengeSystem;
using _Project.Scripts.Core.ScavengeSystem.ScrapGeneration;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public interface ILevelControllerProvider
{
    LevelController LevelControllerInstance { get; }
}

public class LevelController : MonoBehaviour, ILevelControllerProvider
{
    [Title("References")] 
    [SerializeField] private LevelSettings settings;
    [SerializeField] private GameObject mockScrapPrefab; // TODO: Just for the proto
    [SerializeField] private ScrapSpawner scrapSpawner;
    [SerializeField] private AstarPath astarPath;
    
    [SerializeField] private List<GameObject> _mockGameplayContainers;
    [SerializeField] private List<ExitZone> exitZones;
    [SerializeField] private GameObject debugBuildingToSpawn; //TODO: Remove afeter
    
    public LevelController LevelControllerInstance => this;
    
    private ExitZone _lastExitZone;
    private GameObject _currentMockGameplayContainer;

    private List<Transform> _lastUsedSpawners; // TODO: Just for the proto
    private List<GameObject> _currentSpawnedScrap; // TODO: Just for the proto

    private bool _isFreshStart;
    
    private void Awake()
    {
        Services.Add<ILevelControllerProvider>(this);
        Services.Add<LevelController>(this);

        _lastUsedSpawners = new List<Transform>();
        _currentSpawnedScrap = new List<GameObject>();
    }

    private void OnEnable()
    {
        EventManager.Shared.Subscribe<GameEvents.GameStateEvents.GameStateChanged>(OnGameStateChanged);
        EventManager.Shared.Subscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
    }

    private void OnDisable()
    {
        EventManager.Shared.Unsubscribe<GameEvents.GameStateEvents.GameStateChanged>(OnGameStateChanged);
        EventManager.Shared.Unsubscribe<GameEvents.CharacterEvents.CharacterEnteredExitZone>(OnCharacterEnteredExitZone);
    }
    
    private void OnDestroy()
    {
        Services.Remove(this);
    }

    public void Setup(int index = -1)
    {
        if (!_isFreshStart && !settings.RestartLevelOnExchange && index == -1)
        {
            return;
        }
        
        Reset();

        if (index == -1)
        {
            _currentMockGameplayContainer = GetRandomContainer();
        }
        else
        {
            _currentMockGameplayContainer = _mockGameplayContainers[index];
        }
        
        _currentMockGameplayContainer.gameObject.SetActive(true);
        TurnOffAllContainersButCurrentOne();
        SpawnScrap();
        astarPath.Scan();
    }

    public void ForceSetupGameplayContainer(int index)
    {
        Setup(index);
    }
    
    public ExitZone GetRandomExitZone() 
    {
        if (exitZones == null || exitZones.Count < 2) 
        {
            Debug.LogWarning("Not enough exit zones to avoid repeating the last one.");
            if (exitZones != null) _lastExitZone = exitZones[0];
            return _lastExitZone; // Return the last one if no alternative is possible
        }

        ExitZone newExitZone;
        do 
        {
            newExitZone = exitZones[Random.Range(0, exitZones.Count)];
        } 
        while (newExitZone == _lastExitZone);

        _lastExitZone = newExitZone;
        return newExitZone;
    }
    
    // TODO: Just for the proto
    private void Reset()
    {
        foreach (var scrap in _currentSpawnedScrap)
        {
            scrap.GetComponent<ScrapLevelObject>().Release();
            // Destroy(scrap);
        }
        _currentSpawnedScrap.Clear();
        _lastUsedSpawners.Clear();
    }

    private void SpawnScrap()
    {
        List<Transform> targetSpawners = _currentMockGameplayContainer.GetComponentsInChildren<Transform>()
            .Where(t => t.CompareTag("TargetSpawner_Scrap")).ToList();

        _lastUsedSpawners = targetSpawners.ToList();
    
        // Get the amount of scrap to spawn based on settings
        int amountToSpawn = settings.GetAmountToSpawn(targetSpawners.Count);
    
        _lastUsedSpawners = targetSpawners.OrderBy(t => Guid.NewGuid()).ToList();

        foreach (var spawner in _lastUsedSpawners)
        {
            // spawner.gameObject.SetActive(false);
            spawner.GetChild(0).gameObject.SetActive(false);
        }
        
        for (int i = 0; i < amountToSpawn; i++)
        {
            var spawner = _lastUsedSpawners[i];
            var newScrap = scrapSpawner.GetScrap();
            newScrap.transform.SetPositionAndRotation(spawner.position, spawner.rotation);
            newScrap.transform.SetParent(transform);
            newScrap.Setup();
            
            _currentSpawnedScrap.Add(newScrap.gameObject);
            spawner.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void TurnOffAllContainersButCurrentOne()
    {
        foreach (var container in _mockGameplayContainers)
        {
            if (container != _currentMockGameplayContainer)
            {
                container.SetActive(false);
            }
        }
    }

    private GameObject GetRandomContainer()
    {
        int randomIndex;
        do
        {
            randomIndex = UnityEngine.Random.Range(0, _mockGameplayContainers.Count);
        } 
        while (_currentMockGameplayContainer != null && _mockGameplayContainers[randomIndex] == _currentMockGameplayContainer);

        return _mockGameplayContainers[randomIndex];
    }

    public Vector3 GetSpawnPoint()
    {
        return _lastExitZone.Center;
    }
    
    private void OnGameStateChanged(GameEvents.GameStateEvents.GameStateChanged data)
    {
        if (data.CurrentState is StartRunForTheFirstTime)
        {
            _isFreshStart = true;
        }
        
        if (data.CurrentState is RestartRun)
        {
            _isFreshStart = false;
        }
    }
    private void OnCharacterEnteredExitZone(GameEvents.CharacterEvents.CharacterEnteredExitZone context)
    {
        _lastExitZone = context.Zone;
    }

    public void SpawnBuilding(string id)
    {
        debugBuildingToSpawn.SetActive(true);
    }
}