using System;
using _Project.Scripts.Core;
using UnityEngine;
using Object = UnityEngine.Object;

public class TownDirectionHelper : MonoBehaviour
{
    [SerializeField] private Transform movingObject;
    [SerializeField] private float heightOffset;
    [SerializeField] private float radius;
    [SerializeField] private float damping;
    [SerializeField] private float speed;
    
    private Transform _scrapperTransform;
    private Transform _exitZoneTransform;
    
    private void Awake()
    {
        _scrapperTransform = Object.FindObjectOfType<ScrapperController>().transform;
        _exitZoneTransform = Object.FindObjectOfType<ExitZone>().transform;
    }

    private void Update()
    {
        SetDirection();
    }

    private void SetDirection()
    {
        if (_scrapperTransform == null || _exitZoneTransform == null)
        {
            Debug.LogWarning("Missing Scrapper or ExitZone reference");
            return;
        }

        // Get the vector pointing from the scrapper to the exit zone
        Vector3 directionToExit = (_exitZoneTransform.position - _scrapperTransform.position).normalized;

        // Calculate the point on the circle around the scrapper closest to the exit zone
        Vector3 closestPointOnCircle = _scrapperTransform.position + directionToExit * radius;

        // Apply the height offset
        closestPointOnCircle.y = _scrapperTransform.position.y + heightOffset;

        // Smoothly move the moving object to the new position (with damping)
        movingObject.position = Vector3.Lerp(movingObject.position, closestPointOnCircle, Time.deltaTime * damping);

        // Make the movingObject face the exit zone
        movingObject.rotation = Quaternion.LookRotation(directionToExit);
    }
}
