﻿using UnityEngine;
using UnityEngine.Pool;

namespace _Project.Scripts.Core.ScavengeSystem.ScrapGeneration
{
    public class ScrapSpawner : MonoBehaviour
    {
        [SerializeField] private ScrapDataGenerator scrapDataGenerator;
        [SerializeField] private ScrapLevelObject mockScrapPrefab; // TODO: Just for the proto
        [SerializeField] private Vector3 defaultSpawnPoint;
            
        
        private ObjectPool<ScrapLevelObject> _scrapPool;


        private void Awake()
        {
            _scrapPool = new ObjectPool<ScrapLevelObject>(CreateScrap, OnGetScrap, OnReleaseScrap, OnDestroyScrap, false,
                100, 10000);
        }
    
        public ScrapLevelObject GetScrap()
        {
            ScrapLevelObject newScrap = _scrapPool.Get();
            newScrap.debrisData = scrapDataGenerator.GetRandomDebrisData();
            return newScrap;
        }
    
        private ScrapLevelObject CreateScrap()
        {
            var newScrap = Instantiate(mockScrapPrefab, defaultSpawnPoint, Quaternion.identity, parent: transform);
            newScrap.gameObject.SetActive(false);
            newScrap.Released += OnLevelScrapRelease;
            return newScrap;
        }

        private void OnLevelScrapRelease(ScrapLevelObject levelObject)
        {
            _scrapPool.Release(levelObject);
        }

        private void OnGetScrap(ScrapLevelObject instance)
        {
            instance.gameObject.SetActive(true);
        }
    
        private void OnReleaseScrap(ScrapLevelObject instance)
        {
            instance.gameObject.SetActive(false);
        }
    
        private void OnDestroyScrap(ScrapLevelObject instance)
        {
            Destroy(instance.gameObject);
        }
    }
}
