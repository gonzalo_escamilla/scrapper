﻿using UnityEngine;

namespace _Project.Scripts.Core.ScavengeSystem.ScrapGeneration
{
    [CreateAssetMenu(menuName = "Scrapper/Scavenge/Create Generator Settings", fileName = "GeneratorSettings", order = 0)]
    public class ScrapDataGeneratorSettings : ScriptableObject
    {
        [Tooltip("Minimum number of containers that can be generated.")]
        [SerializeField] public int minContainers = 0;

        [Tooltip("Maximum number of containers that can be generated.")]
        [SerializeField] public int maxContainers = 4;

        [Tooltip("Minimum number of loose items that can be generated.")]
        [SerializeField] public int minItems = 0;

        [Tooltip("Maximum number of loose items that can be generated.")]
        [SerializeField] public int maxItems = 4;

        [Tooltip("Weight of generating zero containers (higher values increase the chance).")]
        [SerializeField] public float zeroContainerWeight = 0.5f;

        [Tooltip("Weight of generating non-zero containers (shared between 1 to maxContainers).")]
        [SerializeField] public float nonZeroContainerWeight = 0.5f;

        [Tooltip("Weight of generating zero items (higher values increase the chance).")]
        [SerializeField] public float zeroItemWeight = 0.5f;

        [Tooltip("Weight of generating non-zero items (shared between 1 to maxItems).")]
        [SerializeField] public float nonZeroItemWeight = 0.5f;

        [Tooltip("Maximum depth for nesting containers. Prevents infinite nesting.")]
        [SerializeField] public int maxDepth = 3; 
        
        public int MinContainers => minContainers;
        public int MaxContainers => maxContainers;
        public int MinItems => minItems;
        public int MaxItems => maxItems;
        
        public float ZeroContainerWeight => zeroContainerWeight;
        public float NonZeroContainerWeight => nonZeroContainerWeight;
        public float ZeroItemWeight => zeroItemWeight;
        public float NonZeroItemWeight => nonZeroItemWeight;
        
        public int MaxDepth => maxDepth;
    }
}