﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Core.ScavengeSystem.Containers;
using _Project.Scripts.Core.ScavengeSystem.Items;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Core.ScavengeSystem.ScrapGeneration
{
    public class ScrapDataGenerator : MonoBehaviour
    {
        [SerializeField] private ScrapDataGeneratorSettings settings;
        [SerializeField] private ScavengeSettings scavengeSettings;
        
        [Button("Test")]
        public void Test()
        {
            var randomData = GetRandomDebrisData();
            Debug.LogWarning("Data");
        }
        
        public DebrisData GetRandomDebrisData()
        {
            List<ScrapData> scrap = GetRandomScrap(0);
            return new DebrisData(scrap);
        }

        private List<ScrapData> GetRandomScrap(int currentDepth)
        {
            List<ScrapData> scrapDataList = new List<ScrapData>();

            if (currentDepth >= settings.MaxDepth)
                return scrapDataList; // Prevent deeper nesting

            // Generate a random number of containers
            int numContainers = GetWeightedRandom(settings.MinContainers, settings.MaxContainers, settings.ZeroContainerWeight, settings.NonZeroContainerWeight);
            for (int i = 0; i < numContainers; i++)
            {
                List<ScrapData> nestedScrap = GetRandomScrap(currentDepth + 1); // Recursive call to nest containers
                scrapDataList.Add(new ScrapContainerData(nestedScrap));
            }

            // Generate a random number of loose items
            int numItems = GetWeightedRandom(settings.MinItems, settings.MaxItems, settings.ZeroItemWeight, settings.NonZeroItemWeight);
            for (int i = 0; i < numItems; i++)
            {
                var rarity = GetRandomRarity();
                var pickUpTime = scavengeSettings.GetPickUpTimeFor(rarity);
                scrapDataList.Add(new ScrapItemData(rarity, pickUpTime));
            }

            return scrapDataList;
        }

        // Helper method for generating weighted random numbers
        private int GetWeightedRandom(int min, int max, float zeroWeight, float nonZeroWeight)
        {
            // Create an array of weights corresponding to the range from min to max
            float[] weights = new float[max - min + 1];
            weights[0] = zeroWeight; // 0 has greater weight
            for (int i = 1; i < weights.Length; i++)
            {
                weights[i] = nonZeroWeight; // Non-zero options share the remaining weight
            }

            // Get the total weight
            float totalWeight = 0f;
            foreach (var weight in weights)
            {
                totalWeight += weight;
            }

            // Generate a random value between 0 and totalWeight
            float randomValue = Random.Range(0, totalWeight);
            
            // Determine which range the random value falls into
            float cumulativeWeight = 0f;
            for (int i = 0; i < weights.Length; i++)
            {
                cumulativeWeight += weights[i];
                if (randomValue < cumulativeWeight)
                {
                    return min + i;
                }
            }

            return min; // Fallback, should never happen
        }

        private ScrapRarity GetRandomRarity()
        {
            return (ScrapRarity)Random.Range(0, System.Enum.GetValues(typeof(ScrapRarity)).Length);
        }
    }
}