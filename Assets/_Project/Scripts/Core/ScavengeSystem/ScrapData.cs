﻿using System;
using UnityEngine;

namespace _Project.Scripts.Core.ScavengeSystem
{
    [Serializable]
    public abstract class ScrapData
    {
        public Action<ScrapData> ScavengeCompleted;
        public float CompletionPercentage => _scavengeProgress / _scavengeCompletionThreshold;
        public bool AlreadyScavenged => _scavenged;
        
        protected float _scavengeProgress;
        protected float _scavengeCompletionThreshold;
        protected bool _scavenged;
        
        public virtual void Scavenge()
        {
            if (_scavenged)
            {
                return;
            }

            _scavengeProgress += Time.deltaTime;
            // Debug.LogWarning($"Progres: {_scavengeProgress}");

            if (_scavengeProgress >= _scavengeCompletionThreshold)
            {
                // Debug.LogWarning($"Completed {_scavengeCompletionThreshold}");

                _scavengeProgress = _scavengeCompletionThreshold;
                _scavenged = true;
                ScavengeCompleted?.Invoke(this);
            }
        }
    }
}