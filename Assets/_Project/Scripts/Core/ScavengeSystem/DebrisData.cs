﻿using System;
using System.Collections.Generic;

namespace _Project.Scripts.Core.ScavengeSystem
{
    [System.Serializable]
    public class DebrisData
    {
        public List<ScrapData> AllScrap { get; private set; }

        public event Action<ScrapData> ScrapScavenged;
        
        public DebrisData(List<ScrapData> allScrap)
        {
            AllScrap = allScrap;

            foreach (var scrap in allScrap)
            {
                scrap.ScavengeCompleted += OnScavengeCompleted;
            }
        }
        
        private void OnScavengeCompleted(ScrapData scrapData)
        {
            scrapData.ScavengeCompleted = null;
            ScrapScavenged?.Invoke(scrapData);
        }

        public void AddNewScrap(List<ScrapData> containerScrap)
        {
            foreach (var scrapData in containerScrap)
            {
                AllScrap.Add(scrapData);
                scrapData.ScavengeCompleted += OnScavengeCompleted;
            }
        }
    }
}