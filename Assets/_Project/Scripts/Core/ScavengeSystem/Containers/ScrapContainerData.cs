﻿using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts.Core.ScavengeSystem.Containers
{
    [System.Serializable]
    public class ScrapContainerData : ScrapData
    {
        public bool IsEmpty => ScrapInside == null || ScrapInside.Count == 0;
        public bool IsOpen { get; private set; }

        public List<ScrapData> ScrapInside { get; private set; }
        
        public ScrapContainerData(List<ScrapData> scrap)
        {
            ScrapInside = scrap;
            _scavengeCompletionThreshold = GetRandomCompletionThreshold();
        }

        private float GetRandomCompletionThreshold()
        {
            if (IsEmpty)
            {
                return Random.Range(1f, 3f);
            }
            
            float pickUpTime = 2;
            return pickUpTime;
        }

        public List<ScrapData> Open()
        {
            if (IsOpen)
                return null;

            IsOpen = true;
            List<ScrapData> insideScrap = new List<ScrapData>();

            foreach (var scrapData in ScrapInside)
            {
                insideScrap.Add(scrapData);
            }

            ScrapInside = null;
            return insideScrap;
        }
    }
}