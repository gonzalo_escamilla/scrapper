﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.ScavengeSystem.Containers
{
    public class ScrapContainerView : UILayout
    {
        [SerializeField] private TextMeshProUGUI percentageLabel;
        [SerializeField] private Image image;
        [SerializeField] private Color defaultColor;
        [SerializeField] private Color emptyColor;
        
        private ScrapContainerData _data;
        
        public void SetData(ScrapContainerData data)
        {
            _data = data;
            image.color = defaultColor;
            UpdateLabel();
        }

        public override void Refresh()
        {
            UpdateLabel();
            if (_data.IsOpen)
            {
                UpdateColor();
            }
        }

        private void UpdateColor()
        {
            if (_data.IsEmpty)
            {
                image.color = emptyColor;
            }
        }

        private void UpdateLabel()
        {
            percentageLabel.text = $"{_data.CompletionPercentage:P}";
        }
    }
}