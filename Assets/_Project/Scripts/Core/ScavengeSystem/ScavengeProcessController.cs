﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.EventsSystem;
using _Project.Scripts.Core.ScavengeSystem.Containers;
using _Project.Scripts.Core.ScavengeSystem.Items;
using _Project.Scripts.GameServices;
using Enderlook.EventManager;
using UnityEngine;

namespace _Project.Scripts.Core.ScavengeSystem
{
    public class ScavengeProcessController : MonoBehaviour
    {
        private DebrisData _data;
        private IInputProvider _inputProvider;
        private ScavengeMenuView _scavengeMenu;
        
        public Action ScavengeCompleted;
        public Action ScavengeInterrupted;
        public Action UserExitScavenge;

        private int _currentSelectedIndex;
        private int _currentLoseScrap => _data.AllScrap.Count;
        private bool _isInteracting;
        
        public void SetData(DebrisData debrisData)
        {
            _data = debrisData;
            _inputProvider ??= Services.Get<IInputProvider>();
            _currentSelectedIndex = 0;
            
            _scavengeMenu ??= Services.Get<IMenuInstanceProvider>().GetMenuInstance<ScavengeMenuView>();
            
            _scavengeMenu.SetData(_data);
            _scavengeMenu.Show();
            _scavengeMenu.MoveSelectionTo(_currentSelectedIndex);
            _scavengeMenu.Refresh();
            
            _data.ScrapScavenged += OnScrapScavenged;
            RegisterInputs();
        }

        public void Release()
        {
            _scavengeMenu.Release();
            _scavengeMenu.Hide();
            _scavengeMenu.MoveSelectionTo(0);
        }
        
        private void Update()
        {
            if (_isInteracting)
            {
                TryInteractWithScrap();
            }
        }
        
        private void Exit()
        {
            _data.ScrapScavenged -= OnScrapScavenged;

            UnregisterInputs();
            
            _scavengeMenu.Hide();
            _scavengeMenu.Dispose();
            UserExitScavenge?.Invoke();
        }

        private void OnScrapScavenged(ScrapData data)
        {
            if (data is ScrapItemData itemData)
            {
                _data.AllScrap.Remove(data);
                EventManager.Shared.Raise(new GameEvents.CharacterEvents.ScrapGrabbed(itemData));
            }

            if (data is ScrapContainerData containerData)
            {
                List<ScrapData> containerScrap = containerData.Open();
                _data.AddNewScrap(containerScrap);
            }

            _scavengeMenu.LockSelector();
            _scavengeMenu.PopulateUI();
            _currentSelectedIndex = 0;
            _scavengeMenu.MoveSelectionTo(_currentSelectedIndex);
            _scavengeMenu.Refresh();
        }
        
        private void RegisterInputs()
        {
            _inputProvider.UseUtilityStarted += Exit;
            _inputProvider.MoveInputStarted += TryMoveIndex;
            _inputProvider.ActionPerformed += OnActionPerformed;
            _inputProvider.ActionCanceled += OnActionCanceled;
        }
        
        private void UnregisterInputs()
        {
            _inputProvider.UseUtilityStarted -= Exit;
            _inputProvider.MoveInputStarted -= TryMoveIndex;
            _inputProvider.ActionPerformed -= OnActionPerformed;
            _inputProvider.ActionCanceled -= OnActionCanceled;
        }
        
        private void OnActionPerformed()
        {
            _isInteracting = true;
        }

        private void OnActionCanceled()
        {
            _isInteracting = false;
        }
        
        private void TryInteractWithScrap()
        {
            if (_data.AllScrap.Count == 0)
            {
                Debug.LogWarning("No more data to interact with");
                return;
            }
            
            FitSelectedIndex();
            var scrapData = _data.AllScrap[_currentSelectedIndex];

            if (scrapData == null)
            {
                Debug.LogError($"ScrapData is null.");
                return;
            }

            if (scrapData.AlreadyScavenged)
                return;
            
            scrapData.Scavenge();
            _scavengeMenu.Refresh();
        }
        
        private void TryMoveIndex(Vector2 direction)
        {
            if (direction.x > 0)
            {
                UpdateSelectedIndex(1);
            }
            else if (direction.x < 0)
            {
                UpdateSelectedIndex(-1);
            }

            _scavengeMenu.MoveSelectionTo(_currentSelectedIndex);
        }
        private void UpdateSelectedIndex(int step)
        {
            if (_currentLoseScrap == 0)
            {
                _currentSelectedIndex = 0;
                // TODO: how to handle this=?
                return;
            }
            
            _currentSelectedIndex = (_currentSelectedIndex + step) % _currentLoseScrap;

            if (_currentSelectedIndex < 0)
            {
                _currentSelectedIndex += _currentLoseScrap;
            }
        }

        private void FitSelectedIndex()
        {
            if (_currentLoseScrap == 0)
            {
                _currentSelectedIndex = 0;
                // TODO: how to handle this=?
                return;
            }
            
            _currentSelectedIndex %= _currentLoseScrap;
        }
    }
}