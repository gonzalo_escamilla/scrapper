﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Core.ScavengeSystem.Items;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Project.Scripts.Core.ScavengeSystem
{
    [CreateAssetMenu(menuName = "Scrapper/Scavenge/Create Scavenge Settings", fileName = "ScavengeSettings", order = 0)]
    public class ScavengeSettings : ScriptableObject
    {
        [SerializeField] [MinMaxSlider(0,1)] private Vector2 itemPickUpTimeMultiplierRange;
        
        [SerializeField]
        private List<HealthRecoveryByRarity> healthRecoveryConfigurations;
    
        public int GetHealthFor(ScrapRarity rarity)
        {
            foreach (var config in healthRecoveryConfigurations)
            {
                if (config.Rarity == rarity)
                    return config.HealthToRecover;
            }
            return 0; // Default if no match
        }
    
    
        public float GetAllHealthFrom(List<ScrapItemData> allScrapGathered)
        {
            float totalHealthToRecover = 0;
            foreach (var scrapData in allScrapGathered)
            {
                totalHealthToRecover += GetHealthFor(scrapData.Rarity);
            }

            return totalHealthToRecover;
        }

        public float GetPickUpTimeFor(ScrapRarity rarity)
        {
            float randomMultiplier = Random.Range(itemPickUpTimeMultiplierRange.x, itemPickUpTimeMultiplierRange.y);
            int healthValue = GetHealthFor(rarity);
            return randomMultiplier * healthValue;
        }
    }
}