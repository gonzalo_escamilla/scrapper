using System;
using _Project.Scripts.Core;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.ScavengeSystem
{
    public enum ScrapState
    {
        Hidden,
        Revealed,
        Scavenged
    }

    public enum ScrapType
    {
        Normal,
        Hidden
    }

    public class ScrapLevelObject : BaseInteractable
    {
        [SerializeField] 
        private GameObject graphicsRoot;
    
        [SerializeField] 
        private ScrapType scrapType;

        [SerializeField] 
        private float secondsToGrab;
    
        [ShowInInspector]
        public float FillPercentage => _fillPercentage;
        private float _fillPercentage;    
    
        [SerializeField] 
        private CanvasGroup canvasGroup;
    
        [SerializeField] 
        private Image fillImage;

        public DebrisData debrisData;
    
        private float _currentFillAmount;
        private float _initialSecondsToGrab;
    
        private float CurrentFillAmount
        {
            get => _currentFillAmount;
            set
            {
                _currentFillAmount = value;
                
                if (_currentFillAmount <= 0)
                {
                    _currentFillAmount = 0;
                }

                if (_currentFillAmount > secondsToGrab)
                {
                    _currentFillAmount = secondsToGrab;
                }
            }
        }
    
        public bool CanBeInteracted => _currentState is ScrapState.Revealed;
        public event Action<ScrapLevelObject> Released;

        private ScrapState _currentState;
        private bool _interactActionCompleted;

        private void Awake()
        {        
            _initialSecondsToGrab = secondsToGrab;
        }

        public void Setup()
        {
            secondsToGrab = _initialSecondsToGrab;
        
            _currentState = scrapType switch
            {
                ScrapType.Normal => ScrapState.Revealed,
                ScrapType.Hidden => ScrapState.Hidden,
                _ => throw new ArgumentOutOfRangeException()
            };

            canvasGroup.alpha = 0;

            switch (scrapType)
            {
                case ScrapType.Hidden:
                    _currentState = ScrapState.Hidden;
                    graphicsRoot.SetActive(false);
                    break;
                case ScrapType.Normal:
                    _currentState = ScrapState.Revealed;
                    graphicsRoot.SetActive(true);
                    break;
            }
        }

        public void Release()
        {
            Released?.Invoke(this);
        }
    
        public override bool Interact(GameObject interactionSource, out InteractionResultData interactionResultData)
        {
            interactionResultData = new InteractionResultData();
        
            if (!CanBeInteracted)
            {
                interactionResultData.IsSuccessful = false;
                interactionResultData.Message = "Interaction not completed";
                interactionResultData.Data = null;
            
                return false;
            }
            canvasGroup.alpha = 1;
        
            CurrentFillAmount += Time.deltaTime;
            _fillPercentage = CurrentFillAmount / secondsToGrab;
            fillImage.fillAmount = _fillPercentage;
            
            if (Math.Abs(CurrentFillAmount - secondsToGrab) < 0.05f)
            {
                _interactActionCompleted = true;
           
                interactionResultData.IsSuccessful = true;
                interactionResultData.Message = "Scrap interaction completed";
                interactionResultData.Data = this;
        
                return true;
            }
        
            return false;
        }

        public void ScrapperLeaved()
        {
            canvasGroup.alpha = 0;
            // CurrentFillAmount = 0;
            fillImage.fillAmount = 0;
            // _fillPercentage = 0; // Note: we want the interaction is completed next time
            _interactActionCompleted = false;
        }
        
        [ContextMenu("Detect")]
        public void Detect()
        {
            switch (scrapType)
            {
                case ScrapType.Normal:
                    HandleDetectForNormal();
                    break;
                case ScrapType.Hidden:
                    HandleDetectForHidden();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void HandleDetectForHidden()
        {
            if (_currentState == ScrapState.Scavenged)
            {
                return;
            }

            _currentState = ScrapState.Revealed;
            graphicsRoot.SetActive(true);
        }

        private void HandleDetectForNormal()
        {
            if (_currentState == ScrapState.Scavenged)
            {
                return;
            }

            _currentState = ScrapState.Revealed;
            graphicsRoot.SetActive(true);
        }
    }
}