﻿using _Project.Scripts.Core.ScavengeSystem.Items;

[System.Serializable]
public class HealthRecoveryByRarity
{
    public ScrapRarity Rarity;
    public int HealthToRecover;
}