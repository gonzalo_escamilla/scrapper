﻿namespace _Project.Scripts.Core.ScavengeSystem.Items
{
    public enum ScrapRarity
    {
        Common, // Red
        Uncommon,  // Orange
        Rare,  // Yellow
        Epic,  // Green
        Legendary // Blue
    }
}