﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Core.ScavengeSystem.Items
{
    public class ScrapItemView : UILayout
    {
        [SerializeField] private TextMeshProUGUI percentageLabel;
        [SerializeField] private Image image;

        private ScrapItemData _data;
        
        public void SetData(ScrapItemData data)
        {
            _data = data;
            
            ColorUtility.TryParseHtmlString(data.GetRarityColor(), out Color color);
            image.color = color;
        }
        
        public override void Refresh()
        {
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            percentageLabel.text = $"{_data.CompletionPercentage:P}";
        }
    }
}