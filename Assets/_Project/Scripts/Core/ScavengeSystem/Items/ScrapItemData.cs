﻿namespace _Project.Scripts.Core.ScavengeSystem.Items
{
    [System.Serializable]
    public class ScrapItemData : ScrapData
    {
        public ScrapRarity Rarity;

        public ScrapItemData(ScrapRarity rarity, float pickUpTime)
        {
            Rarity = rarity;
            _scavengeCompletionThreshold = pickUpTime;
        }

        public string GetRarityColor()
        {
            return Rarity switch
            {
                ScrapRarity.Common => "red",
                ScrapRarity.Uncommon => "orange",
                ScrapRarity.Rare => "yellow",
                ScrapRarity.Epic => "green",
                ScrapRarity.Legendary => "blue",
                _ => "white"
            };
        }
    }
}