﻿using System.Collections.Generic;
using _Project.Scripts.Core.CoreGUI;
using _Project.Scripts.Core.ScavengeSystem.Containers;
using _Project.Scripts.Core.ScavengeSystem.Items;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _Project.Scripts.Core.ScavengeSystem
{
    public class ScavengeMenuView : GameMenuBase
    {
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private Transform scrapContainerParent;
        [SerializeField] [AssetsOnly] private ScrapContainerView containerPrefab;
        [SerializeField] [AssetsOnly] private ScrapItemView itemPrefab;
        [SerializeField] private Transform selectorImage;
        
        private DebrisData _data;
        private List<UILayout> _scrapViews = new();
        
        public override void Show()
        {
            canvasGroup.DOFade(1, 0.25f);
        }

        public override void Hide()
        {
            canvasGroup.DOFade(0, 0.25f);
        }

        public void SetData(DebrisData data)
        {
            _data = data;

            PopulateUI();
        }

        public void Release()
        {
            _data = null;
        }
        
        public void Dispose()
        {
            selectorImage.SetParent(this.transform);
            foreach (var scrapView in _scrapViews)
            {
                Destroy(scrapView.gameObject);
            }
            _scrapViews.Clear();
            _data = null;
        }
        
        public void PopulateUI()
        {
            if (_scrapViews.Count > 0)
            {
                foreach (var layout in _scrapViews)
                {
                    Destroy(layout.gameObject);
                }
            }           
            _scrapViews.Clear();

            
            foreach (var scrap in _data.AllScrap)
            {
                if (scrap is ScrapItemData itemData)
                {
                    var newItemView = Instantiate(itemPrefab, Vector3.zero, Quaternion.identity);
                    newItemView.SetData(itemData);
                    newItemView.transform.SetParent(scrapContainerParent);
                    _scrapViews.Add(newItemView);
                }
                if (scrap is ScrapContainerData containerData)
                {
                    var newContainerView = Instantiate(containerPrefab, Vector3.zero, Quaternion.identity);
                    newContainerView.SetData(containerData);
                    newContainerView.transform.SetParent(scrapContainerParent);
                    _scrapViews.Add(newContainerView);
                }
            }
        }

        public void MoveSelectionTo(int currentSelectedIndex)
        {
            if (_scrapViews.Count <= 0 || _scrapViews[currentSelectedIndex] == null)
            {
                Debug.LogWarning("No scrap to select");
                return;
            }
            
            Transform newParent = _scrapViews[currentSelectedIndex].transform;
            selectorImage.SetParent(newParent);
            selectorImage.transform.localPosition = Vector3.zero;
            selectorImage.SetAsFirstSibling();
        }

        public void Refresh()
        {            
            foreach (var scrapView in _scrapViews)
            {
                scrapView.Refresh();
            }
        }

        public void LockSelector()
        {
            selectorImage.SetParent(transform);
            selectorImage.transform.localPosition = Vector3.zero;
            selectorImage.SetAsFirstSibling();
        }
    }
}