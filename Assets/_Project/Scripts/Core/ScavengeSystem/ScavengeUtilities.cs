﻿using System.Collections.Generic;
using _Project.Scripts.Core.ScavengeSystem.Containers;
using _Project.Scripts.Core.ScavengeSystem.Items;

namespace _Project.Scripts.Core.ScavengeSystem
{
    public static class ScavengeUtilities
    {
        public static DebrisData GetMockDebrisData()
        {
            var containerScrap = new List<ScrapData>()
            {
                new ScrapItemData(ScrapRarity.Rare, 1),
                new ScrapItemData(ScrapRarity.Uncommon, 2)
            };
            var containerData = new ScrapContainerData(containerScrap);
                
            var allScrap = new List<ScrapData>()
            {
                containerData,
                new ScrapItemData(ScrapRarity.Epic, 3),
                new ScrapItemData(ScrapRarity.Common, 4)
            };

            DebrisData newMockData = new(allScrap);

            return newMockData;
        }
    }
}