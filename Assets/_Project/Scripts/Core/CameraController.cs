﻿using System;
using _Project.Scripts.Core;
using _Project.Scripts.GameServices;
using Cinemachine;
using UnityEngine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class CameraController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    [SerializeField] private Transform target;
    [SerializeField] private float rotationSpeed = 5f;
    [SerializeField] private float rotationThreshold = 0.125f;
    
    IInputProvider _input;

    private bool _isRotation;
    private float _currentRotation;
    
    public void Awake()
    {
        target ??= FindObjectOfType<ScrapperController>().transform;
        virtualCamera ??= GetComponent<CinemachineVirtualCamera>();
        
        Services.WaitFor<IInputProvider>(OnInputProvider);
            
        if (!target)
        {
            Debug.LogError("Target object not found.");
        }

        if (!virtualCamera)
        {
            Debug.LogError("Virtual Camera not found.");
        }
        virtualCamera.Follow = target;
    }

    private void Update()
    {
        if (Mathf.Abs(_currentRotation) > rotationThreshold)
        {
            float rotationAngle = Mathf.Sign(_currentRotation) * rotationSpeed * Time.deltaTime;
            virtualCamera.transform.RotateAround(virtualCamera.transform.position, Vector3.up, rotationAngle);
        }
        
        if (!_isRotation)
        {
            _currentRotation = 0;
        }
    }

    private void OnInputProvider(IInputProvider inputProvider)
    {
        _input = inputProvider;
        _input.RotateCameraPerformed += OnRotateCameraPerformed;
        _input.RotateCameraCanceled += OnRotateCameraCanceled;
    }

    private void OnDisable()
    {
        _input.RotateCameraPerformed -= OnRotateCameraPerformed;
        _input.RotateCameraCanceled -= OnRotateCameraCanceled;
    }
    
    private void OnRotateCameraPerformed(float direction)
    {
        if (_isRotation)
            return;
        
        _isRotation = true;
        _currentRotation = direction;
    }

    private void OnRotateCameraCanceled()
    {
        _isRotation = false;
    }
}