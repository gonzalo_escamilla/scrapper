﻿using System;
using _Project.Scripts.Core;
using _Project.Scripts.Core.EventsSystem;
using Enderlook.EventManager;
using UnityEngine;

public class ExitZone : MonoBehaviour
{
    [SerializeField] private Transform center;

    public Vector3 Center => center.position;
    
    public event Action CharacterEntered;
    public event Action CharacterLeaved;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ScrapperController>())
        {
            CharacterEntered?.Invoke();
            EventManager.Shared.Raise(new GameEvents.CharacterEvents.CharacterEnteredExitZone(this));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<ScrapperController>())
        {
            CharacterLeaved?.Invoke();
            EventManager.Shared.Raise<GameEvents.CharacterEvents.CharacterLeavedExitZone>();
        }
    }
}
