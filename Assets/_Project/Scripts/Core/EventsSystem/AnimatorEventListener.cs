using System;
using System.Collections.Generic;
using _Project.Scripts.Core.EventsSystem;
using UnityEngine;
using Object = UnityEngine.Object;

namespace _Project.Scripts.Utilities
{
    public class AnimatorEventListener : MonoBehaviour
    {
        private Dictionary<string, Action<AnimationEventData>> events = new ();

        public Action<AnimationEventData> AddListener(string eventName, Action<AnimationEventData> callback)
        {
            eventName = eventName.ToLower();

            if (events.ContainsKey(eventName))
            {
                return events[eventName];
            }
            events.Add(eventName, callback);
            return events[eventName];
        }

        public void ClearEvents()
        {
            events.Clear();
        }

        public void TriggerEvent(Object eventObject)
        {
            if (eventObject == null)
            {
                return;
            }
        
            AnimationEventData animationEventData = eventObject as AnimationEventData;
            if (animationEventData == null)
            {
                Debug.LogError("The animation event data is not valid");
                return;
            }

            string eventKey = animationEventData.EventKey.ToLower();
            events[eventKey]?.Invoke(animationEventData);
        }
    }
}