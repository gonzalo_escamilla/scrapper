﻿using UnityEngine;

namespace _Project.Scripts.Core.EventsSystem
{
    [CreateAssetMenu(menuName = "Scrapper/Animation/AnimationEventData", fileName = "AnimationEventData", order = 0)]
    public class AnimationEventData : ScriptableObject
    {
        [SerializeField] private string eventKey;

        public string EventKey => eventKey;
    }
}