﻿using System;
using _Project.Scripts.Core.GameStates;
using _Project.Scripts.Core.NarrativeSystem;
using _Project.Scripts.Core.ScavengeSystem;
using _Project.Scripts.Core.ScavengeSystem.Items;
using UnityEngine.SceneManagement;

namespace _Project.Scripts.Core.EventsSystem
{
    public static class GameEvents
    {
        private static byte GetByteFromBool(bool value)
        {
            return (byte)(value ? 1 : 2);
        }

        private static bool GetBoolFromByte(byte value)
        {
            return value switch
            {
                1 => true,
                2 => false,
                _ => throw new InvalidOperationException("Struct is default."),
            };
        }

        public static class Dialogue
        {
            public readonly struct DialogueInteractionCompleted
            {
                public readonly DialogueTriggerData DialogueTriggerData;

                public DialogueInteractionCompleted(DialogueTriggerData dialogueTriggerData)
                {
                    DialogueTriggerData = dialogueTriggerData;
                }
            }

            public readonly struct DialogueStarted
            {
                public readonly string KnotName;

                public DialogueStarted(string knotName)
                {
                    KnotName = knotName;
                }
            }

            public readonly struct InkVariableUpdated
            {
                public readonly string VariableName;
                public readonly object Value;

                public InkVariableUpdated(string variableName, object value)
                {
                    this.VariableName = variableName;
                    this.Value = value; 
                }
            }
        }

        #region CombatEvents

        public static class CombatEvents
        {
            public readonly struct BulletFired { }
            public readonly struct WeaponRecharged { } //TBD
            public readonly struct BulletHitEnemy { }
            public readonly struct BulletHitGround { }
            public readonly struct ChangeWeapon { } //TBD
        } 

        #endregion

        
        #region CharacterEvents

        public static class CharacterEvents
        {
            public readonly struct ScrapInteractionCompleted
            {
                public readonly ScrapLevelObject ScrapLevelObject;

                public ScrapInteractionCompleted(ScrapLevelObject scrapLevelObject)
                {
                    ScrapLevelObject = scrapLevelObject;
                }
            }
                
            public readonly struct DebugScrapGrabbed
            {
                public readonly ScrapLevelObject GrabbedScrapLevelObject;

                public DebugScrapGrabbed(ScrapLevelObject grabbedScrapLevelObject)
                {
                    GrabbedScrapLevelObject = grabbedScrapLevelObject;
                }
            }
            
            public readonly struct ScrapGrabbed
            {
                public readonly ScrapItemData GrabbedScrapItem;

                public ScrapGrabbed(ScrapItemData grabbedScrapItem)
                {
                    GrabbedScrapItem = grabbedScrapItem;
                }
            }
            
            public readonly struct InitialHealthSet
            {
                public readonly float CurrentHealth;

                public InitialHealthSet(float currentHealth)
                {
                    CurrentHealth = currentHealth;
                }
            }
            
            public readonly struct HealthLost
            {
                public readonly float CurrentHealth;

                public HealthLost(float currentHealth)
                {
                    CurrentHealth = currentHealth;
                }
            }
            
            public readonly struct HealthChanged
            {
                public readonly float CurrentHealth;

                public HealthChanged(float currentHealth)
                {
                    CurrentHealth = currentHealth;
                }
            }
            
            public readonly struct HealthChangedInExchange
            {
                public readonly float AddedHealth;

                public HealthChangedInExchange(float addedHealth)
                {
                    AddedHealth = addedHealth;
                }
            }
            
            public readonly struct CharacterDied { }

            public readonly struct CharacterEnteredExitZone
            {
                public readonly ExitZone Zone;

                public CharacterEnteredExitZone(ExitZone zone)
                {
                    Zone = zone;
                }
            }
            public readonly struct CharacterLeavedExitZone { }

            public struct StatsChanged
            {
                public readonly ScrapperStats Stats;

                public StatsChanged(ScrapperStats stats)
                {
                    Stats = stats;
                }
            }

            public struct SpeedAttributeChanged
            {
                public readonly float Speed;

                public SpeedAttributeChanged(float speed)
                {
                    Speed = speed;
                }
            }

            public struct HealthAttributeChanged
            {
                public readonly float Health;

                public HealthAttributeChanged(float health)
                {
                    Health = health;
                }
            }
            
            public readonly struct CharacterDashStarted { }
            public readonly struct RadarChargeStarted { }
            public readonly struct RadarChargeReleased { }
        }

        #endregion
        
        #region UIEvents

        public static class UIEvents
        {
            // public readonly struct RestartButtonPressed { } // Deprecated
        }

        #endregion
        
        #region Game State

        public static class GameStateEvents
        {
            public readonly struct ApplicationResetTriggered { }
                
            public readonly struct GameStateChanged
            {
                public readonly GameState CurrentState;

                public GameStateChanged(GameState newState)
                {
                    CurrentState = newState;
                }
            }
            
            public readonly struct RestartRunTriggered { } // Deprecated

            public readonly struct OnApplicationFocusChanged
            {
                private readonly byte _isFocused;
                public readonly bool IsFocused => GetBoolFromByte(_isFocused);

                public OnApplicationFocusChanged(bool isFocused)
                {
                    _isFocused = GetByteFromBool(isFocused);
                }
            }

            public readonly struct OnPause
            {
                private readonly byte _isPaused;
                public readonly bool IsPaused => GetBoolFromByte(_isPaused);

                public OnPause(bool isPaused)
                {
                    _isPaused = GetByteFromBool(isPaused);
                }
            }

            public readonly struct OnSceneLoaded
            {
                private readonly Scene? _scene;
                public readonly Scene Scene => _scene.Value;

                public OnSceneLoaded(Scene scene)
                {
                    _scene = scene;
                }
            }

            /// <summary>
            /// Called after the whole scene gets initiallized.
            /// </summary>
            public readonly struct OnSceneFullyLoaded
            {
                private readonly Scene? _scene;
                public readonly Scene Scene => _scene.Value;

                public OnSceneFullyLoaded(Scene scene)
                {
                    _scene = scene;
                }
            }

            public readonly struct LogosFinishedPlaying { }
        }
        #endregion
        
        

        public static class PlayerEvents
        {
            public readonly struct PlayerScrapExchangeFinished { }
        }
    }
}