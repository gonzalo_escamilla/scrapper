namespace Pyros.Editor.Toolbar
{
    public interface IToolbarObject
    {
        void Setup();
        void Draw();
    }
}