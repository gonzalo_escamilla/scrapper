using System;
using UnityEngine;

namespace Pyros.Editor.Toolbar
{
    [Serializable]
    struct ToolbarIcons
    {
        public Texture2D GitIcon;
    }
}