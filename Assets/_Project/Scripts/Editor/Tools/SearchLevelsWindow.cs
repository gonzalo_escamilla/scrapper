﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEditor.SceneManagement;

namespace _Project.Scripts.Editor.Tools
{
    public class SearchLevelsWindow : EditorWindow
    {
        [MenuItem("Tools/Scrapper/Levels/Search levels")]
        public static void ShowWindow()
        {
            var window = EditorWindow.GetWindow<SearchLevelsWindow>() ?? EditorWindow.CreateInstance<SearchLevelsWindow>();
            window.titleContent = new GUIContent("Search levels");
            window.minSize = window.maxSize = new Vector2(500, 600);
            window.Show();
        }
        private Vector2 scrollPosition;

        private void OnGUI()
        {
            GUILayout.Label("Scenes In Build", EditorStyles.boldLabel);

            // Get the scenes in build settings
            EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
            if (scenes.Length == 0)
            {
                GUILayout.Label("No scenes in build settings.");
                return;
            }

            scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true);
            foreach (EditorBuildSettingsScene scene in scenes)
            {
                if (scene == null)
                    continue;

                string scenePath = scene.path;
                string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePath);

                GUILayout.BeginHorizontal();

                GUILayout.Label(sceneName, GUILayout.Width(200));

                if (GUILayout.Button("Open"))
                {
                    OpenScene(scenePath);
                }
                if (GUILayout.Button("Add"))
                {
                    OpenSceneAdditive(scenePath);
                    DisableNonMainCameras();
                }
                if (GUILayout.Button("Close"))
                {
                    CloseIfOpened(scenePath);
                }
                
                GUILayout.EndHorizontal();
            }
            GUILayout.EndScrollView();
        }

        private void OpenScene(string scenePath)
        {
            if (SceneManager.GetActiveScene().path != scenePath)
            {
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    CloseAllScenesExcept(scenePath);
                    EditorSceneManager.OpenScene(scenePath);
                }
            }
            else
            {
                Debug.Log("Scene is already open.");
            }
        }
        
        private void OpenSceneAdditive(string scenePath)
        {
            if (SceneManager.GetActiveScene().path != scenePath)
            {
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    EditorSceneManager.OpenScene(scenePath, OpenSceneMode.Additive);
                }
            }
            else
            {
                Debug.Log("Scene is already open.");
            }
        }

        private void CloseIfOpened(string scenePath)
        {
            Scene sceneToClose = SceneManager.GetSceneByPath(scenePath);

            if (sceneToClose.isLoaded && sceneToClose.buildIndex != 0)
            {
                if (EditorSceneManager.SaveModifiedScenesIfUserWantsTo(new[] { sceneToClose }))
                {
                    EditorSceneManager.CloseScene(sceneToClose, true);
                }
            }
            else
            {
                Debug.Log("Scene is either not loaded or it is the first scene in the build settings and cannot be closed.");
            }
        }
        
        private void DisableNonMainCameras()
        {
            Camera[] allCameras = GameObject.FindObjectsOfType<Camera>();

            foreach (Camera cam in allCameras)
            {
                if (cam.tag != "MainCamera")
                {
                    cam.enabled = false;
                }
            }
        }
        
        private void CloseAllScenesExcept(string scenePath)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (scene.path != scenePath && scene.buildIndex != 0)
                {
                    if (scene.isLoaded && EditorSceneManager.SaveModifiedScenesIfUserWantsTo(new[] { scene }))
                    {
                        EditorSceneManager.CloseScene(scene, true);
                    }
                }
            }
        }
    }
}
#endif
