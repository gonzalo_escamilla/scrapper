using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Project.Scripts.Utilities
{
    [System.Serializable]
    public class Detector<T> where T : Component
    {
        [Header("Settings")] 
        private Transform _center;
        // private LayerMask _detectionMask;
        // private float _radius;
        // private float _frameRate = 0.25f;
        // private int _maxDetectableColliders = 10;
        // private bool _sortByDistance;
        private float _elapsedTime = 0f;
        
        [Header("Status")]
        [SerializeField] private List<T> detectedComponents;

        public event Action<List<T>> OnNewDetection;
        public event Action<T> OnTargetDetected;
        public event Action<T> OnTargetLost;
        
        private Collider[] _allocatedColliders;
        private HashSet<T> _detectedInThisFrame = new();
        private DetectorSettings _settings;

        public Detector(Transform center, DetectorSettings settings)
        {
            _center = center;
            _settings = settings;
            _elapsedTime = 0f;
            
            _allocatedColliders = new Collider[_settings.maxDetectableColliders];
            detectedComponents = new List<T>();
        }

        public void Update()
        {
            Detect();
        }

        public List<T> GetCurrentDetectedComponents()
        {
            return detectedComponents;
        }
        
        public static void DetectColliders(Vector3 center, float radius, out Collider[] hits, LayerMask detectionLayer)
        {
            Collider[] colliders = new Collider[10];
            int collidersAmount = Physics.OverlapSphereNonAlloc(center, radius, colliders, detectionLayer);
            hits = new Collider[collidersAmount];

            for (int i = 0; i < hits.Length; i++)
            {
                hits[i] = colliders[i];
            }
        }

        public List<T> Detect(float range)
        {
            detectedComponents = new();
            
            int collidersAmount = Physics.OverlapSphereNonAlloc(_center.position, range, _allocatedColliders, _settings.detectionMask);
            
            for (int i = 0; i < collidersAmount; i++)
            {
                var component = _allocatedColliders[i].GetComponent<T>();
                if (component != null)
                {
                    if (!detectedComponents.Contains(component))
                    {
                        detectedComponents.Add(component);
                    }
                }
            }

            return detectedComponents;
        }
        
        public void SortByDistance(List<T> currentColl)
        {
            if (currentColl.Count > 0)
            {
                currentColl.Sort((a, b) =>
                {
                    return Vector3.SqrMagnitude(a.transform.position - _center.position).CompareTo(
                        Vector3.SqrMagnitude(b.transform.position - _center.position));
                });
            }
        }

        public void Reset()
        {
            detectedComponents = new List<T>();
            _allocatedColliders = new Collider[_settings.maxDetectableColliders];
        }
        
        private void Detect()
        {
            _elapsedTime += Time.deltaTime;
            if (!(_elapsedTime > _settings.frameRate)) 
                return;
        
            int amount  = Physics.OverlapSphereNonAlloc(_center.position, _settings.radius, _allocatedColliders, _settings.detectionMask);
        
            _elapsedTime = 0;
        
            detectedComponents.RemoveAll(component => component == null);
        
            _detectedInThisFrame.Clear();  // Clear the set before each detection
        
            for (int i = 0; i < amount; i++)
            {
                var detectedComponent = _allocatedColliders[i].GetComponent<T>();
        
                if (detectedComponent == null)
                    continue;
        
                _detectedInThisFrame.Add(detectedComponent);  // Track components detected in this frame
        
                if (detectedComponents.Contains(detectedComponent)) 
                    continue;
        
                detectedComponents.Add(detectedComponent);
        
                // Invoke the detected objective event.
                OnTargetDetected?.Invoke(detectedComponent);
            }
        
            var componentsToRemove = new List<T>();
            foreach (var component in detectedComponents)
            {
                if (!_detectedInThisFrame.Contains(component))  // Remove components no longer detected
                {
                    componentsToRemove.Add(component);
                }
            }
        
            foreach (var component in componentsToRemove)
            {
                detectedComponents.Remove(component);
                OnTargetLost?.Invoke(component);
            }
        
            if (_settings.sortByDistance)
                SortByDistance(detectedComponents);
            
            OnNewDetection?.Invoke(detectedComponents);
        }

        public void OnDrawGizmos()
        {
            if (!Application.isPlaying)
                return;
            
            Gizmos.DrawWireSphere(_center.position, _settings.radius);

            for (int i = 0; i < detectedComponents.Count; i++)
            {
                Gizmos.color = Color.white;
                if (_settings.sortByDistance && i == 0)
                {
                    Gizmos.color = Color.green;
                }
                
                Gizmos.DrawLine(_center.position, detectedComponents[i].transform.position);
            }
        }
    }
}