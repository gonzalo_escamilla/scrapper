﻿namespace _Project.Scripts
{
    public static class BuildSettings
    {
        //In case we need to mimic a debug build in code for some reason
        public static bool IsDebugBuild => UnityEngine.Debug.isDebugBuild;

        public static bool IsUnityEditor
        {
            get
            {
#if UNITY_EDITOR
                return true;
#endif
                return false;
            }
        }
    }
}