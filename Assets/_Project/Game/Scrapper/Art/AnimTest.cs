using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimTest : MonoBehaviour
{
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("w")) {
            animator.SetBool("IsWalking", true);
        }
        if (!Input.GetKey("w"))
        {
            animator.SetBool("IsWalking", false);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetBool("IsRunning", true);
        }
        if (!Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetBool("IsRunning", false);
        }

        if(Input.GetKey(KeyCode.Space))
        {
            animator.SetBool("IsRolling", true);
        }
        if (!Input.GetKey(KeyCode.Space))
        {
            animator.SetBool("IsRolling", false);
        }

        if (Input.GetKey("p"))
        {
            animator.SetBool("IsScrapping", true);
        }
        if (!Input.GetKey("p"))
        {
            animator.SetBool("IsScrapping", false);
        }

        if (Input.GetKey("o"))
        {
            animator.SetBool("IsDeath", true);
        }
        if (!Input.GetKey("o"))
        {
            animator.SetBool("IsDeath", false);
        }
    }
}
