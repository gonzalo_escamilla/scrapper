// Variables
VAR current_health_percentage = 0.1

VAR current_scrap_amount_common = 1
VAR current_scrap_amount_uncommon = 1
VAR current_scrap_amount_rare = 1
VAR current_scrap_amount_epic = 1
VAR current_scrap_amount_legendary = 1

VAR guita = 1

VAR has_grabbed_red_sphere = false
VAR has_grabbed_green_sphere = false
VAR has_grabbed_blue_sphere = false