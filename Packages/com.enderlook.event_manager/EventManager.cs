using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Enderlook.EventManager
{
    public class EventManager
    {
        private readonly ConcurrentDictionary<Type, List<Delegate>> dictionaryVoid = new();
        private readonly ConcurrentDictionary<Type, List<Delegate>> dictionaryWithArgument = new();

        public static EventManager Shared { get; } = new();

        public void Subscribe<TEvent>(Action<TEvent> callback)
        {
            List<Delegate> list = dictionaryWithArgument.GetOrAdd(typeof(TEvent), static _ => new List<Delegate>());
            lock (list)
                list.Add(callback);
        }

        public void Subscribe<TEvent>(Action callback)
        {
            List<Delegate> list = dictionaryVoid.GetOrAdd(typeof(TEvent), static _ => new List<Delegate>());
            lock (list)
                list.Add(callback);
        }

        public void Unsubscribe<TEvent>(Action<TEvent> callback)
        {
            if (dictionaryWithArgument.TryGetValue(typeof(TEvent), out List<Delegate> list))
            {
                lock (list)
                    list.Remove(callback);
            }
        }

        public void Unsubscribe<TEvent>(Action callback)
        {
            if (dictionaryVoid.TryGetValue(typeof(TEvent), out List<Delegate> list))
            {
                lock (list)
                    list.Remove(callback);
            }
        }

        public void Raise<TEvent>() where TEvent : new() => Raise(new TEvent());

        public void Raise<TEvent>(TEvent argument)
        {
            (Delegate[] array, int count) withVoid = Work(dictionaryVoid);
            (Delegate[] array, int count) withArgument = Work(dictionaryWithArgument);

            Span<Delegate> span = withVoid.array.AsSpan(0, withVoid.count);
            foreach (Delegate element in span)
                Unsafe.As<Action>(element)();
            span.Clear();
            ArrayPool<Delegate>.Shared.Return(withVoid.array);

            span = withArgument.array.AsSpan(0, withArgument.count);
            foreach (Delegate element in span)
                Unsafe.As<Action<TEvent>>(element)(argument);
            span.Clear();
            ArrayPool<Delegate>.Shared.Return(withArgument.array);

            static (Delegate[] array, int count) Work(ConcurrentDictionary<Type, List<Delegate>> dictionary)
            {
                if (dictionary.TryGetValue(typeof(TEvent), out List<Delegate> list))
                {
                    Delegate[] array;
                    lock (list)
                    {
                        int count = list.Count;
                        array = ArrayPool<Delegate>.Shared.Rent(count);
                        list.CopyTo(array, 0);
                        return (array, count);
                    }
                }
                return (Array.Empty<Delegate>(), 0);
            }
        }
    }
}